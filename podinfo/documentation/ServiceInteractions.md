# CUBE | User Microservice 

## Overview
This document specifies the various form of interactions this service will have
with other microservices and the scenarios that will initiate these interactions

## Service Interactions
The user microservice will primarily interact with the authentication service, the email service, and the social service. Much of these interactions for now will be through http and rabbitMQ. In the future, the hope is to have all inter-service communication utilize grpc. 

### Email Service
Interactions with the email service will be initiated by the following set of events. 
* User & Team account creation
    - Authentication
        - Email sent to the email service with the body message stating to validate email account
    - Welcome email body message sent to the email microservice upon sucessfuly completion of the authentication step
* User & Team account deletion
    - Goodbye email body message sent to the email microservice (.... sad to see you leave)
* Subscription (Team & User Account) 
    - Email certifying the creation of the subscription
* Password Reset 
    - Email sent with the reset token
