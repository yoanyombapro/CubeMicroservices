package model

// IHashable provides interface definitions for various models associated with this service
type IHashable interface {
	GetPasswordToHash() string
}

// GetPasswordToHash returns the password from a team model
func (t Team) GetPasswordToHash() string {
	return t.GetPassword()
}

// GetPasswordToHash returns the passwword from a user model
func (u User) GetPasswordToHash() string {
	return u.GetPassword()
}

// GetPasswordToHash returns a password from a team orm model
func (t TeamORM) GetPasswordToHash() string {
	return t.Password
}

// GetPasswordToHash returns a password from a user orm model
func (u UserORM) GetPasswordToHash() string {
	return u.Password
}
