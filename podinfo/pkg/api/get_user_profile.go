package api

import (
	"net/http"

	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type GetUserProfileResponse struct {
	Error       error         `json:"error"`
	UserProfile model.Profile `json:"result"`
}

// swagger:response getUserProfileResponse
type GetUserProfileResponseSwagger struct {
	// in: body
	Body struct {
		// error
		// required: true
		Error error `json:"error"`
		// profile of interest
		// required: true
		UserProfile model.Profile `json:"result"`
	}
}

// swagger:route GET /v1/profile/{id} User UserProfile getUserProfile
// Gets a user profile by account id
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: getUserProfileResponse
// gets a user profile from the backend by the specified account id
func (s *Server) getUserProfileHandler(w http.ResponseWriter, r *http.Request) {
	// extract the user account id from the request url
	userAccountId := requestutils.ExtractIDFromRequest(r)

	// query the database for the set of users on interest
	userProfile, err := s.Db.GetUserProfile(r.Context(), userAccountId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	response := GetUserProfileResponse{
		err,
		userProfile,
	}
	s.JSONResponse(w, r, response)
}
