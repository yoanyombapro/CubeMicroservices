package api

import (
	"bytes"
	"encoding/json"
	"math/rand"
	"net/http"
	"strconv"
	"testing"
	"time"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/errorutils"
)

func TestUserLogout(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	randNum := strconv.Itoa(rand.Intn(100000))

	loginUserReq := LoginUserRequest{
		Username: "test-username" + randNum,
		Password: "ksdnlfsjd" + randNum,
	}

	// marshall the struct into a json string
	jsonStr, err := json.Marshal(&loginUserReq)
	if err != nil {
		return
	}

	srv := InitializeMockServer()

	// call the authentication service through wrapper
	aggregatedErr, _ := srv.Auth.SignUp(loginUserReq.Username, loginUserReq.Password)
	if aggregatedErr != nil {
		if errorutils.ProcessAggregatedErrorsInTest(t, aggregatedErr) {
			return
		}
	}

	// set up http request
	req, _ := http.NewRequest("POST", "/v1/login/user", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	// perform request
	response := ExecuteRequest(req, srv.loginUserHandler)
	CheckResponseCode(t, http.StatusOK, response.Code)

	var loginResponse LoginResponse
	_ = json.Unmarshal(response.Body.Bytes(), &loginResponse)

	if loginResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", loginResponse.Error)
	}

	// now log the user out
	// set up http request
	req, _ = http.NewRequest("Delete", "/v1/logout", nil)
	req.Header.Set("Content-Type", "application/json")

	// perform request
	response = ExecuteRequest(req, srv.logoutHandler)
	CheckResponseCode(t, http.StatusOK, response.Code)
}
