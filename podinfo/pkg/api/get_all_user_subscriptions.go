package api

import (
	"net/http"

	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type GetAllUserSubscriptionsResponse struct {
	Error         error                 `json:"error"`
	Subscriptions []model.Subscriptions `json:"result"`
}

// swagger:parameters getAllSubscriptions
type GetAllUserSubscriptionsRequestSwagger struct {
	// in: query
	AccountId uint32
}

// swagger:response getSubscriptionsResponse
type GetAllUserSubscriptionsResponseSwagger struct {
	// in: body
	Body struct {
		// errors occuring throughout the request lifecycle
		// required: true
		Error error `json:"error"`
		// set of subscriptions of interest
		// required: true
		Subscriptions []model.Subscriptions `json:"result"`
	}
}

// swagger:route GET /v1/user/subscription/{id} User Subscription getAllSubscriptions
//
// Get User Subscriptions
//
// Gets a set of subscriptions for a given user
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: getSubscriptionsResponse
func (s *Server) getAllUserSubscriptionsHandler(w http.ResponseWriter, r *http.Request) {
	userAccountId := requestutils.ExtractIDFromRequest(r)
	subscriptions, err := s.Db.GetAllUserSubscriptions(r.Context(), userAccountId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response := GetAllUserSubscriptionsResponse{
		err,
		subscriptions,
	}
	s.JSONResponse(w, r, response)
}
