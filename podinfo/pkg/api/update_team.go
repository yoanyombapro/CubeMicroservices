package api

import (
	"net/http"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/helper/customerror"
	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type UpdateTeamRequest struct {
	Team model.Team `json:"payload"`
}

type UpdateTeamResponse struct {
	Error error      `json:"error"`
	Team  model.Team `json:"payload"`
}

// Update team request
// swagger:parameters updateTeamRequest
type UpdateTeamRequestSwagger struct {
	// in: body
	Body struct {
		// team to update
		// required: true
		Team model.Team `json:"payload"`
	}
	// in: query
	// required: true
	// exmaple; 1243
	TeamId uint32 `json:"teamId"`
}

// Updated team response
// swagger:response updateTeamResponse
type UpdateTeamResponseSwagger struct {
	// in: body
	Body struct {
		// updated team
		// required: true
		Team model.Team `json:"result"`
		// error
		// required: true
		Error error `json:"error"`
	}
}

// swagger:route PUT /v1/team/{id} Team updateTeamRequest
// Updates Team
//
// Updates a team account present in the backend database
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: updateTeamResponse
// updates a team based on id
func (s *Server) updateTeamHandler(w http.ResponseWriter, r *http.Request) {
	var (
		updateTeamRequest UpdateTeamRequest
	)

	id := requestutils.ExtractIDFromRequest(r)

	// decode the data present in the body
	err := customerror.DecodeJSONBody(w, r, &updateTeamRequest)
	if err != nil {
		requestutils.ProcessMalformedRequest(w, err)
		return
	}

	updatedTeam, err := s.Db.UpdateTeam(r.Context(), id, updateTeamRequest.Team)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response := UpdateTeamResponse{
		nil,
		*updatedTeam,
	}

	s.JSONResponse(w, r, response)
}
