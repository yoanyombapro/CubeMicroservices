package api

import (
	"net/http"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type DeleteTeamProfileResponse struct {
	Error error `json:"error"`
}

// swagger:parameters deleteTeamProfileRequest
type deleteTeamProfileRequest struct {
	// team account id to which the profile to delete is tied to
	// in: query
	// required: true
	// example: 123
	teamAccountId uint32 `jsons:"id"`
}

// swagger:response deleteTeamProfileResponse
type deleteTeamProfileResponse struct {
	// in: body
	Body struct {
		// error occuring throughout request lifecycle
		// required: true
		// example: unable to find team account to delete
		Error error `jsons:"error"`
	}
}

// swagger:route DELETE /v1/team/profile/{id} Team deleteTeamProfileRequest
// Deletes Team Profile
//
// Deletes a team profile and all related associations in the backend data store
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: deleteTeamProfileResponse
// creates a user subscription record and ties it to a user account
func (s *Server) deleteTeamProfileHandler(w http.ResponseWriter, r *http.Request) {
	// extract the account id to which the profile belongs to
	teamAccountId := requestutils.ExtractIDFromRequest(r)

	err := s.Db.DeleteTeamProfileByID(r.Context(), teamAccountId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response := DeleteTeamProfileResponse{
		err,
	}

	s.JSONResponse(w, r, response)
}
