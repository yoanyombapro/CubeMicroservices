package api

import (
	"net/http"

	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/errorutils"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

// Get user by id request
// swagger:parameters getUser
type GetUserByIdRequestSwagger struct {
	// id of the user account to get
	// in: query
	// required: true
	Id uint32 `json:"result"`
}

// swagger:route GET /v1/user/{id} User getUser
// Returns a user account by id
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: userOperationResponse
// gets a user by account id
func (s *Server) getUserAccountHandler(w http.ResponseWriter, r *http.Request) {
	var (
		userAccount *model.User
		response    UserOperationResponse
	)

	id := requestutils.ExtractIDFromRequest(r)

	userAccount, err := s.Db.GetUserByID(r.Context(), id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if s.config.EnableAuthServicePrivateIntegration {
		// Obtain the account of interest from the authentication service as it
		// is the single source of truth for all authentication records tied to a
		// user
		aggregatedErr, authnAccount := s.Auth.GetAccount(userAccount.AuthnAccountId, userAccount.UserName, userAccount.Password)
		if aggregatedErr != nil {
			if errorutils.ProcessAggregatedErrors(w, aggregatedErr) {
				return
			}
		}

		// once the account is obtained, we assert that it is not locked or deleted
		// this is important to enforce that consistency at the record level is witheld
		// throughout the entire backend
		if authnAccount == nil {
			http.Error(w, "account authentication records do not exist", http.StatusNotFound)
			return
		}

		if authnAccount.Result.Locked {
			http.Error(w, "account is locked", http.StatusNotFound)
			return
		} else if authnAccount.Result.Deleted {
			http.Error(w, "account is deleted", http.StatusNotFound)
			return
		}
	}

	response.Error = err
	response.User = *userAccount

	// store the request in the database
	s.JSONResponse(w, r, response)
}
