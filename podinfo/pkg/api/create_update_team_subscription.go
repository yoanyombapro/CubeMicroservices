package api

import (
	"net/http"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/helper/customerror"
	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type CreateTeamSubscriptionRequest struct {
	Subscription model.Subscriptions `json:"payload"`
}

type CreateTeamSubscriptionResponse struct {
	Error error      `json:"error"`
	Team  model.Team `json:"payload"`
}

// swagger:response createTeamSubscriptionResponse
type createTeamSubscriptionResponse struct {
	// the subscription of interest created
	// in: body
	Body struct {
		// error arising from the operation
		// required: true
		Error error `json:"error"`
		// team to which subscription is tied to
		// required: true
		Team model.Team `json:"result"`
	}
}

// Create team subscription request object
// swagger:parameters createTeamSubscriptionRequest
type createTeamSubscriptionRequestSwagger struct {
	// in: body
	Body struct {
		// errors arising during operation
		// required: true
		Error error `json:"error"`
		// subscriptions to update
		// required: true
		Subscription model.Subscriptions `json:"result"`
	}
	// team account id
	// in: query
	// required: true
	TeamAccountId uint32
}

// swagger:route POST /v1/team/subscription/{id} Team createTeamSubscriptionRequest
//
// Creates Team Subscription
//
// creates a subscriptions and ties it to a given team account by id
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: createTeamSubscriptionResponse
// 400: genericError
// 404: genericError
// 403: genericError
// 406: genericError
// 401: genericError
// 500: genericError
// creates a profile for a team account in the backend datastore
func (s *Server) createOrUpdateTeamSubscriptionsHandler(w http.ResponseWriter, r *http.Request) {
	var (
		createTeamSubscriptionRequest CreateTeamSubscriptionRequest
	)

	id := requestutils.ExtractIDFromRequest(r)

	// decode the data present in the body
	err := customerror.DecodeJSONBody(w, r, &createTeamSubscriptionRequest)
	if err != nil {
		requestutils.ProcessMalformedRequest(w, err)
		return
	}

	team, err := s.Db.CreateOrUpdateTeamSubscription(r.Context(), id, createTeamSubscriptionRequest.Subscription)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response := CreateTeamSubscriptionResponse{
		nil,
		*team,
	}

	s.JSONResponse(w, r, response)
}
