package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
)

func TestDeleteTeamProfile(t *testing.T) {
	var (
		createProfileResponse     CreateTeamProfileResponse
		deleteTeamProfileResponse DeleteTeamProfileResponse
	)

	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	teamAccountId, token, err := CreateTeamAndOptionalTeamProfile(t, srv, createProfileResponse, true)
	if err != nil {
		fmt.Println(err)
		return
	}

	response := DeleteTeamProfile(teamAccountId, token, srv)
	CheckResponseCode(t, http.StatusOK, response.Code)

	json.Unmarshal(response.Body.Bytes(), &deleteTeamProfileResponse)

	if deleteTeamProfileResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", deleteTeamProfileResponse.Error)
	}

	// attempt to get the team profile which should fail
	response = GetTeamProfile(teamAccountId, token, srv)
	CheckResponseCode(t, http.StatusNotFound, response.Code)

	DeleteTeamSuccessfully(teamAccountId, token, srv, t)
}

func DeleteTeamSuccessfully(teamAccountId uint32, token string, srv *Server, t *testing.T) {
	var deleteTeamResponse DeleteTeamResponse
	response := DeleteTeam(teamAccountId, token, srv)
	CheckResponseCode(t, http.StatusOK, response.Code)

	json.Unmarshal(response.Body.Bytes(), &deleteTeamResponse)

	if deleteTeamResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", deleteTeamResponse.Error)
	}
}
