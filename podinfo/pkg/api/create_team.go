package api

import (
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/contracts/proto/contracts"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/helper"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/helper/customerror"
	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type CreateTeamRequest struct {
	Team    model.Team `json:"team"`
	AdminId uint32     `json:"adminId"`
}

type CreateTeamResponse struct {
	Id    uint32 `json:"id"`
	Error error  `json:"error"`
}

// swagger:parameters createTeam
type createTeamRequest struct {
	// in: body
	Body struct {
		// team account
		// required: true
		Team model.Team `json:"team"`
	}
}

// team created
// swagger:response createTeamResponse
type createTeamResponse struct {
	// in: body
	Body struct {
		// user account id
		// required: true
		// example: 20
		Id uint32 `json:"id"`
		// error
		// required: true
		// example: team account record already exists
		Error error `json:"error"`
	}
}

// swagger:route POST /v1/team Team createTeam
//
// Create Team
//
// creates a team account record in the backend database
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: createTeamResponse
// 400: genericError
// 404: genericError
// 403: genericError
// 406: genericError
// 401: genericError
// 500: genericError
// createTeamHandler creates a team account in the backend datastore
func (s *Server) createTeamHandler(w http.ResponseWriter, r *http.Request) {
	var (
		createTeamRequest CreateTeamRequest
		team              model.Team
	)

	q := r.URL.Query()

	adminId, _ := strconv.Atoi(q.Get("adminId"))
	adminUserAccountId := uint32(adminId)

	// extract the authn_id from the jwt token in the request header
	err, authnId := s.ExtractJwtFromHeader(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// decode the data present in the body
	err = customerror.DecodeJSONBody(w, r, &createTeamRequest)
	if err != nil {
		requestutils.ProcessMalformedRequest(w, err)
		return
	}

	if &createTeamRequest == nil {
		http.Error(w, "error parsing request object", http.StatusBadRequest)
		return
	}

	team = createTeamRequest.Team
	if team.Email == "" || team.Name == "" {
		http.Error(w, "invalid input parameters. please specify a username and email", http.StatusBadRequest)
		return
	}

	// assert to the auth service, the team exists as a user must first signup through
	// the auth service before being created on the user management service
	if s.config.Production {
		if s.TeamExistsToAuthService(w, &team) {
			return
		}
	}

	password, err := helper.ValidateAndHashPassword(team)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	team.Password = password
	team.AuthnAccountId = authnId

	teamAccountId, err := s.Db.CreateTeam(r.Context(), team, adminUserAccountId)
	if err != nil {
		// TODO: frontend should reattempt this operation if we fail here as long as failure is not due to
		// an account already existing in the backend
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// TODO: move exchange to a data structure that can be easily accessible
	msg := contracts.EmailContract{SenderEmailAddress: s.config.ServiceEmail, TargetEmailAddress: team.Email, Subject: "Welcome To The Cube Platform", Message: "We are happy to have you"}
	msgByteStr := []byte(fmt.Sprintf("%v", msg))
	err = s.MessagingClient.PublishOnQueue(nil, msgByteStr, "Email-Service")
	if err != nil {
		// TODO: implement this as a retryable operation
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// communicate with the email service. Ensure that the email body message
	// is templatized and as granular as possible
	response := CreateTeamResponse{
		teamAccountId,
		err,
	}

	s.JSONResponse(w, r, response)
}
