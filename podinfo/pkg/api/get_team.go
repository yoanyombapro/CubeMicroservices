package api

import (
	"net/http"

	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/errorutils"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type GetTeamResponse struct {
	Team  model.Team `json:"payload"`
	Error error      `json:"error"`
}

// swagger:parameters getTeamRequest
type getTeamRequest struct {
	// in: query
	TeamAccountId uint32 `json:"accountId`
}

// swagger:response getTeamResponse
type getTeamResponse struct {
	// in: body
	Body struct {
		// error
		// required: true
		Error error `json:"error"`
		// team of interest
		Team model.Team `json:"team"`
	}
}

// swagger:route GET /v1/team/{id} Team getTeamRequest
// Gets A Team Account
//
// Obtains a team account from the backend database by team account id
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: getTeamResponse
// gets a team of interest by id
func (s *Server) getTeamHandler(w http.ResponseWriter, r *http.Request) {
	var (
		team *model.Team
	)

	id := requestutils.ExtractIDFromRequest(r)

	if id == 0 {
		http.Error(w, "invalid team id", http.StatusBadRequest)
	}

	team, err := s.Db.GetTeamByID(r.Context(), id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	if s.config.Production {
		if s.TeamExistsToAuthService(w, team) {
			return
		}
	}

	response := GetTeamResponse{
		Team:  *team,
		Error: err,
	}

	s.JSONResponse(w, r, response)
}

func (s *Server) TeamExistsToAuthService(w http.ResponseWriter, team *model.Team) bool {
	// Obtain the account of interest from the authentication service as it
	// is the single source of truth for all authentication records tied to a
	// user
	aggregatedErr, authnAccount := s.Auth.GetAccount(team.AuthnAccountId, team.Name, team.Password)
	if aggregatedErr != nil {
		if errorutils.ProcessAggregatedErrors(w, aggregatedErr) {
			return true
		}
	}

	// once the account is obtained, we assert that it is not locked or deleted
	// this is important to enforce that consistency at the record level is witheld
	// throughout the entire backend
	if authnAccount == nil {
		http.Error(w, "account authentication records do not exist", http.StatusNotFound)
		return true
	}

	if authnAccount.Result.Locked {
		http.Error(w, "account is locked", http.StatusNotFound)
		return true
	} else if authnAccount.Result.Deleted {
		http.Error(w, "account is deleted", http.StatusNotFound)
		return true
	}
	return false
}
