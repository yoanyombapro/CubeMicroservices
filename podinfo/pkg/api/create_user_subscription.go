package api

import (
	"net/http"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/helper/customerror"
	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type CreateUserSubscriptionRequest struct {
	Subscription model.Subscriptions `json:"result"`
}

type CreateUserSubscriptionResponse struct {
	Error        error                  `json:"error"`
	Subscription []*model.Subscriptions `json:"result"`
}

// swagger:parameters createUserSubscriptionRequest
type createUserSubscriptionRequestSwagger struct {
	// the profile of interest to create
	// in: body
	Body struct {
		// subscription to create
		// required: true
		Subscription model.SubscriptionsORM `json:"result"`
	}
	// user account to tie the subscription to
	// in: query
	UserAccountId uint32
}

// swagger:response createUserSubscriptionResponse
type createUserSubscriptionResponse struct {
	// the profile of interest to create
	// in: body
	Body struct {
		// subscription to create
		// required: true
		Subscription []model.SubscriptionsORM `json:"result"`
		// error occuring throughout request lifecycle
		// required: true
		Error error `json:"error"`
	}
}

// swagger:route POST /v1/user/subscription/{id} User Subscription createUserSubscriptionRequest
// Creates a user subscription record and ties it to a user account
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: createUserSubscriptionResponse
// creates a user subscription record and ties it to a user account
func (s *Server) createUserSubscriptionHandler(w http.ResponseWriter, r *http.Request) {
	var (
		createSubscriptionRequest  CreateUserSubscriptionRequest
		createSubscriptionResponse CreateUserSubscriptionResponse
	)

	// obtain the user account id to which this subscription is tied to
	// from the request id
	userAccountId := requestutils.ExtractIDFromRequest(r)

	// decode the request body
	err := customerror.DecodeJSONBody(w, r, &createSubscriptionRequest)
	if err != nil {
		requestutils.ProcessMalformedRequest(w, err)
		return
	}

	err = s.Db.CreateUserSubscription(r.Context(), userAccountId, createSubscriptionRequest.Subscription)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user, err := s.Db.GetUserByID(r.Context(), userAccountId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	createSubscriptionResponse.Error = err
	createSubscriptionResponse.Subscription = user.Subscriptions

	s.JSONResponse(w, r, createSubscriptionResponse)
}
