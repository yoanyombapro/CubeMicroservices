package api

import (
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/errorutils"
)

type OperationResponse struct {
	Error error `json:"error"`
}

// Delete user by id request
// swagger:parameters deleteUser
type DeleteUserByIdRequestSwagger struct {
	// id of the user account to delete
	// in: query
	// required: true
	Id uint32 `json:"result"`
}

// swagger:route DELETE /v1/user/{id} User deleteUser
// Updates a user account present in the backend database
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: userOperationResponse
// gets a user by account id
func (s *Server) deleteUserAccountHandler(w http.ResponseWriter, r *http.Request) {
	var response OperationResponse

	vars := mux.Vars(r)
	id, _ := vars["id"]
	processedId, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Ensure the user account actually exists
	exist, userAccount, err := s.Db.GetUserIfExists(uint32(processedId), "", "")
	if !exist {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Important to first attempt deletion of the credentials through the authentication
	// service and then to the record in the user's service database
	aggregatedErr := s.Auth.DeleteAccount(userAccount.UserName, userAccount.Password, userAccount.AuthnAccountId)
	if aggregatedErr != nil {
		if errorutils.ProcessAggregatedErrors(w, aggregatedErr) {
			return
		}
	}

	err = s.Db.DeleteUser(r.Context(), uint32(processedId))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response.Error = err
	s.JSONResponse(w, r, response)
}
