package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
)

func TestGetUser(t *testing.T) {
	var userResponse UserOperationResponse
	numUsersToCreate := 1

	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	_, createdUsers := createUsersThroughApi(srv, numUsersToCreate)

	for _, createdUser := range createdUsers {
		if createdUser != nil {
			// get the use by Id
			id := fmt.Sprint(createdUser.User.Id)
			// specify url
			url := "/v1/user/" + id
			// attempt to obtain the user again from the database through id
			// set up http request
			req, _ := http.NewRequest("GET", url, nil)
			req.Header.Set("Content-Type", "application/json")
			// perform the request
			response := ExecuteRequest(req, srv.getUserAccountHandler)

			// check the response code
			CheckResponseCode(t, http.StatusOK, response.Code)

			// unmarshall the response and assert that there are no errors
			_ = json.Unmarshal(response.Body.Bytes(), &userResponse)

			if userResponse.Error != nil {
				t.Errorf("Expected empty error field to be returned'. Got '%v'", userResponse.Error)
			}
		}
	}
}
