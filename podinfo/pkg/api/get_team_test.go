package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
)

func TestGetTeam(t *testing.T) {
	var (
		createProfileResponse CreateTeamProfileResponse
		getTeamResponse       GetTeamResponse
	)

	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	teamAccountId, token, err := CreateTeamAndOptionalTeamProfile(t, srv, createProfileResponse, false)
	if err != nil {
		fmt.Println(err)
		return
	}

	// obtain the team but it should not contain a profile reference
	response := GetTeam(teamAccountId, token, srv)
	CheckResponseCode(t, http.StatusOK, response.Code)
	// transform the response
	_ = json.Unmarshal(response.Body.Bytes(), &getTeamResponse)
	if getTeamResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", getTeamResponse.Error)
	}

	if getTeamResponse.Team.Id != teamAccountId {
		t.Errorf("Expected the id to remain the same (%v). Got %v", fmt.Sprint(getTeamResponse.Team.Id), fmt.Sprint(teamAccountId))
	}
}
