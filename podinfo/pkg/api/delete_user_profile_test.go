package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
)

func TestDeleteUserProfileTest(t *testing.T) {
	var (
		createUserProfileResponse CreateUserProfileResponse
		deleteUserProfileResponse OperationResponse
	)

	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	userAccountId, profileId, token, err := CreateUserAndOptionalUserProfile(t, srv, createUserProfileResponse, true)
	if err != nil {
		fmt.Println(err)
		return
	}

	response := DeleteUserProfile(userAccountId, profileId, token, srv)
	CheckResponseCode(t, http.StatusOK, response.Code)

	json.Unmarshal(response.Body.Bytes(), &deleteUserProfileResponse)

	if deleteUserProfileResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", deleteUserProfileResponse.Error)
	}

	// attempt to get the user profile which should fail
	response = GetUserProfile(userAccountId, token, srv)
	CheckResponseCode(t, http.StatusNotFound, response.Code)
}
