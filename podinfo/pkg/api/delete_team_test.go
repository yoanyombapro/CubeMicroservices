package api

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestDeleteTeam(t *testing.T) {
	var (
		createProfileResponse CreateTeamProfileResponse
		deleteTeamResponse    DeleteTeamResponse
	)

	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	teamAccountId, token, err := CreateTeamAndOptionalTeamProfile(t, srv, createProfileResponse, false)
	if err != nil {
		fmt.Println(err)
		return
	}

	response := DeleteTeam(teamAccountId, token, srv)
	// CheckResponseCode(t, http.StatusOK, response.Code)

	json.Unmarshal(response.Body.Bytes(), &deleteTeamResponse)

	if deleteTeamResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", deleteTeamResponse.Error)
	}

}
