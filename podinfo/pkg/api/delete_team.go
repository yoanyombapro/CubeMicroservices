package api

import (
	"net/http"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/errorutils"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type DeleteTeamResponse struct {
	Error error `json:"error"`
}

// swagger:parameters deleteTeamRequest
type deleteTeamRequest struct {
	// team account id to delete
	// in: query
	// required: true
	// example: 123
	teamAccountId uint32 `jsons:"id"`
}

// swagger:response deleteTeamResponse
type deleteTeamResponse struct {
	// in: body
	Body struct {
		// error occuring throughout request lifecycle
		// required: true
		// example: unable to find team account to delete
		Error error `jsons:"error"`
	}
}

// swagger:route DELETE /v1/team/{id} Team deleteTeamRequest
// Deletes Team Account
//
// Deletes a team account and all related associations in the backend data store
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: deleteTeamResponse
// creates a user subscription record and ties it to a user account
func (s *Server) deleteTeamHandler(w http.ResponseWriter, r *http.Request) {
	id := requestutils.ExtractIDFromRequest(r)

	if id == 0 {
		http.Error(w, "invalid team id", http.StatusBadRequest)
	}

	// Ensure the user account actually exists
	teamAccount, err := s.Db.GetTeamByID(r.Context(), id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if s.config.Production {
		// Important to first attempt deletion of the credentials through the authentication
		// service and then to the record in the user's service database
		aggregatedErr := s.Auth.DeleteAccount(teamAccount.Name, teamAccount.Password, teamAccount.AuthnAccountId)
		if aggregatedErr != nil {
			if errorutils.ProcessAggregatedErrors(w, aggregatedErr) {
				return
			}
		}
	}

	err = s.Db.DeleteTeamByID(r.Context(), id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response := DeleteTeamResponse{
		Error: err,
	}

	s.JSONResponse(w, r, response)
}
