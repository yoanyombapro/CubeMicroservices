package api

import (
	"net/http"

	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type GetTeamProfileResponse struct {
	Error       error             `json:"error"`
	TeamProfile model.TeamProfile `json:"payload"`
}

// swagger:parameters getTeamProfileRequest
type getTeamProfileRequest struct {
	// in: query
	TeamAccountId uint32 `json:"accountId`
}

// swagger:response getTeamProfileResponse
type getTeamProfileResponse struct {
	// in: body
	Body struct {
		// error
		// required: true
		Error error `json:"error"`
		// team profile of interest
		// required: true
		TeamProfile model.TeamProfile `json:"team"`
	}
}

// swagger:route GET /v1/team/profile/{id} Team getTeamProfileResponse
// Gets A Team Profile
//
// Obtains a team profile from the backend database by team account id it is tied to
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: getTeamProfileResponse
// gets a team of interest by id
func (s *Server) getTeamProfileHandler(w http.ResponseWriter, r *http.Request) {
	// extract the team account id from the request url
	teamAccountId := requestutils.ExtractIDFromRequest(r)

	teamProfile, err := s.Db.GetTeamProfileByID(r.Context(), teamAccountId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	response := GetTeamProfileResponse{
		nil,
		*teamProfile,
	}

	s.JSONResponse(w, r, response)
}
