package api

import (
	"net/http"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/helper/customerror"
	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type CreateTeamProfileRequest struct {
	Profile model.TeamProfile `json:"payload"`
}

type CreateTeamProfileResponse struct {
	Id    uint32 `json:"id"`
	Error error  `json:"error"`
}

// Team profile request payload
// swagger:parameters createTeamProfile
type createTeamProfileRequest struct {
	// in: body
	Body struct {
		// team profile
		// required: true
		Profile model.TeamProfile `json:"payload"`
	}
}

// Create team profile successfully
// swagger:response createTeamProfileResponse
type createTeamProfileResponse struct {
	// in: body
	Body struct {
		// team profile id
		// required: true
		// example: 13123
		Id uint32 `json:"id"`
		// error
		// required: true
		// example: profile already exists
		Error error `json:"error"`
	}
}

// swagger:route POST /v1/team/profile/{id} Team createTeamProfile
//
// Create Team Profile
//
// creates a profile for a team account record in the backend database
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: createTeamProfileResponse
// 400: genericError
// 404: genericError
// 403: genericError
// 406: genericError
// 401: genericError
// 500: genericError
// creates a profile for a team account in the backend datastore
func (s *Server) createTeamProfileHandler(w http.ResponseWriter, r *http.Request) {
	var (
		createTeamProfileRequest CreateTeamProfileRequest
	)

	id := requestutils.ExtractIDFromRequest(r)

	// decode the data present in the body
	err := customerror.DecodeJSONBody(w, r, &createTeamProfileRequest)
	if err != nil {
		requestutils.ProcessMalformedRequest(w, err)
		return
	}

	teamProfileId, err := s.Db.CreateTeamProfile(r.Context(), id, createTeamProfileRequest.Profile)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response := CreateTeamProfileResponse{
		teamProfileId,
		err,
	}

	s.JSONResponse(w, r, response)
}
