package api

import (
	"net/http"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/helper/customerror"
	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type CreateUserProfileRequest struct {
	UserProfile model.Profile `json:"result"`
}

type CreateUserProfileResponse struct {
	UserProfile model.Profile `json:"result"`
	Error       error         `json:"error"`
}

// Create user profile request
// swagger:parameters createProfile
type createUserProfileRequest struct {
	// the profile of interest to create
	// in: body
	Body struct {
		// user profile
		// required: true
		UserProfile model.ProfileORM `json:"payload"`
	}
	// user account id to which this profile will be tied to
	// in: query
	UserAccountId uint32
}

// swagger:response createProfileResponse
type createUserProfileResponse struct {
	// the profile of interest to create
	// in: body
	Body struct {
		// the created user profile
		// required: true
		UserProfile model.Profile `json:"result"`
		// required: true
		Error error `json:"error"`
	}
}

// swagger:route POST /v1/user/profile/{id} User UserProfile createProfile
// Creates a user profile record and ties it to a user account
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: createProfileResponse
// creates a user profile record and ties it to a user account
func (s *Server) createUserProfileHandler(w http.ResponseWriter, r *http.Request) {
	var (
		profile               *model.Profile
		createProfileRequest  CreateUserProfileRequest
		createProfileResponse CreateUserProfileResponse
	)

	// obtain the user account id to which this profile is tied to
	// from the request id
	userAccountId := requestutils.ExtractIDFromRequest(r)

	// decode the request body
	err := customerror.DecodeJSONBody(w, r, &createProfileRequest)
	if err != nil {
		requestutils.ProcessMalformedRequest(w, err)
		return
	}

	profile, err = s.Db.CreateUserProfile(r.Context(), userAccountId, createProfileRequest.UserProfile)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	createProfileResponse.UserProfile = *profile
	createProfileResponse.Error = err
	s.JSONResponse(w, r, createProfileResponse)
}
