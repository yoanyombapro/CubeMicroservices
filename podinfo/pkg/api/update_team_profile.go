package api

import (
	"net/http"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/helper/customerror"
	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type UpdateTeamProfileRequest struct {
	Profile model.TeamProfile `json:"payload"`
}

type UpdateTeamProfileResponse struct {
	Error   error      `json:"error"`
	Profile model.Team `json:"payload"`
}

// Update team profile request
// swagger:parameters updateTeamProfileRequest
type UpdateTeamProfileRequestSwagger struct {
	// in: body
	Body struct {
		// team profile to update
		// required: true
		TeamProfile model.TeamProfile `json:"payload"`
	}
	// in: query
	// required: true
	// example; 1243
	TeamId uint32 `json:"teamId"`
}

// Updated team profile response
// swagger:response updateTeamProfileResponse
type UpdateTeamProfileResponseSwagger struct {
	// in: body
	Body struct {
		// updated team profile
		// required: true
		TeamProfile model.TeamProfile `json:"result"`
		// error
		// required: true
		Error error `json:"error"`
	}
}

// swagger:route PUT /v1/team/profile/{id} Team updateTeamProfileRequest
// Updates Team
//
// Updates a team profile present in the backend database based on account id
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: updateTeamProfileResponse
// updates a team profile based on id
func (s *Server) updateTeamProfileHandler(w http.ResponseWriter, r *http.Request) {
	var (
		updateTeamProfileRequest UpdateTeamProfileRequest
	)

	id := requestutils.ExtractIDFromRequest(r)

	// decode the data present in the body
	err := customerror.DecodeJSONBody(w, r, &updateTeamProfileRequest)
	if err != nil {
		requestutils.ProcessMalformedRequest(w, err)
		return
	}

	updatedTeamProfile, err := s.Db.UpdateTeamProfile(r.Context(), id, updateTeamProfileRequest.Profile)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response := UpdateTeamProfileResponse{
		nil,
		*updatedTeamProfile,
	}

	s.JSONResponse(w, r, response)
}
