package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
)

func TestCreateUserProfile(t *testing.T) {
	var (
		createProfileResponse CreateUserProfileResponse
	)

	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	apiResponse, token, done := CreateTestUserBackendCall(t, srv)
	if done {
		return
	}

	if apiResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", apiResponse.Error)
	}

	userAccountId := apiResponse.Id

	err, jsonStr := CreateUserProfileAndConvertToJson()
	if err != nil {
		fmt.Println(err)
		return
	}

	url := "/v1/user/profile/" + fmt.Sprint(userAccountId)

	// set up http request
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	_, req = AttachIdToUrl(userAccountId, req)
	UpdateRequestHeaders(req, token)

	// perform request
	response := ExecuteRequest(req, srv.createUserProfileHandler)
	CheckResponseCode(t, http.StatusOK, response.Code)

	json.Unmarshal(response.Body.Bytes(), &createProfileResponse)

	if createProfileResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", createProfileResponse.Error)
	}
}
