package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
)

func TestGetTeamProfile(t *testing.T) {
	var (
		createProfileResponse  CreateTeamProfileResponse
		getTeamResponse        GetTeamResponse
		getTeamProfileResponse GetTeamProfileResponse
	)

	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	teamAccountId, token, err := CreateTeamAndOptionalTeamProfile(t, srv, createProfileResponse, true)
	if err != nil {
		fmt.Println(err)
		return
	}

	// obtain the team but it should not contain a profile reference
	response := GetTeam(teamAccountId, token, srv)
	CheckResponseCode(t, http.StatusOK, response.Code)
	// transform the response
	_ = json.Unmarshal(response.Body.Bytes(), &getTeamResponse)
	if getTeamResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", getTeamResponse.Error)
	}

	if getTeamResponse.Team.Id != teamAccountId {
		t.Errorf("Expected the id to remain the same (%v). Got %v", fmt.Sprint(getTeamResponse.Team.Id), fmt.Sprint(teamAccountId))
	}

	if getTeamResponse.Team.TeamProfile == nil {
		t.Errorf("Expected the team profile to be populated Got %v", getTeamResponse.Team.TeamProfile)
	}

	// attempt to get the team profile which should fail
	response = GetTeamProfile(teamAccountId, token, srv)
	CheckResponseCode(t, http.StatusOK, response.Code)

	_ = json.Unmarshal(response.Body.Bytes(), &getTeamProfileResponse)
	if getTeamProfileResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", getTeamResponse.Error)
	}

	if getTeamProfileResponse.TeamProfile.Id == 0 {
		t.Errorf("Expected the id to be non zero. Got %v", fmt.Sprint(getTeamProfileResponse.TeamProfile.Id))
	}
}
