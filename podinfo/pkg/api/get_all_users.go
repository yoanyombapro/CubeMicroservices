package api

import (
	"net/http"
	"strconv"

	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
)

type GetAllUsersResponse struct {
	Error error        `json:"error"`
	Users []model.User `json:"result"`
}

// swagger:parameters getAllUsersRequest
type GetAllUsersRequestSwagger struct {
	// maximal number of records to return
	// in: query
	// required: true
	limit uint32
}

// swagger:parameters getAllUsersByIntentRequest
type GetAllUsersByIntentRequestSwagger struct {
	// maximal number of records to return
	// in: query
	// required: true
	limit uint32
	// user account intent
	// required: true
	intent string
}

// swagger:parameters getAllUsersByAccountTypeRequest
type GetAllUsersByAccountTypeRequestSwagger struct {
	// maximal number of records to return
	// in: query
	// required: true
	limit uint32
	// user account type
	// required: true
	accountType string
}

// swagger:response getAllUsersResponse
type GetAllUsersResponseSwagger struct {
	// in: body
	Body struct {
		// error
		// required: true
		Error error `json:"error"`
		// set of users
		// required: true
		Users []model.User `json:"result"`
	}
}

// swagger:route GET /v1/user?limit=xxx User getAllUsersRequest
// Gets a set of user records based on some specified limit
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: getAllUsersResponse
// gets a set of users from the backend database based on a limit
func (s *Server) getAllUsersHandler(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()

	limit, _ := strconv.Atoi(q.Get("limit"))
	maxUsers := uint32(limit)

	// query the database for the set of users on interest
	users, err := s.Db.GetAllUsers(r.Context(), maxUsers)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response := GetAllUsersResponse{
		err,
		users,
	}

	s.JSONResponse(w, r, response)
}

// swagger:route GET /v1/user?limit=xxx?intent=xxx User getAllUsersByIntentRequest
// Gets a set of user records based on some specified limit and intent
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: getAllUsersResponse
// gets a set of users from the backend database based on a limit and intent
func (s *Server) getAllUsersByIntentHandler(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()

	limit, _ := strconv.Atoi(q.Get("limit"))
	maxUsers := uint32(limit)

	intent := q.Get("intent")

	// query the database for the set of users on interest
	users, err := s.Db.GetAllUsersByIntent(r.Context(), intent, maxUsers)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response := GetAllUsersResponse{
		err,
		users,
	}
	s.JSONResponse(w, r, response)
}

// swagger:route GET /v1/user?limit=xxx?accountType=xxx User getAllUsersByAccountTypeRequest
// Gets a set of user records based on some specified limit and account type
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: getAllUsersResponse
// gets a set of users from the backend database based on a limit and intent
func (s *Server) getAllUsersByAccountTypeHandler(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()

	limit, _ := strconv.Atoi(q.Get("limit"))
	maxUsers := uint32(limit)

	accountType := q.Get("accountType")

	// query the database for the set of users on interest
	users, err := s.Db.GetAllUsersByAccountType(r.Context(), accountType, maxUsers)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response := GetAllUsersResponse{
		err,
		users,
	}
	s.JSONResponse(w, r, response)
}
