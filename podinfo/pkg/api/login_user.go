package api

import (
	"net/http"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/helper/customerror"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/errorutils"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type LoginUserRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginResponse struct {
	JwtToken string `json:"token"`
	Error    error  `json:"error"`
}

// Log in user
// swagger:parameters loginUserRequest
type loginUserRequest struct {
	// in: body
	Body struct {
		// user name
		// required: true
		// example: test-username
		Username string `json:"username"`
		// password
		// required: true
		// example: test-password
		Password string `json:"password"`
	}
}

// swagger:response loginUserResponse
type loginUserResponse struct {
	// in: body
	Body struct {
		// Jwt Token
		// required: true
		// example: kBxbjzKVDjvasgvds.askdhjaskjdgsagjcdgc.asjdjkasfgdas
		JwtToken string `json:"token"`
		// error
		// required: true
		// example: unable to get token
		Error error `json:"error"`
	}
}

// swagger:route POST /v1/login/user User loginUserRequest
// Log in user
//
// Logs in a user into the system
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: loginUserResponse
// logs in a team into the system
func (s *Server) loginUserHandler(w http.ResponseWriter, r *http.Request) {
	var (
		loginUserRequest LoginUserRequest
	)

	// decode the data present in the body
	err := customerror.DecodeJSONBody(w, r, &loginUserRequest)
	if err != nil {
		requestutils.ProcessMalformedRequest(w, err)
		return
	}

	if loginUserRequest.Password == "" || loginUserRequest.Username == "" {
		http.Error(w, "invalid input parameters. please specify a username and password", http.StatusBadRequest)
		return
	}

	aggregatedErr, token := s.Auth.Login(loginUserRequest.Username, loginUserRequest.Password)
	if aggregatedErr != nil {
		if errorutils.ProcessAggregatedErrors(w, aggregatedErr) {
			return
		}
	}

	loginResponse := LoginResponse{
		JwtToken: *token,
		Error:    nil,
	}

	// TODO: cross check that the hashed password version matches the password
	s.JSONResponse(w, r, loginResponse)
}
