package api

import (
	"net/http"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/errorutils"
)

// https://www.cloudjourney.io/articles/security/jwt_in_golang-su/

// TODO: Automate testing : https://martinheinz.dev/blog/5
func (s *Server) logoutHandler(w http.ResponseWriter, r *http.Request) {
	// hit authn log out endpoint and return
	aggregatedErr := s.Auth.LogOut()
	if aggregatedErr != nil {
		if errorutils.ProcessAggregatedErrors(w, aggregatedErr) {
			return
		}
	}

	s.JSONResponse(w, r, http.StatusOK)
}
