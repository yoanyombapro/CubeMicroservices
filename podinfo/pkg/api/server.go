// Package User Microservice API.
//
// This serves as the user's microservice api definition for the CUBE Platform
//
// Terms Of Service:
//
// there are no TOS at this moment, use at your own risk we take no responsibility
//
//     Schemes: http, https
//     Host: localhost
//     BasePath: /v1
//     Version: 1.0.0
//     License: MIT http://opensource.org/licenses/MIT
//     Contact: Yoan Yomba<yoanyombapro@gmail.com.com> http://CUBE.com
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - api_key:
//
//
//     Extensions:
//     x-meta-value: value
//     x-meta-array:
//       - value1
//       - value2
//     x-meta-array-obj:
//       - name: obj
//         value: field
//
// swagger:meta
package api

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"os"
	"strings"
	"sync/atomic"
	"time"

	"github.com/go-openapi/runtime/middleware"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"golang.org/x/crypto/acme/autocert"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"

	"gitlab.com/yoanyombapro/CubeMicroservices/common/counters/infrastructure"
	"gitlab.com/yoanyombapro/CubeMicroservices/common/messaging/rabbitmq"
	"gitlab.com/yoanyombapro/CubeMicroservices/common/monitoring"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/authentication"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/database"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/fscache"
	middleware2 "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/middleware"
)

var (
	healthy int32
	ready   int32
	watcher *fscache.Watcher
)

type FluxConfig struct {
	GitUrl    string `mapstructure:"git-url"`
	GitBranch string `mapstructure:"git-branch"`
}

type Config struct {
	HttpClientTimeout                   time.Duration `mapstructure:"http-client-timeout"`
	HttpServerTimeout                   time.Duration `mapstructure:"http-server-timeout"`
	HttpServerShutdownTimeout           time.Duration `mapstructure:"http-server-shutdown-timeout"`
	BackendURL                          []string      `mapstructure:"backend-url"`
	UILogo                              string        `mapstructure:"ui-logo"`
	UIMessage                           string        `mapstructure:"ui-message"`
	UIColor                             string        `mapstructure:"ui-color"`
	UIPath                              string        `mapstructure:"ui-path"`
	DataPath                            string        `mapstructure:"data-path"`
	ConfigPath                          string        `mapstructure:"config-path"`
	Port                                string        `mapstructure:"port"`
	PortMetrics                         int           `mapstructure:"port-metrics"`
	Hostname                            string        `mapstructure:"hostname"`
	H2C                                 bool          `mapstructure:"h2c"`
	RandomDelay                         bool          `mapstructure:"random-delay"`
	RandomError                         bool          `mapstructure:"random-error"`
	Unhealthy                           bool          `mapstructure:"unhealthy"`
	Unready                             bool          `mapstructure:"unready"`
	JWTSecret                           string        `mapstructure:"jwt-secret"`
	Production                          bool          `mapstructure:"production"`
	RedirectToHttps                     bool          `mapstructure:"redirectohttps"`
	ServiceName                         string        `mapstructure:"service-name"`
	ZipkinServerUrl                     string        `mapstructure:"zipkin-server-url"`
	AmqpServerUrl                       string        `mapstructure:"amqp-server-url"`
	EnableAuthServicePrivateIntegration bool          `mapstructure:"ENABLE_AUTH_SERVICE_PRIVATE_INTEGRATION"`
	ServiceEmail                        string        `mapstructure:"SERVICE_EMAIL"`
}

type Server struct {
	router          *mux.Router
	logger          *zap.Logger
	config          *Config
	Db              *database.Database
	Auth            *authentication.Authentication
	JwtConfig       *authentication.JwtConfiguration
	Keys            *authentication.JsonKeys
	InfraCounters   *infrastructure.InfrastructureCounters
	MessagingClient *rabbitmq.RabbitMQClient
}

func NewServer(
	config *Config,
	logger *zap.Logger,
	database *database.Database,
	messagingClient *rabbitmq.RabbitMQClient,
	infrastructureCounter *infrastructure.InfrastructureCounters,
	authServiceWrapper *authentication.Authentication) (*Server, error) {

	var srv *Server
	srv = &Server{
		router:          mux.NewRouter(),
		logger:          logger,
		config:          config,
		Db:              database,
		Auth:            authServiceWrapper,
		JwtConfig:       nil,
		InfraCounters:   infrastructureCounter,
		MessagingClient: messagingClient,
	}

	return srv, nil
}

func (s *Server) registerHandlers() {
	jwtMiddleware := middleware2.NewJwtMiddleware(s.JwtConfig.Issuer, s.config.JWTSecret)

	s.router.Handle("/metrics", promhttp.Handler())
	s.router.PathPrefix("/debug/pprof/").Handler(http.DefaultServeMux)
	s.router.HandleFunc("/", s.indexHandler).HeadersRegexp("User-Agent", "^Mozilla.*").Methods("GET")
	s.router.HandleFunc("/", s.infoHandler).Methods("GET")
	s.router.HandleFunc("/version", s.versionHandler).Methods("GET")
	s.router.HandleFunc("/echo", s.echoHandler).Methods("POST")
	s.router.HandleFunc("/env", s.envHandler).Methods("GET", "POST")
	s.router.HandleFunc("/headers", s.echoHeadersHandler).Methods("GET", "POST")
	s.router.HandleFunc("/healthz", s.healthzHandler).Methods("GET")
	s.router.HandleFunc("/readyz", s.readyzHandler).Methods("GET")
	s.router.HandleFunc("/readyz/enable", s.enableReadyHandler).Methods("POST")
	s.router.HandleFunc("/readyz/disable", s.disableReadyHandler).Methods("POST")
	s.router.HandleFunc("/panic", s.panicHandler).Methods("GET")
	s.router.HandleFunc("/status/{code:[0-9]+}", s.statusHandler).Methods("GET", "POST", "PUT").Name("status")
	s.router.HandleFunc("/store", s.storeWriteHandler).Methods("POST")
	s.router.HandleFunc("/store/{hash}", s.storeReadHandler).Methods("GET").Name("store")
	s.router.HandleFunc("/configs", s.configReadHandler).Methods("GET")
	s.router.HandleFunc("/token", s.tokenGenerateHandler).Methods("POST")
	s.router.HandleFunc("/token/validate", s.tokenValidateHandler).Methods("GET")
	s.router.HandleFunc("/api/info", s.infoHandler).Methods("GET")
	s.router.HandleFunc("/api/echo", s.echoHandler).Methods("POST")
	s.router.HandleFunc("/ws/echo", s.echoWsHandler)
	s.router.HandleFunc("/chunked", s.chunkedHandler)
	s.router.HandleFunc("/chunked/{wait:[0-9]+}", s.chunkedHandler)

	// Create Handlers
	s.router.HandleFunc("/v1/user/signup", s.createUserAccountHandler).Methods("POST")
	s.router.HandleFunc("/v1/user/profile/{id:[0-9]+}", jwtMiddleware.Handler(s.createUserProfileHandler)).Methods("POST")
	s.router.HandleFunc("/v1/user/subscription/{id:[0-9]+}", jwtMiddleware.Handler(s.createUserSubscriptionHandler)).Methods("POST")
	s.router.HandleFunc("/v1/team/signup", s.createTeamHandler).Queries("adminId", "{id:[0-9]+}").Methods("POST")
	s.router.HandleFunc("/v1/team/profile", jwtMiddleware.Handler(s.createTeamProfileHandler)).Methods("POST")
	s.router.HandleFunc("/v1/team/subscription/{id:[0-9]+}", jwtMiddleware.Handler(s.createOrUpdateTeamSubscriptionsHandler)).Methods("POST")
	s.router.HandleFunc("/v1/team/subscription/{id:[0-9]+}", jwtMiddleware.Handler(s.createOrUpdateTeamSubscriptionsHandler)).Methods("PUT")

	// Update Handlers
	s.router.HandleFunc("/v1/user/{id:[0-9]+}", jwtMiddleware.Handler(s.updatedUserAccountHandler)).Methods("PUT")
	s.router.HandleFunc("/v1/user/profile", jwtMiddleware.Handler(s.updateUserProfileHandler)).Queries("accountId", "{id:[0-9]+}", "profileId", "{id:[0-9]+}").Methods("PUT")
	s.router.HandleFunc("/v1/user/subscription", jwtMiddleware.Handler(s.updateUserSubscriptionHandler)).Queries("accountId", "{id:[0-9]+}", "subscriptionId", "{id:[0-9]+}").Methods("PUT")
	s.router.HandleFunc("/v1/team/{id:[0-9]+}", jwtMiddleware.Handler(s.updateTeamHandler)).Methods("PUT")
	s.router.HandleFunc("/v1/team/profile/{id:[0-9]+}", jwtMiddleware.Handler(s.updateTeamProfileHandler)).Methods("PUT")
	s.router.HandleFunc("/v1/team/subscription/{id:[0-9]+}", jwtMiddleware.Handler(s.createOrUpdateTeamSubscriptionsHandler)).Methods("PUT")

	// Delete Handlers
	s.router.HandleFunc("/v1/user/{id:[0-9]+}", jwtMiddleware.Handler(s.deleteUserAccountHandler)).Methods("DELETE")
	s.router.HandleFunc("/v1/user/profile", jwtMiddleware.Handler(s.deleteUserProfileHandler)).Queries("accountId", "{id:[0-9]+}", "profileId", "{id:[0-9]+}").Methods("DELETE")
	s.router.HandleFunc("/v1/team/{id:[0-9]+}", jwtMiddleware.Handler(s.deleteTeamHandler)).Methods("DELETE")
	s.router.HandleFunc("/v1/team/profile/{id:[0-9]+}", jwtMiddleware.Handler(s.deleteTeamProfileHandler)).Methods("DELETE")
	// TODO implement deleting user & team subscription
	s.router.HandleFunc("/v1/user/subscription", jwtMiddleware.Handler(s.deleteUserSubscriptionHandler)).Queries("accountId", "{id:[0-9]+}", "subscriptionId", "{id:[0-9]+}").Methods("DELETE")

	// Get Handlers
	s.router.HandleFunc("/v1/user/{id:[0-9]+}", jwtMiddleware.Handler(s.getUserAccountHandler)).Methods("GET")
	s.router.HandleFunc("/v1/user/profile/{id:[0-9]+}", jwtMiddleware.Handler(s.getUserProfileHandler)).Methods("GET")
	s.router.HandleFunc("/v1/user/subscription/{id:[0-9]+}", jwtMiddleware.Handler(s.getAllUserSubscriptionsHandler)).Methods("GET")
	s.router.HandleFunc("/v1/team/{id:[0-9]+}", jwtMiddleware.Handler(s.getTeamHandler)).Methods("GET")
	s.router.HandleFunc("/v1/team/profile/{id:[0-9]+}", jwtMiddleware.Handler(s.getTeamProfileHandler)).Methods("GET")
	// TODO implement get user & team subscription handlers

	// Get All Handlers
	s.router.HandleFunc("/v1/user", jwtMiddleware.Handler(s.getAllUsersHandler)).Queries("limit", "{id:[0-9]+}").Methods("GET")
	s.router.HandleFunc("/v1/user", jwtMiddleware.Handler(s.getAllUsersByIntentHandler)).Queries("limit", "{id:[0-9]+}", "intent", "{intent}").Methods("GET")
	s.router.HandleFunc("/v1/user/profile", jwtMiddleware.Handler(s.getAllUserProfilesHandler)).Queries("limit", "{id:[0-9]+}").Methods("GET")
	s.router.HandleFunc("/v1/user/profile", jwtMiddleware.Handler(s.getAllUserProfilesByNationalityTypeHandler)).Queries("limit", "{id:[0-9]+}", "nationality", "{nationality}").Methods("GET")
	s.router.HandleFunc("/v1/user/profile", jwtMiddleware.Handler(s.getAllUserProfilesByProfileTypeHandler)).Queries("limit", "{id:[0-9]+}", "profileType", "{profileType}").Methods("GET")

	// authentication
	s.router.HandleFunc("/v1/login/user", s.loginUserHandler).Methods("POST")
	s.router.HandleFunc("/v1/login/team", s.loginTeamHandler).Methods("POST")
	s.router.HandleFunc("/v1/logout", s.logoutHandler).Methods("DELETE")

	// TODO implement groups functionality

	// Add tracing to all routes
	_ = s.router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		route.Handler(monitoring.TracingMiddleware(route.GetName(), route.GetHandler()))
		methods, err := route.GetMethods()
		if err != nil {
			return err
		}

		if len(methods) < 1 {
			return errors.New("no matching methods for route")
		}

		route.Handler(monitoring.MonitoringMiddleware(s.config.ServiceName, route.GetName(), methods[0]+" "+route.GetName(), route.GetHandler()))
		return nil
	})

	// server swagger files in a seamless manner
	ops := middleware.RedocOpts{SpecURL: "/swagger.yaml", Title: "User's Microservice API"}
	sh := middleware.Redoc(ops, nil)
	s.router.Handle("/docs", sh)
	s.router.Handle("/swagger.yaml", http.FileServer(http.Dir("./pkg/api/docs")))

	// TODO register counters here
	s.InfraCounters.RequestCounters.RegisterMetrics()
	s.InfraCounters.WorkerCounters.RegisterMetrics()
}

func (s *Server) registerMiddlewares() {
	prom := middleware2.NewPrometheusMiddleware()
	s.router.Use(prom.Handler)
	httpLogger := middleware2.NewLoggingMiddleware(s.logger)
	s.router.Use(httpLogger.Handler)
	s.router.Use(middleware2.VersionMiddleware)

	/*
		if s.config.RandomDelay {
			s.router.Use(randomDelayMiddleware)
		}
		if s.config.RandomError {
			s.router.Use(randomErrorMiddleware)
		}
	*/

}

func (s *Server) makeServerFromMux(mux *mux.Router) *http.Server {
	// set timeouts so that a slow or malicious client doesn't
	// hold resources forever
	return &http.Server{
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 5 * time.Second,
		IdleTimeout:  120 * time.Second,
		Handler:      mux,
	}
}

func (s *Server) makeHTTPServer() *http.Server {
	return s.makeServerFromMux(s.router)
}

func (s *Server) makeHTTPToHTTPSRedirectServer() *http.Server {
	handleRedirect := func(w http.ResponseWriter, r *http.Request) {
		newURI := "https://" + r.Host + r.URL.String()
		http.Redirect(w, r, newURI, http.StatusFound)
	}
	s.router.HandleFunc("/", handleRedirect)
	return s.makeServerFromMux(s.router)
}

func (s *Server) ListenAndServe(stopCh <-chan struct{}) {
	var m *autocert.Manager
	var httpsSrv *http.Server

	go s.startMetricsServer()

	s.registerHandlers()
	s.registerMiddlewares()

	var handler http.Handler
	if s.config.H2C {
		handler = h2c.NewHandler(s.router, &http2.Server{})
	} else {
		handler = s.router
	}

	srv := &http.Server{
		Addr:         ":" + s.config.Port,
		WriteTimeout: s.config.HttpServerTimeout,
		ReadTimeout:  s.config.HttpServerTimeout,
		IdleTimeout:  2 * s.config.HttpServerTimeout,
		Handler:      handler,
	}

	if s.config.Production {
		if s.config.RedirectToHttps {
			s.logger.Info("Redirecting all http traffic to https")
			handleRedirect := func(w http.ResponseWriter, r *http.Request) {
				newURI := "https://" + r.Host + r.URL.String()
				http.Redirect(w, r, newURI, http.StatusFound)
			}
			s.router.HandleFunc("/", handleRedirect)
			handler = s.router
		}

		hostPolicy := func(ctx context.Context, host string) error {
			// Note: change to your real host
			allowedHost := "localhost"
			if host == allowedHost {
				return nil
			}
			return fmt.Errorf("acme/autocert: only %s host is allowed", allowedHost)
		}

		dataDir := "."
		m = &autocert.Manager{
			Prompt:     autocert.AcceptTOS,
			HostPolicy: hostPolicy,
			Cache:      autocert.DirCache(dataDir),
		}

		httpsSrv = s.makeHTTPServer()
		httpsSrv.Addr = ":443"
		httpsSrv.TLSConfig = &tls.Config{GetCertificate: m.GetCertificate}

		// run https server in background
		go func() {
			s.logger.Info("Starting https server")
			if err := httpsSrv.ListenAndServeTLS("", ""); err != http.ErrServerClosed {
				s.logger.Fatal("HTTPS server crashed", zap.Error(err))
			}
		}()
	}

	//s.printRoutes()

	// load configs in memory and start watching for changes in the config dir
	if stat, err := os.Stat(s.config.ConfigPath); err == nil && stat.IsDir() {
		var err error
		watcher, err = fscache.NewWatch(s.config.ConfigPath)
		if err != nil {
			s.logger.Error("config watch error", zap.Error(err), zap.String("path", s.config.ConfigPath))
		} else {
			watcher.Watch()
		}
	}

	// run http server in background
	go func() {
		s.logger.Info("Starting http server")
		if err := srv.ListenAndServe(); err != http.ErrServerClosed {
			s.logger.Fatal("HTTP server crashed", zap.Error(err))
		}
	}()

	// signal Kubernetes the server is ready to receive traffic
	if !s.config.Unhealthy {
		atomic.StoreInt32(&healthy, 1)
	}
	if !s.config.Unready {
		atomic.StoreInt32(&ready, 1)
	}

	// wait for SIGTERM or SIGINT
	<-stopCh
	ctx, cancel := context.WithTimeout(context.Background(), s.config.HttpServerShutdownTimeout)
	defer cancel()

	// all calls to /healthz and /readyz will fail from now on
	atomic.StoreInt32(&healthy, 0)
	atomic.StoreInt32(&ready, 0)

	s.logger.Info("Shutting down HTTP & HTTPS server", zap.Duration("timeout", s.config.HttpServerShutdownTimeout))

	// wait for Kubernetes readiness probe to remove this instance from the load balancer
	// the readiness check interval must be lower than the timeout
	if viper.GetString("level") != "debug" {
		time.Sleep(3 * time.Second)
	}

	// attempt graceful shutdown
	if err := srv.Shutdown(ctx); err != nil {
		s.logger.Warn("HTTP server graceful shutdown failed", zap.Error(err))
	} else {
		s.logger.Info("HTTP server stopped")
	}

	if s.config.Production {
		// attempt graceful shutdown
		if err := httpsSrv.Shutdown(ctx); err != nil {
			s.logger.Warn("HTTPS server graceful shutdown failed", zap.Error(err))
		} else {
			s.logger.Info("HTTPS server stopped")
		}
	}
}

func (s *Server) startMetricsServer() {
	if s.config.PortMetrics > 0 {
		mux := http.DefaultServeMux
		mux.Handle("/metrics", promhttp.Handler())
		mux.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			w.Write([]byte("OK"))
		})

		srv := &http.Server{
			Addr:    fmt.Sprintf(":%v", s.config.PortMetrics),
			Handler: mux,
		}

		srv.ListenAndServe()
	}
}

func (s *Server) printRoutes() {
	s.router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		pathTemplate, err := route.GetPathTemplate()
		if err == nil {
			fmt.Println("ROUTE:", pathTemplate)
		}
		pathRegexp, err := route.GetPathRegexp()
		if err == nil {
			fmt.Println("Path regexp:", pathRegexp)
		}
		queriesTemplates, err := route.GetQueriesTemplates()
		if err == nil {
			fmt.Println("Queries templates:", strings.Join(queriesTemplates, ","))
		}
		queriesRegexps, err := route.GetQueriesRegexp()
		if err == nil {
			fmt.Println("Queries regexps:", strings.Join(queriesRegexps, ","))
		}
		methods, err := route.GetMethods()
		if err == nil {
			fmt.Println("Methods:", strings.Join(methods, ","))
		}
		fmt.Println()
		return nil
	})
}

func (s *Server) ExtractJwtFromHeader(r *http.Request) (error, uint32) {
	reqToken := r.Header.Get("Authorization")
	splitToken := strings.Split(reqToken, "Bearer")
	token := splitToken[1]
	// extract the id from the token
	id, err := authentication.ExtractClaims(token)
	if err != nil {
		return err, 0
	}
	return nil, id
}

type ArrayResponse []string
type MapResponse map[string]string
