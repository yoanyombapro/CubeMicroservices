package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
)

// TestTeamCreateProfile tests wether a team profile was successfully created
func TestTeamCreateProfile(t *testing.T) {
	var (
		getTeamResponse           GetTeamResponse
		createTeamProfileResponse CreateTeamProfileResponse
	)

	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	apiResponse, token, done := CreateTestTeamBackendCall(t, srv)
	if done {
		return
	}

	if apiResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", apiResponse.Error)
	}

	jsonStr, err := CreateTeamProfileAndConvertToJson()
	if err != nil {
		fmt.Println(err)
		return
	}

	teamID := apiResponse.Id

	url := "/v1/team/" + fmt.Sprint(teamID)

	req, _ := http.NewRequest("GET", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	_, req = AttachIdToUrl(teamID, req)
	UpdateRequestHeaders(req, token)

	response := ExecuteRequest(req, srv.createTeamProfileHandler)
	CheckResponseCode(t, http.StatusOK, response.Code)

	// transform the response
	_ = json.Unmarshal(response.Body.Bytes(), &createTeamProfileResponse)
	if createTeamProfileResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", getTeamResponse.Error)
	}
}
