package api

import (
	"bytes"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
	"time"

	"crypto/rand"

	"github.com/dgrijalva/jwt-go"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/authentication"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/database"
	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/errorutils"
)

// newUUID generates a random UUID according to RFC 4122
func NewUUID() (string, error) {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return "", err
	}
	// variant bits
	uuid[8] = uuid[8]&^0xc0 | 0x80
	// version 4 (pseudo-random)
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:]), nil
}

// ExecuteRequest executes an api request and returns a recorded response
func ExecuteRequest(req *http.Request, handle func(http.ResponseWriter, *http.Request)) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(handle)
	handler.ServeHTTP(rr, req)
	return rr
}

// AttachAuthTokenToHeader attaches an authorization token to a request header
func AttachAuthTokenToHeader(token *authentication.TokenId, req *http.Request) {
	bearerToken := "Bearer " + token.Result.Token
	// attach the token to the request header
	req.Header.Set("Authorization", bearerToken)
}

// DeleteCreatedEntities sets up GORM `onCreate` hook and return a function that can be deferred to
// remove all the entities created after the hook was set up
// You can use it as
//
// func TestSomething(t *testing.T){
//     db, _ := gorm.Open(...)
//
//     cleaner := DeleteCreatedEntities(db)
//     defer cleaner()
//
// }
func DeleteCreatedEntities(db *gorm.DB) func() {
	type entity struct {
		table   string
		keyname string
		key     interface{}
	}
	var entries []entity
	hookName := "cleanupHook"

	db.Callback().Create().After("gorm:create").Register(hookName, func(scope *gorm.Scope) {
		// fmt.Printf("Inserted entities of %s with %s=%v\n", scope.TableName(), scope.PrimaryKey(), scope.PrimaryKeyValue())
		entries = append(entries, entity{table: scope.TableName(), keyname: scope.PrimaryKey(), key: scope.PrimaryKeyValue()})
	})
	return func() {
		// Remove the hook once we're done
		defer db.Callback().Create().Remove(hookName)
		// Find out if the current db object is already a transaction
		_, inTransaction := db.CommonDB().(*sql.Tx)
		tx := db
		if !inTransaction {
			tx = db.Begin()
		}
		// Loop from the end. It is important that we delete the entries in the
		// reverse order of their insertion
		for i := len(entries) - 1; i >= 0; i-- {
			entry := entries[i]
			// fmt.Printf("Deleting entities from '%s' table with key %v\n", entry.table, entry.key)
			tx.Table(entry.table).Where(entry.keyname+" = ?", entry.key).Delete("")
		}

		if !inTransaction {
			tx.Commit()
		}
	}
}

// CheckResponseCode asserts that the expected response code matches the actual response code
func CheckResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

// CreateToken creates an access token (jwt) and attaches it to a jwt claims map
func CreateToken(userId int32) (string, error) {
	var err error
	// Creating Access Token
	os.Setenv("ACCESS_SECRET", "jdnfksdmfksd") // this should be in an env file
	atClaims := jwt.MapClaims{}
	atClaims["authorized"] = true
	atClaims["Bearer"] = userId
	atClaims["exp"] = time.Now().Add(time.Hour * 24).Unix()
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atClaims)
	token, err := at.SignedString([]byte(os.Getenv("ACCESS_SECRET")))
	if err != nil {
		return "", err
	}
	return token, nil
}

func InitializeMockServerAndDb() (*Server, func()) {
	srv := NewMockServer()

	_ = database.CreateTableForTests(srv.Db.Engine)

	// defer deleting all created entries
	cleaner := DeleteCreatedEntities(srv.Db.Engine)
	return srv, cleaner
}

func InitializeMockServer() *Server {
	return NewMockServer()
}

func AttachIdToUrl(id uint32, req *http.Request) (map[string]string, *http.Request) {
	// Hack to try to fake gorilla/mux vars
	vars := map[string]string{
		"id": fmt.Sprint(id),
	}

	req = mux.SetURLVars(req, vars)
	return vars, req
}

func CreateTeamProfileAndConvertToJson() ([]byte, error) {
	teamProfile := GenerateTestTeamProfile()
	createProfileRequest := CreateTeamProfileRequest{teamProfile}

	// marshall the struct into a json object
	jsonStr, err := json.Marshal(&createProfileRequest)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	return jsonStr, err
}

// create a random subscription and marshalls to json byte string
func CreateTeamSubscriptionAndConvertToJson() (error, []byte) {
	subscription := model.Subscriptions{
		Id:                   0,
		SubscriptionName:     "Smart News Subscriptions",
		SubscriptionStatus:   "Active",
		StartDate:            nil,
		EndDate:              nil,
		AccessType:           "Public",
		IsActive:             false,
		CreatedAt:            nil,
		DeletedAt:            nil,
		UpdatedAt:            nil,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	createTeamSubscriptionRequest := CreateTeamSubscriptionRequest{Subscription: subscription}
	jsonStr, err := json.Marshal(&createTeamSubscriptionRequest)
	if err != nil {
		return err, nil
	}

	return nil, jsonStr
}

// creates a team and marshalls to json byte string
func CreateTeamAndConvertToJson() (error, []byte) {
	randomId, err := NewUUID()
	if err != nil {
		return err, nil
	}

	team := model.Team{
		Id:                   0,
		CreatedAt:            nil,
		DeletedAt:            nil,
		UpdatedAt:            nil,
		Name:                 "test-team" + randomId,
		Tags:                 nil,
		Email:                "test@gmail.com" + randomId,
		Type:                 "investor" + randomId,
		Industry:             "tech" + randomId,
		FoundedDate:          nil,
		NumberOfEmployees:    0,
		Headquarters:         nil,
		Members:              nil,
		Advisors:             nil,
		SocialMedia:          nil,
		PhoneNumber:          "424-410-6123",
		Password:             "jkdasgdas" + randomId,
		TeamProfile:          nil,
		Subscriptions:        nil,
		IsActive:             true,
		ResetToken:           "",
		ResetTokenExpiration: nil,
		Admin:                nil,
		Bio:                  "Trying to connect with other entrepreneurs",
		AuthnAccountId:       0,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	teamRequest := CreateTeamRequest{
		Team: team,
	}

	jsonStr, err := json.Marshal(&teamRequest)
	if err != nil {
		return err, nil
	}

	return nil, jsonStr
}

func CreateTestTeamBackendCall(t *testing.T, srv *Server) (CreateTeamResponse, string, bool) {
	// first need to create a user account
	err, jsonStr := CreateUserAndConvertToJson()
	if err != nil {
		fmt.Println(err)
		return CreateTeamResponse{}, "", true
	}

	req, done := ConstructCreateUserRequestHelper(t, jsonStr, srv)
	if done {
		return CreateTeamResponse{}, "", true
	}

	// store the token in a local variable for later use
	bearerToken := req.Header.Get("Authorization")
	token := strings.Split(bearerToken, " ")[1]

	// perform request
	response := ExecuteRequest(req, srv.createUserAccountHandler)
	CheckResponseCode(t, http.StatusOK, response.Code)

	var createUserApiResponse CreateUserResponse
	_ = json.Unmarshal(response.Body.Bytes(), &createUserApiResponse)

	userId := createUserApiResponse.Id

	err, jsonStr = CreateTeamAndConvertToJson()
	if err != nil {
		fmt.Println(err)
		return CreateTeamResponse{}, "", true
	}

	req, done = ConstructCreateTeamRequestHelper(t, jsonStr, srv, userId)
	if done {
		return CreateTeamResponse{}, "", true
	}

	// store the token in a local variable for later use
	bearerToken = req.Header.Get("Authorization")
	token = strings.Split(bearerToken, " ")[1]

	// perform request
	response = ExecuteRequest(req, srv.createTeamHandler)
	CheckResponseCode(t, http.StatusOK, response.Code)

	var apiResponse CreateTeamResponse
	_ = json.Unmarshal(response.Body.Bytes(), &apiResponse)
	return apiResponse, token, false
}

func ConstructCreateTeamRequestHelper(t *testing.T, jsonStr []byte, srv *Server, adminUserId uint32) (*http.Request, bool) {
	random, err, done := GetRandomString()
	if done {
		fmt.Println(err)
		return nil, false
	}

	// set up http request
	req, _ := http.NewRequest("POST", "/v1/team/signup"+"?adminId="+fmt.Sprint(adminUserId), bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	// pass in some random username and password to the authentication service
	// in order to obtain a jwt token for authentication
	teamname := "test " + random
	password := "test-passwrod" + random
	if SignupAndStoreTokenInHeader(t, srv, teamname, password, req) {
		return nil, true
	}
	return req, false
}

func GetRandomString() (string, error, bool) {
	// randomize the various entries in the team account in order to ensure things are randomized
	// and we dont deal with circumstances in which we have duplicate values in the database
	randomId, err := NewUUID()
	if err != nil {
		fmt.Println(err)
		return "", nil, true
	}
	return randomId, err, false
}

func SignupAndStoreTokenInHeader(t *testing.T, srv *Server, name, password string, req *http.Request) bool {
	// attempt to signup in the authentication service, obtain the jwt response token,
	// append it to the authorization header of the request and send the request through
	aggregatedErr, token := srv.Auth.SignUp(name, password)
	if aggregatedErr != nil {
		if errorutils.ProcessAggregatedErrorsInTest(t, aggregatedErr) {
			return true
		}
	}

	// set the token in the authorization header
	if token != nil {
		AttachAuthTokenToHeader(token, req)
	}
	return false
}

func GetTeam(userAccountId uint32, token string, srv *Server) *httptest.ResponseRecorder {
	// attempt to get the team profile of interest, call should fail
	url := "/v1/team/" + fmt.Sprint(userAccountId)
	// set up http request
	req, _ := http.NewRequest("GET", url, nil)
	_, req = AttachIdToUrl(userAccountId, req)
	UpdateRequestHeaders(req, token)

	// perform request
	response := ExecuteRequest(req, srv.getTeamHandler)

	return response
}

func GetUser(userAccountId uint32, token string, srv *Server) *httptest.ResponseRecorder {
	url := "/v1/user/" + fmt.Sprint(userAccountId)
	// set up http request
	req, _ := http.NewRequest("GET", url, nil)
	_, req = AttachIdToUrl(userAccountId, req)
	UpdateRequestHeaders(req, token)

	// perform request
	response := ExecuteRequest(req, srv.getUserAccountHandler)
	return response
}

func GetAllUsers(token string, srv *Server, limit int) *httptest.ResponseRecorder {
	url := "/v1/user?limit=" + fmt.Sprint(limit)
	// set up http request
	req, _ := http.NewRequest("GET", url, nil)
	UpdateRequestHeaders(req, token)

	// perform request
	response := ExecuteRequest(req, srv.getAllUsersHandler)
	return response
}

func GetUserProfile(userAccountId uint32, token string, srv *Server) *httptest.ResponseRecorder {
	url := "/v1/user/profile/" + fmt.Sprint(userAccountId)
	// set up http request
	req, _ := http.NewRequest("GET", url, nil)
	_, req = AttachIdToUrl(userAccountId, req)
	UpdateRequestHeaders(req, token)

	// perform request
	response := ExecuteRequest(req, srv.getUserProfileHandler)
	return response
}

// Extracts an account id from a jwt token
func ExtractIdFromToken(token string) (error, int) {
	id, err := authentication.ExtractClaims(token)
	if err != nil {
		return err, 0
	}
	return nil, int(id)
}

// Updates the request headers and adds a jwt token as well as content type
func UpdateRequestHeaders(req *http.Request, token string) {
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+token)
}

// Creates a random user and marshall to json returning
// any error occuring as well as the marshalled json string
func CreateUserAndConvertToJson() (error, []byte) {
	id, err := NewUUID()
	if err != nil {
		return err, nil
	}

	user := model.User{
		UserName:          "yoanyomba" + id,
		Email:             "yoanyombapro@gmail.com" + id,
		PasswordConfirmed: "crunchesForYou" + id,
		Password:          "crunchesForYou" + id,
		FirstName:         "yoan" + id,
		LastName:          "yomba" + id,
		Intent:            "Investment" + id,
		UserAccountType:   "investor",
	}

	createRequest := CreateUserRequest{user}
	jsonStr, err := json.Marshal(&createRequest)
	if err != nil {
		return err, nil
	}

	return nil, jsonStr
}

// Creates a random user profile and marshall to json returning
// any error occuring as well as the marshalled json string
func CreateUserProfileAndConvertToJson() (error, []byte) {
	profile := model.Profile{
		Id:                   0,
		CreatedAt:            nil,
		DeletedAt:            nil,
		UpdatedAt:            nil,
		Bio:                  "Here to make friends and become lit",
		Experience:           nil,
		Address:              nil,
		Education:            nil,
		Skills:               nil,
		Team:                 nil,
		Group:                nil,
		SocialMedia:          nil,
		Settings:             nil,
		ProfileType:          "investor",
		Nationality:          "camerron",
		AvatarUrl:            "",
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	createRequest := CreateUserProfileRequest{profile}
	jsonStr, err := json.Marshal(&createRequest)
	if err != nil {
		return err, nil
	}

	return nil, jsonStr
}

func GetTeamProfile(userAccountId uint32, token string, srv *Server) *httptest.ResponseRecorder {
	// attempt to get the team profile of interest, call should fail
	url := "/v1/team/profile/" + fmt.Sprint(userAccountId)
	// set up http request
	req, _ := http.NewRequest("GET", url, nil)
	_, req = AttachIdToUrl(userAccountId, req)
	UpdateRequestHeaders(req, token)

	// perform request
	response := ExecuteRequest(req, srv.getTeamProfileHandler)
	return response
}

func DeleteTeamProfile(userAccountId uint32, token string, srv *Server) *httptest.ResponseRecorder {
	// Delete team profile
	url := "/v1/team/" + fmt.Sprint(userAccountId)
	// set up http request
	req, _ := http.NewRequest("DELETE", url, nil)
	_, req = AttachIdToUrl(userAccountId, req)
	UpdateRequestHeaders(req, token)

	// perform request
	response := ExecuteRequest(req, srv.deleteTeamProfileHandler)

	return response
}

func DeleteTeam(userAccountId uint32, token string, srv *Server) *httptest.ResponseRecorder {
	// Delete team profile
	url := "/v1/team/" + fmt.Sprint(userAccountId)
	// set up http request
	req, _ := http.NewRequest("DELETE", url, nil)
	_, req = AttachIdToUrl(userAccountId, req)
	UpdateRequestHeaders(req, token)

	// perform request
	response := ExecuteRequest(req, srv.deleteTeamHandler)

	return response
}

func DeleteUser(userAccountId uint32, token string, srv *Server) *httptest.ResponseRecorder {
	// Delete user account
	url := "/v1/user/" + fmt.Sprint(userAccountId)
	// set up http request
	req, _ := http.NewRequest("DELETE", url, nil)
	_, req = AttachIdToUrl(userAccountId, req)
	UpdateRequestHeaders(req, token)

	// perform request
	response := ExecuteRequest(req, srv.deleteUserAccountHandler)
	return response
}

func DeleteUserProfile(userAccountId uint32, profileId uint32, token string, srv *Server) *httptest.ResponseRecorder {
	// Delete user profile
	url := "/v1/user/" + fmt.Sprint(userAccountId) + "?" + "accountId=" + fmt.Sprint(userAccountId) + "&profileId=" + fmt.Sprint(profileId)
	// set up http request
	req, _ := http.NewRequest("DELETE", url, nil)
	_, req = AttachIdToUrl(userAccountId, req)
	UpdateRequestHeaders(req, token)

	// perform request
	response := ExecuteRequest(req, srv.deleteUserProfileHandler)
	return response
}

func UpdateTeam(teamAccountId uint32, token string, payload []byte, srv *Server) *httptest.ResponseRecorder {
	// attempt to get the team profile of interest, call should fail
	url := "/v1/team/" + fmt.Sprint(teamAccountId)
	// set up http request
	req, _ := http.NewRequest("PUT", url, bytes.NewBuffer(payload))
	_, req = AttachIdToUrl(teamAccountId, req)
	UpdateRequestHeaders(req, token)

	// perform request
	response := ExecuteRequest(req, srv.updateTeamHandler)

	return response
}

func UpdateTeamProfile(teamAccountId uint32, token string, payload []byte, srv *Server) *httptest.ResponseRecorder {
	// attempt to get the team profile of interest, call should fail
	url := "/v1/team/profile" + fmt.Sprint(teamAccountId)
	// set up http request
	req, _ := http.NewRequest("PUT", url, bytes.NewBuffer(payload))
	_, req = AttachIdToUrl(teamAccountId, req)
	UpdateRequestHeaders(req, token)

	// perform request
	response := ExecuteRequest(req, srv.updateTeamProfileHandler)

	return response
}

// create a random subscription and marshalls to json byte string
func CreateUserSubscriptionAndConvertToJson() (error, []byte) {
	subscription := model.Subscriptions{
		Id:                   0,
		SubscriptionName:     "Smart News Subscriptions",
		SubscriptionStatus:   "Active",
		StartDate:            &timestamp.Timestamp{},
		EndDate:              &timestamp.Timestamp{},
		AccessType:           "Public",
		IsActive:             false,
		CreatedAt:            nil,
		DeletedAt:            nil,
		UpdatedAt:            nil,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	createUserSubscriptionRequest := CreateUserSubscriptionRequest{Subscription: subscription}
	jsonStr, err := json.Marshal(&createUserSubscriptionRequest)
	if err != nil {
		return err, nil
	}

	return nil, jsonStr
}

func CreateTestUserBackendCall(t *testing.T, srv *Server) (CreateUserResponse, string, bool) {
	// first need to create a user account
	err, jsonStr := CreateUserAndConvertToJson()
	if err != nil {
		fmt.Println(err)
		return CreateUserResponse{}, "", true
	}

	req, done := ConstructCreateUserRequestHelper(t, jsonStr, srv)
	if done {
		return CreateUserResponse{}, "", true
	}

	// store the token in a local variable for later use
	bearerToken := req.Header.Get("Authorization")
	token := strings.Split(bearerToken, " ")[1]

	// perform request
	response := ExecuteRequest(req, srv.createUserAccountHandler)
	CheckResponseCode(t, http.StatusOK, response.Code)

	var apiResponse CreateUserResponse
	_ = json.Unmarshal(response.Body.Bytes(), &apiResponse)
	return apiResponse, token, false
}

func ConstructCreateUserRequestHelper(t *testing.T, jsonStr []byte, srv *Server) (*http.Request, bool) {
	random, err, done := GetRandomString()
	if done {
		fmt.Println(err)
		return nil, false
	}

	// set up http request
	req, _ := http.NewRequest("POST", "/v1/user/signup", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	// pass in some random username and password to the authentication service
	// in order to obtain a jwt token for authentication
	username := "test " + random
	password := "test-passwrod" + random
	if SignupAndStoreTokenInHeader(t, srv, username, password, req) {
		return nil, true
	}
	return req, false
}

func CreateTeamAndOptionalTeamProfile(t *testing.T, srv *Server, createProfileResponse CreateTeamProfileResponse, createTeamProfileFlag bool) (uint32, string, error) {
	apiResponse, token, done := CreateTestTeamBackendCall(t, srv)
	if done {
		return 0, "", errors.New("failed To Create Team")
	}

	if apiResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", apiResponse.Error)
	}

	teamAccountId := apiResponse.Id

	// create the team profile based on a flag
	if createTeamProfileFlag {
		jsonStr, err := CreateTeamProfileAndConvertToJson()
		if err != nil {
			return 0, "", err
		}

		url := "/v1/team/profile/" + fmt.Sprint(teamAccountId)

		// set up http request
		req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
		_, req = AttachIdToUrl(teamAccountId, req)
		UpdateRequestHeaders(req, token)

		// perform request
		response := ExecuteRequest(req, srv.createTeamProfileHandler)
		CheckResponseCode(t, http.StatusOK, response.Code)

		json.Unmarshal(response.Body.Bytes(), &createProfileResponse)

		if createProfileResponse.Error != nil {
			t.Errorf("Expected empty error field to be returned'. Got '%v'", createProfileResponse.Error)
		}
	}

	return teamAccountId, token, nil
}

func CreateUserAndOptionalUserProfile(t *testing.T, srv *Server, createProfileResponse CreateUserProfileResponse, createUserProfileFlag bool) (uint32, uint32, string, error) {
	// attempt creation of a sample test user account
	apiResponse, token, done := CreateTestUserBackendCall(t, srv)
	if done {
		return 0, 0, "", errors.New("failed To Create User")
	}
	// assert that no errors occured
	if apiResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", apiResponse.Error)
	}

	// extract the account id for the user
	userAccountId := apiResponse.Id

	// create the user profile based on a flag
	if createUserProfileFlag {
		// create a user profile marshalled into bytes
		err, jsonStr := CreateUserProfileAndConvertToJson()
		if err != nil {
			return 0, 0, "", err
		}

		// create the url to which to hit for this logic to properly occur
		url := "/v1/user/profile/" + fmt.Sprint(userAccountId)

		// set up http request
		req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
		_, req = AttachIdToUrl(userAccountId, req)
		UpdateRequestHeaders(req, token)

		// perform request
		response := ExecuteRequest(req, srv.createUserProfileHandler)
		CheckResponseCode(t, http.StatusOK, response.Code)

		// obtain the response from the prior api call
		json.Unmarshal(response.Body.Bytes(), &createProfileResponse)

		// process any occcuring errors
		if createProfileResponse.Error != nil {
			t.Errorf("Expected empty error field to be returned'. Got '%v'", createProfileResponse.Error)
		}

	}

	return userAccountId, createProfileResponse.UserProfile.Id, token, nil
}

func createUsersThroughApi(srv *Server, numUsers int) ([]*httptest.ResponseRecorder, []*UserOperationResponse) {
	responsesMetaData := make([]*httptest.ResponseRecorder, numUsers, numUsers*2)
	responsesData := make([]*UserOperationResponse, numUsers, numUsers*2)

	randomId, err := NewUUID()
	if err != nil {
		return nil, nil
	}

	for i := 0; i < numUsers; i++ {
		username := "yoanyomba" + "_" + randomId
		email := "yoanyombapro@gmail.com" + "_" + randomId
		password := "testpassword" + "_" + randomId
		firstName := "yoan" + "_" + randomId
		lastName := "yomba" + "_" + randomId
		intent := "intent" + "_" + randomId

		// generate user object
		user := model.User{UserName: username, Email: email, PasswordConfirmed: password,
			Password: password, FirstName: firstName, LastName: lastName, Intent: intent}

		createRequest := CreateUserRequest{user}
		jsonStr, err := json.Marshal(&createRequest)

		if err != nil {
			fmt.Println(err)
			return nil, nil
		}

		// set up http request
		req, _ := http.NewRequest("POST", "/v1/user/create", bytes.NewBuffer(jsonStr))
		req.Header.Set("Content-Type", "application/json")
		// perform the request
		response := ExecuteRequest(req, srv.createUserAccountHandler)

		// extract the data from the response code and populate and array
		var m UserOperationResponse
		_ = json.Unmarshal(response.Body.Bytes(), &m)

		responsesData = append(responsesData, &m)
		responsesMetaData = append(responsesMetaData, response)
	}

	return responsesMetaData, responsesData
}

func GenerateTestTeamProfile() model.TeamProfile {
	medialinks := []string{
		"https://gorm.io/docs/associations.html", "http://gorm.io/docs/associations.html",
	}
	/*
		settings := model.Settings{
			Id:                0,
			CreatedAt:         nil,
			UpdatedAt:         nil,
			DeletedAt:         nil,
			LastLogin:         nil,
			LastLoginLocation: "",
			Notification: &model.Notification{
				Id:        0,
				CreatedAt: nil,
				UpdatedAt: nil,
				DeletedAt: nil,
				PauseAll:  false,
				PostAndComments: &model.PostAndCommentsPushNotification{
					Id:                           0,
					CreatedAt:                    nil,
					UpdatedAt:                    nil,
					DeletedAt:                    nil,
					Likes:                        nil,
					LikesAndCommentsOnPostsOfYou: nil,
					PostsOfYou:                   nil,
					Comments:                     nil,
					CommentLikes:                 nil,
					XXX_NoUnkeyedLiteral:         struct{}{},
					XXX_unrecognized:             nil,
					XXX_sizecache:                0,
				},
				FollowingAndFollowers: nil,
				DirectMessages:        nil,
				EmailAndSms:           nil,
				XXX_NoUnkeyedLiteral:  struct{}{},
				XXX_unrecognized:      nil,
				XXX_sizecache:         0,
			},
			Privacy: &model.Privacy{
				Id:                   0,
				CreatedAt:            nil,
				UpdatedAt:            nil,
				DeletedAt:            nil,
				ActivityStatus:       true,
				PrivateAccount:       true,
				BlockedAccounts:      nil,
				MutedAccounts:        nil,
				BlockedTeamAccounts:  nil,
				MutedTeamAccounts:    nil,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			},
			Payment: &model.Payments{
				Id:        0,
				CreatedAt: nil,
				UpdatedAt: nil,
				DeletedAt: nil,
				LastLogin: nil,
				DebitCard: &model.Card{
					Id:                   0,
					CreatedAt:            nil,
					UpdatedAt:            nil,
					DeletedAt:            nil,
					LastLogin:            nil,
					CardNumber:           "1234-3334-5563-7677",
					SecurityCode:         "1290",
					CardZipCode:          "19103",
					FullName:             "D Yoan L Mekonchou Yomba",
					City:                 "New York",
					State:                "NY",
					Zipcode:              "19002",
					Address:              "80 Lafayette St",
					XXX_NoUnkeyedLiteral: struct{}{},
					XXX_unrecognized:     nil,
					XXX_sizecache:        0,
				},
				CreditCard:           nil,
				Pin:                  nil,
				XXX_NoUnkeyedLiteral: struct{}{},
				XXX_unrecognized:     nil,
				XXX_sizecache:        0,
			},
			XXX_NoUnkeyedLiteral: struct{}{},
			XXX_unrecognized:     nil,
			XXX_sizecache:        0,
		}
	*/
	investorDetails := model.InvestorDetail{
		Id:                   0,
		CreatedAt:            nil,
		DeletedAt:            nil,
		UpdatedAt:            nil,
		InvestorType:         model.InvestorType_angel_Investors,
		InvestmentStage:      model.InvestmentStage_expansion_second_third_stage_capital,
		NumberOfExits:        10,
		NumberOfInvestments:  30,
		NumberOfFunds:        2,
		Investments:          nil,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	startupDetails := model.StartupDetail{
		Id:                   0,
		CreatedAt:            nil,
		DeletedAt:            nil,
		UpdatedAt:            nil,
		TotalFunding:         100000,
		LatestRoundFunding:   10000,
		LatestRound:          "Series-A",
		LatestRoundEndDate:   nil,
		FundingType:          "Debt",
		CompanyName:          "Lens Platform",
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	media := model.Media{
		Id:                   0,
		DocumentLinks:        medialinks,
		PhotoLinks:           medialinks,
		VideoLinks:           medialinks,
		PresentationLinks:    medialinks,
		CreatedAt:            nil,
		DeletedAt:            nil,
		UpdatedAt:            nil,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
	teamProfile := model.TeamProfile{
		Id:                   0,
		CreatedAt:            nil,
		DeletedAt:            nil,
		UpdatedAt:            nil,
		Settings:             nil,
		InvestorDetail:       &investorDetails,
		StartupDetail:        &startupDetails,
		Media:                &media,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}
	return teamProfile
}
