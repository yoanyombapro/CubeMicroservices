package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
)

func TestDeleteUserSubscription(t *testing.T) {
	// TODO refactor into the testutils folder
	var (
		createUserSubscriptionResponse CreateUserSubscriptionResponse
		operationResponse              OperationResponse
	)

	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	// execute the backend call to the user's api and obtain the response
	apiResponse, token, done := CreateTestUserBackendCall(t, srv)
	if done {
		return
	}

	if apiResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", apiResponse.Error)
	}

	// Attempt creation of the team subscription from scratch
	err, jsonStr := CreateUserSubscriptionAndConvertToJson()
	if err != nil {
		fmt.Println(err)
		return
	}

	url := "/v1/user/subscription/" + fmt.Sprint(apiResponse.Id)

	// set up http request
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	_, req = AttachIdToUrl(apiResponse.Id, req)
	UpdateRequestHeaders(req, token)

	// perform request
	response := ExecuteRequest(req, srv.createUserSubscriptionHandler)
	CheckResponseCode(t, http.StatusOK, response.Code)

	json.Unmarshal(response.Body.Bytes(), &createUserSubscriptionResponse)

	if createUserSubscriptionResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", createUserSubscriptionResponse.Error)
	}

	url = "/v1/user/subscription" + "?" + "accountId=" + fmt.Sprint(apiResponse.Id) + "&" + "subscriptionId=" + fmt.Sprint(createUserSubscriptionResponse.Subscription[0].Id)
	req, _ = http.NewRequest("DELETE", url, bytes.NewBuffer(jsonStr))
	_, req = AttachIdToUrl(apiResponse.Id, req)
	UpdateRequestHeaders(req, token)

	// perform request
	response = ExecuteRequest(req, srv.deleteUserSubscriptionHandler)
	CheckResponseCode(t, http.StatusOK, response.Code)

	json.Unmarshal(response.Body.Bytes(), &operationResponse)

	if operationResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", operationResponse.Error)
	}
}
