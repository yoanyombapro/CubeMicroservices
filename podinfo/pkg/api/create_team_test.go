package api

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"testing"
	"time"
)

func TestCreateTeam(t *testing.T) {
	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	apiResponse, _, done := CreateTestTeamBackendCall(t, srv)
	if done {
		return
	}

	if apiResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", apiResponse.Error)
	}
}

func TestGetNonexistentTeam(t *testing.T) {
	var id uint32
	// create a mock server which will hold all necessary handlers
	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	id = 2000
	url := "/v1/team/" + fmt.Sprint(id)

	// set up http request
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("Content-Type", "application/json")

	_, req = AttachIdToUrl(id, req)

	// perform request
	response := ExecuteRequest(req, srv.getTeamHandler)
	CheckResponseCode(t, http.StatusNotFound, response.Code)
}

func TestTeamsEmptyTable(t *testing.T) {
	var getTeamResponse GetTeamResponse

	// create a mock server which will hold all necessary handlers
	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	rand.Seed(time.Now().UnixNano())
	id := strconv.Itoa(rand.Intn(100))

	url := "/v1/team/" + id
	req, _ := http.NewRequest("GET", url, nil)
	response := ExecuteRequest(req, srv.getTeamHandler)
	CheckResponseCode(t, http.StatusBadRequest, response.Code)

	// unmarshall the response and assert that there are no errors
	_ = json.Unmarshal(response.Body.Bytes(), &getTeamResponse)

	if getTeamResponse.Error == nil && getTeamResponse.Team.Id != 0 {
		t.Errorf("Expected an error to be raised'. Got '%v'", getTeamResponse.Error)
	}
}
