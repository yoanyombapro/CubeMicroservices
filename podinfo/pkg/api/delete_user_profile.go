package api

import (
	"net/http"
	"strconv"
)

// swagger:parameters deleteProfile
type DeleteUserProfileRequestSwagger struct {
	// user account id
	// in: query
	// required: true
	UserAccountId uint32
	// profile id
	// in: query
	// required: true
	ProfileId uint32
}

// swagger:route DELETE /v1/user/profile?accountId=xxx&&profileId=xxx User UserProfile deleteProfile
// Deletes a user profile record
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: operationResponse
// deletes a user profile record
func (s *Server) deleteUserProfileHandler(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()

	accId, _ := strconv.Atoi(q.Get("accountId"))
	accountId := uint32(accId)

	profId, _ := strconv.Atoi(q.Get("profileId"))
	profileId := uint32(profId)

	if err := s.Db.DeleteUserProfile(r.Context(), accountId, profileId); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	deleteOperationResponse := OperationResponse{
		Error: nil,
	}

	s.JSONResponse(w, r, deleteOperationResponse)
}
