package api

import (
	"net/http"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/helper/customerror"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/errorutils"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type LoginTeamRequest struct {
	TeamName string `json:"teamname"`
	Password string `json:"password"`
}

// Log in team
// swagger:parameters loginTeamRequest
type loginTeamRequest struct {
	// in: body
	Body struct {
		// team name
		// required: true
		// example: eventbrite
		TeamName string `json:"teamname"`
		// password
		// required: true
		// example: test-password
		Password string `json:"password"`
	}
}

// swagger:response loginTeamResponse
type loginTeamResponse struct {
	// in: body
	Body struct {
		// Jwt Token
		// required: true
		// example: kBxbjzKVDjvasgvds.askdhjaskjdgsagjcdgc.asjdjkasfgdas
		JwtToken string `json:"token"`
		// error
		// required: true
		// example: unable to get token
		Error error `json:"error"`
	}
}

// swagger:route POST /v1/login/team Team loginTeamRequest
// Log in Team
//
// Logs in a team into the system
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: loginTeamResponse
// logs in a team into the system
func (s *Server) loginTeamHandler(w http.ResponseWriter, r *http.Request) {
	var (
		loginTeamRequest LoginTeamRequest
	)

	// decode the data present in the body
	err := customerror.DecodeJSONBody(w, r, &loginTeamRequest)
	if err != nil {
		requestutils.ProcessMalformedRequest(w, err)
		return
	}

	if loginTeamRequest.Password == "" || loginTeamRequest.TeamName == "" {
		http.Error(w, "invalid input parameters. please specify a team name and password", http.StatusBadRequest)
		return
	}

	aggregatedErr, token := s.Auth.Login(loginTeamRequest.TeamName, loginTeamRequest.Password)
	if aggregatedErr != nil {
		if errorutils.ProcessAggregatedErrors(w, aggregatedErr) {
			return
		}
	}

	loginResponse := LoginResponse{
		JwtToken: *token,
		Error:    nil,
	}

	// TODO: cross check that the hashed password version matches the password
	s.JSONResponse(w, r, loginResponse)
}
