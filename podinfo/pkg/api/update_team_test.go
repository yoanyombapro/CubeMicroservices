package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
)

func TestTeamUpdate(t *testing.T) {
	var (
		createProfileResponse CreateTeamProfileResponse
		getTeamResponse       GetTeamResponse
		updateTeamRequest     UpdateTeamRequest
		updateTeamResponse    UpdateTeamResponse
	)

	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	teamAccountId, token, err := CreateTeamAndOptionalTeamProfile(t, srv, createProfileResponse, false)
	if err != nil {
		fmt.Println(err)
		return
	}

	// obtain the team but it should not contain a profile reference
	response := GetTeam(teamAccountId, token, srv)
	CheckResponseCode(t, http.StatusOK, response.Code)
	// transform the response
	_ = json.Unmarshal(response.Body.Bytes(), &getTeamResponse)
	if getTeamResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", getTeamResponse.Error)
	}

	if getTeamResponse.Team.Id != teamAccountId {
		t.Errorf("Expected the id to remain the same (%v). Got %v", fmt.Sprint(getTeamResponse.Team.Id), fmt.Sprint(teamAccountId))
	}

	updateTeamRequest.Team = getTeamResponse.Team
	updateTeamRequest.Team.Type = "Startup team"
	jsonStr, err := json.Marshal(&updateTeamRequest)
	if err != nil {
		fmt.Println(err)
		return
	}

	response = UpdateTeam(teamAccountId, token, jsonStr, srv)
	CheckResponseCode(t, http.StatusOK, response.Code)
	// transform the response
	_ = json.Unmarshal(response.Body.Bytes(), &updateTeamResponse)
	if updateTeamResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", updateTeamResponse.Error)
	}

	if updateTeamResponse.Team.Id != teamAccountId {
		t.Errorf("Expected the id to remain the same (%v). Got %v", fmt.Sprint(getTeamResponse.Team.Id), fmt.Sprint(teamAccountId))
	}

	if updateTeamResponse.Team.Type != updateTeamRequest.Team.Type {
		t.Errorf("Expected the id to remain the same (%v). Got %v", fmt.Sprint(updateTeamRequest.Team.Type), fmt.Sprint(updateTeamResponse.Team.Type))
	}
}
