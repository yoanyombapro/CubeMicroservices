package api

import (
	"net/http"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/helper/customerror"
	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/errorutils"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

// Update user request
// swagger:parameters updateUser
type UpdateUserRequestSwagger struct {
	// user account to create
	// in: body
	Body struct {
		// required: true
		User model.User `json:"result"`
	}
}

// swagger:route PUT /v1/user/update User updateUser
// Updates a user account present in the backend database
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: userOperationResponse
// gets a user by account id
func (s *Server) updatedUserAccountHandler(w http.ResponseWriter, r *http.Request) {
	var (
		createUserRequest CreateUserRequest
		response          UserOperationResponse
	)

	accountId := requestutils.ExtractIDFromRequest(r)

	err := customerror.DecodeJSONBody(w, r, &createUserRequest)
	if err != nil {
		requestutils.ProcessMalformedRequest(w, err)
		return
	}

	if createUserRequest.User.Email == "" || createUserRequest.User.UserName == "" {
		http.Error(w, "invalid input parameters. please specify a username and email", http.StatusBadRequest)
		return
	}

	if err := createUserRequest.User.Validate(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// check if the updated value is the username field by attempting to obtain
	// the user of interest by the current username. If successfully then we
	// know the username field has not been updated. If unsuccessful, then the
	// username does not exist. In such a scenario we perform one additional check
	// (get user by id) in order to fully assert the user does exists in the db
	// prior to performing an update api call to the authentication service
	exist, _, err := s.Db.CheckIfUserExistsByUsername(createUserRequest.User.UserName)
	if !exist {
		// the username field is indeed being updated
		// must check if the user actually does exist based on the user id
		exist, _, err = s.Db.GetUserIfExists(createUserRequest.User.Id, "", "")
		if !exist {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		// send the update username request to the authentication service
		// TODO implement retry logic on failures
		aggregatedErr := s.Auth.UpdateUsername(createUserRequest.User.UserName, createUserRequest.User.Password, createUserRequest.User.AuthnAccountId)
		if aggregatedErr != nil {
			if errorutils.ProcessAggregatedErrors(w, aggregatedErr) {
				return
			}
		}
	}

	updatedUser, err := s.Db.UpdateUser(r.Context(), accountId, createUserRequest.User)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response.Error = err
	response.User = *updatedUser
	s.JSONResponse(w, r, response)
}
