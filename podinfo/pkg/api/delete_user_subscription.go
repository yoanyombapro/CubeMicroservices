package api

import (
	"net/http"
	"strconv"
)

// swagger:parameters deleteSubscription
type DeleteSubscriptionRequestSwagger struct {
	// user account id
	// in: query
	// required: true
	UserAccountId uint32
	// subscription id
	// in: query
	// required: true
	subscriptionId uint32
}

// swagger:route DELETE /v1/user/profile?accountId=xxx&&subscriptionId=xxx User Subscription deleteSubscription
// Deletes a user profile record
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: operationResponse
// deletes a user subscription record
func (s *Server) deleteUserSubscriptionHandler(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()

	accId, _ := strconv.Atoi(q.Get("accountId"))
	accountId := uint32(accId)

	profId, _ := strconv.Atoi(q.Get("subscriptionId"))
	subscriptionId := uint32(profId)

	if err := s.Db.DeleteUserSubscription(r.Context(), accountId, subscriptionId); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	deleteOperationResponse := OperationResponse{
		Error: nil,
	}

	s.JSONResponse(w, r, deleteOperationResponse)
}
