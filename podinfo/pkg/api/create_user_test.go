package api

// https://semaphoreci.com/community/tutorials/building-and-testing-a-rest-api-in-go-with-gorilla-mux-and-postgresql

import (
	"encoding/json"
	"math/rand"
	"net/http"
	"strconv"
	"testing"
	"time"
)

func TestCreateUser(t *testing.T) {
	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	apiResponse, _, done := CreateTestUserBackendCall(t, srv)
	if done {
		return
	}

	if apiResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", apiResponse.Error)
	}
}

func TestEmptyUserTable(t *testing.T) {
	var userResponse UserOperationResponse

	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	rand.Seed(time.Now().UnixNano())
	randId := rand.Intn(1000)
	id := strconv.Itoa(randId)

	url := "/v1/user/" + id
	req, _ := http.NewRequest("GET", url, nil)
	_, req = AttachIdToUrl(uint32(randId), req)

	response := ExecuteRequest(req, srv.getUserAccountHandler)
	CheckResponseCode(t, http.StatusBadRequest, response.Code)

	// unmarshall the response and assert that there are no errors
	_ = json.Unmarshal(response.Body.Bytes(), &userResponse)

	if userResponse.Error == nil && userResponse.User.Id != 0 {
		t.Errorf("Expected an error to be raised'. Got '%v'", userResponse.Error)
	}
}
