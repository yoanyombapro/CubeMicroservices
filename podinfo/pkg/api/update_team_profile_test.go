package api

import (
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
)

func TestUpdateTeamProfile(t *testing.T) {
	var (
		createProfileResponse     CreateTeamProfileResponse
		getTeamProfileResponse    GetTeamProfileResponse
		updateTeamProfileRequest  UpdateTeamProfileRequest
		updateTeamProfileResponse UpdateTeamProfileResponse
	)

	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	teamAccountId, token, err := CreateTeamAndOptionalTeamProfile(t, srv, createProfileResponse, true)
	if err != nil {
		fmt.Println(err)
		return
	}

	// attempt to get the team profile which should fail
	response := GetTeamProfile(teamAccountId, token, srv)
	CheckResponseCode(t, http.StatusOK, response.Code)

	// transform the response
	_ = json.Unmarshal(response.Body.Bytes(), &getTeamProfileResponse)
	if getTeamProfileResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", getTeamProfileResponse.Error)
	}

	if getTeamProfileResponse.TeamProfile.Id == 0 {
		t.Errorf("Expected id field to not be zeo'. Got '%v'", getTeamProfileResponse.TeamProfile.Id)
	}

	updateTeamProfileRequest.Profile = getTeamProfileResponse.TeamProfile
	updateTeamProfileRequest.Profile.InvestorDetail.InvestmentStage = model.InvestmentStage_expansion_second_third_stage_capital
	updateTeamProfileRequest.Profile.InvestorDetail.InvestmentStage = model.InvestmentStage_early_first_second_stage_capital

	jsonStr, err := json.Marshal(updateTeamProfileRequest)
	if err != nil {
		fmt.Println(err)
		return
	}

	response = UpdateTeamProfile(teamAccountId, token, jsonStr, srv)
	CheckResponseCode(t, http.StatusOK, response.Code)
	_ = json.Unmarshal(response.Body.Bytes(), &updateTeamProfileResponse)
	if updateTeamProfileResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", updateTeamProfileResponse.Error)
	}

	if getTeamProfileResponse.TeamProfile.Id != updateTeamProfileResponse.Profile.TeamProfile.Id {
		t.Errorf("Expected the id to remain the same (%v). Got %v", fmt.Sprint(updateTeamProfileResponse.Profile.Id), fmt.Sprint(getTeamProfileResponse.TeamProfile.Id))
	}
}
