package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
)

func TestCreateTeamSubscription(t *testing.T) {
	var createTeamSubscriptionResponse CreateTeamSubscriptionResponse

	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	// execute the backend call to the team's api and obtain the response
	apiResponse, token, done := CreateTestTeamBackendCall(t, srv)
	if done {
		return
	}

	if apiResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", apiResponse.Error)
	}

	// Attempt creation of the team subscription from scratch
	err, jsonStr := CreateTeamSubscriptionAndConvertToJson()
	if err != nil {
		fmt.Println(err)
		return
	}

	url := "/v1/team/subscription/" + fmt.Sprint(apiResponse.Id)

	// set up http request
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	_, req = AttachIdToUrl(apiResponse.Id, req)
	UpdateRequestHeaders(req, token)

	// perform request
	response := ExecuteRequest(req, srv.createOrUpdateTeamSubscriptionsHandler)
	CheckResponseCode(t, http.StatusOK, response.Code)

	json.Unmarshal(response.Body.Bytes(), &createTeamSubscriptionResponse)

	if createTeamSubscriptionResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", createTeamSubscriptionResponse.Error)
	}

	// TODO update the subscription and save it
}
