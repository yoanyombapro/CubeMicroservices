package api

import (
	"net/http"
	"strconv"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/helper/customerror"
	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type UpdateUserProfileRequest struct {
	Profile model.Profile `json:"result"`
}

type UpdateUserProfileResponse struct {
	Error   error         `json:"error"`
	Profile model.Profile `json:"result"`
}

// swagger:parameters updateProfile
type UpdateUserProfileRequestSwagger struct {
	// in: body
	Body struct {
		// errors
		// required: true
		Error error `json:"error"`
		// profile to update
		// required: true
		Profile model.Profile `json:"result"`
	}
	// user account id
	// in: query
	// required: true
	UserAccountId uint32
	// profile id
	// in: query
	// required: true
	ProfileId uint32
}

// swagger:response updateProfileResponse
type UpdateUserProfileResponseSwagger struct {
	// in: body
	Body struct {
		// error
		// required: true
		Error error `json:"error"`
		// profile to update
		// required: true
		Profile model.Profile `json:"result"`
	}
}

// swagger:route PUT /v1/user/profile?accountId=xxx&&profileId=xxx User UserProfile updateProfile
// Updates a user profile record
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: updateProfileResponse
// updates a user profile record and ties it to a user account
func (s *Server) updateUserProfileHandler(w http.ResponseWriter, r *http.Request) {
	var (
		updateUserProfileRequest UpdateUserProfileRequest
	)
	q := r.URL.Query()

	accId, _ := strconv.Atoi(q.Get("accountId"))
	accountId := uint32(accId)

	profId, _ := strconv.Atoi(q.Get("profileId"))
	profileId := uint32(profId)

	// decode the request body
	err := customerror.DecodeJSONBody(w, r, &updateUserProfileRequest)
	if err != nil {
		requestutils.ProcessMalformedRequest(w, err)
		return
	}

	profile, err := s.Db.UpdateUserProfile(r.Context(), accountId, profileId, updateUserProfileRequest.Profile)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	updateUserProfileResponse := UpdateUserProfileResponse{
		Error:   err,
		Profile: *profile,
	}

	s.JSONResponse(w, r, updateUserProfileResponse)
}
