package api

import (
	"bytes"
	"encoding/json"
	"math/rand"
	"net/http"
	"strconv"
	"testing"
	"time"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/errorutils"
)

func TestUserLogin(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	rand_value := strconv.Itoa(rand.Intn(100))

	createUserLoginRequest := LoginUserRequest{
		Username: "yoanyomba2-test" + rand_value,
		Password: "Garanada123-test!!" + rand_value,
	}

	// marshall the struct into a json object
	jsonStr, err := json.Marshal(&createUserLoginRequest)
	if err != nil {
		// fmt.Println(err)
		return
	}

	srv := InitializeMockServer()

	// call the authentication service through wrapper
	aggregatedErr, _ := srv.Auth.SignUp(createUserLoginRequest.Username, createUserLoginRequest.Password)
	if aggregatedErr != nil {
		if errorutils.ProcessAggregatedErrorsInTest(t, aggregatedErr) {
			return
		}
	}

	// set up http request
	req, _ := http.NewRequest("POST", "/v1/login/user", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	// perform request
	response := ExecuteRequest(req, srv.loginUserHandler)
	CheckResponseCode(t, http.StatusOK, response.Code)

	var loginResponse LoginResponse
	_ = json.Unmarshal(response.Body.Bytes(), &loginResponse)

	if loginResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", loginResponse.Error)
	}
}
