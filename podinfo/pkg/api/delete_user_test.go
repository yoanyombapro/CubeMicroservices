package api

import (
	"encoding/json"
	"net/http"
	"testing"
)

func TestDeleteUser(t *testing.T) {
	var deleteUserResponse OperationResponse

	// create database connection and spin up mock server
	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	// perform a create user api call
	apiResponse, token, done := CreateTestUserBackendCall(t, srv)
	if done {
		return
	}

	// from the api response check for errors and fail if any found
	if apiResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", apiResponse.Error)
	}

	// perform the deletion request based on the user id returned in the api response
	response := DeleteUser(apiResponse.Id, token, srv)

	// check the response code of the operation
	CheckResponseCode(t, http.StatusOK, response.Code)

	// extract the response body into a valid objects
	if err := json.Unmarshal(response.Body.Bytes(), &deleteUserResponse); err != nil {
		t.Errorf("failed to properly unmarshall request'. Got '%v'", err)
	}

	// check for errors
	if deleteUserResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", deleteUserResponse.Error)
	}
}
