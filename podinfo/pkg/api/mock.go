package api

import (
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
	"go.uber.org/zap"

	"gitlab.com/yoanyombapro/CubeMicroservices/common/circuitbreaker"
	"gitlab.com/yoanyombapro/CubeMicroservices/common/messaging/rabbitmq"
	"gitlab.com/yoanyombapro/CubeMicroservices/common/tracing"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/authentication"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/database"
)

func NewMockServer() *Server {
	config := &Config{
		Port:                                "9898",
		HttpServerShutdownTimeout:           5 * time.Second,
		HttpServerTimeout:                   30 * time.Second,
		BackendURL:                          []string{},
		ConfigPath:                          "/config",
		DataPath:                            "/data",
		HttpClientTimeout:                   30 * time.Second,
		UIColor:                             "blue",
		UIPath:                              ".ui",
		UIMessage:                           "Greetings",
		Hostname:                            "localhost",
		EnableAuthServicePrivateIntegration: false,
		ServiceEmail:                        "yoanyombapro@gmail.com",
		AmqpServerUrl:                       "amqp://guest:guest@localhost:5672",
		ServiceName:                         "TestService",
		ZipkinServerUrl:                     "http://zipkin:9411",
	}

	logger, _ := zap.NewDevelopment()

	connSettings := "postgresql://doadmin:oqshd3sto72yyhgq@test-do-user-6612421-0.a.db.ondigitalocean.com:25060/defaultdb?sslmode=require"

	// initialize server
	db := Initialize(connSettings)
	auth := authentication.NewAuthenticationService("http://localhost:3000", config.EnableAuthServicePrivateIntegration)

	// call the authentication service and obtain configuration parameters such as jwt signing key
	aggErr, jwtConfig := auth.GetJwtPublicKey()
	if aggErr != nil {
		logger.Panic("failed to obtain jwt public key", zap.Error(aggErr.Error))
	}

	// from the config object should call a get request to the jwks_uri
	// which is the url at which the public key resides
	aggErr, jwtKeys := auth.GetJwks(jwtConfig.JwtPublicKeyURI)
	if aggErr != nil {
		logger.Panic("failed to obtain jwt public key", zap.Error(aggErr.Error))
	}

	logger.Info("Successfully obtained jwt config parameters from authentication service")

	m := map[string]rabbitmq.Exchange{
		"Email-Service": {
			ExchangeName: "Email-Service",
			ExchangeType: "direct",
			Durable:      true,
			AutoDelete:   false,
			Internal:     false,
			NoWait:       false,
			Args:         nil,
		},
		"Notification-Service": {
			ExchangeName: "Notification-Service",
			ExchangeType: "direct",
			Durable:      true,
			AutoDelete:   false,
			Internal:     false,
			NoWait:       false,
			Args:         nil,
		},
		"discovery": {
			ExchangeName: "discovery",
			ExchangeType: "direct",
			Durable:      true,
			AutoDelete:   false,
			Internal:     false,
			NoWait:       false,
			Args:         nil,
		},
	}

	queues := rabbitmq.InitiateQueues(m)
	amqpClient, err := rabbitmq.New(config.AmqpServerUrl, queues, logger)
	if err != nil {
		logger.Error("error occurred while initiating connection to rabbitmq broker.", zap.Error(err))
		panic(err.Error())
	}

	tracing.InitTracing(config.ZipkinServerUrl, config.ServiceName)
	circuitbreaker.ConfigureHystrix([]string{"EmailService", "SocialService"}, amqpClient)
	client := &http.Client{}
	var transport http.RoundTripper = &http.Transport{
		DisableKeepAlives: true,
	}
	client.Transport = transport
	circuitbreaker.Client = client

	return &Server{
		router:          mux.NewRouter(),
		logger:          logger,
		config:          config,
		Db:              db,
		Auth:            auth,
		JwtConfig:       jwtConfig,
		Keys:            jwtKeys,
		MessagingClient: amqpClient,
	}
}

func Initialize(connSettings string) *database.Database {
	var err error
	// configure logging
	logger := zap.L()
	defer logger.Sync()
	stdLog := zap.RedirectStdLog(logger)
	defer stdLog()

	connectionString := connSettings

	// connect to database
	dbInstance, err := database.New(connectionString, logger)
	if err != nil {
		logger.Info("Error connecting to database", zap.Error(err))
		os.Exit(1)
	}

	return dbInstance
}
