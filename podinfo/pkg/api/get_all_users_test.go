package api

import (
	"encoding/json"
	"net/http"
	"testing"
)

func TestGetAllUsers(t *testing.T) {
	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	var (
		latesttoken string
		getAllUsers GetAllUsersResponse
	)

	numTestUsers := 2
	ids := make([]uint32, numTestUsers, numTestUsers+3)

	for i := 0; i < numTestUsers; i++ {
		apiResponse, token, done := CreateTestUserBackendCall(t, srv)
		if done {
			return
		}

		ids = append(ids, apiResponse.Id)

		if i == numTestUsers-1 {
			latesttoken = token
		}
	}

	// get all users
	response := GetAllUsers(latesttoken, srv, numTestUsers)
	// check the response code
	CheckResponseCode(t, http.StatusOK, response.Code)

	// unmarshall the response and assert that there are no errors
	_ = json.Unmarshal(response.Body.Bytes(), &getAllUsers)

	if getAllUsers.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", getAllUsers.Error)
	}
}

// Find takes a slice and looks for an element in it. If found it will
// return it's key, otherwise it will return -1 and a bool of false.
func Find(slice []uint32, val uint32) (int, bool) {
	for i, item := range slice {
		if item == val {
			return i, true
		}
	}
	return -1, false
}
