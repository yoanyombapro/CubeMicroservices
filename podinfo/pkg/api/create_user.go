package api

import (
	"fmt"
	"net/http"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/contracts/proto/contracts"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/helper"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/helper/customerror"
	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/errorutils"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type UserOperationResponse struct {
	Error error      `json:"error"`
	User  model.User `json:"result"`
}

type CreateUserRequest struct {
	User model.User `json:"user"`
}

type CreateUserResponse struct {
	Error error  `json:"error"`
	Id    uint32 `json:"id"`
}

// Create user request
// swagger:parameters createUser
type createUserRequest struct {
	// in: body
	Body struct {
		// user account to create
		// required: true
		User model.User `json:"user"`
	}
}

// User Successfully created
// swagger:response createUserResponse
type createUserResponse struct {
	// in: body
	Body struct {
		// user account id
		// required: true
		// example: 20
		Id uint32 `json:"id"`
		// error
		// required: true
		// example: user already exists
		Error error `json:"error"`
	}
}

// Basic user response upon completion of update operations
// swagger:response userOperationResponse
type userOperationResponseSwagger struct {
	// created user account as well as any errors raised
	// in: body
	Body struct {
		// required: true
		Error error `json:"error"`
		// the user account record updated
		// required: true
		User model.User `json:"result"`
	}
}

// Error occured during request lifecycle
// swagger:response genericError
type genericErrorResponseSwagger struct {
	// in: body
	Body struct {
		// required: true
		Error error `json:"error"`
	}
}

// swagger:response operationResponse
type OperationResponseSwagger struct {
	// in: body
	Body struct {
		// error
		// required: true
		// example: Error occured
		Error error `json:"error"`
	}
}

// swagger:route POST /v1/user User createUser
//
// Create User Account
//
// creates a user account object in the backend database
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: createUserResponse
// 400: genericError
// 404: genericError
// 403: genericError
// 406: genericError
// 401: genericError
// 500: genericError
// creates a user account
func (s *Server) createUserAccountHandler(w http.ResponseWriter, r *http.Request) {
	var (
		createUserRequest CreateUserRequest
		userAccount       model.User
	)

	// extract the authn_id from the jwt token in the request header
	err, authnId := s.ExtractJwtFromHeader(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = customerror.DecodeJSONBody(w, r, &createUserRequest)
	if err != nil {
		requestutils.ProcessMalformedRequest(w, err)
		return
	}

	userAccount = createUserRequest.User
	if userAccount.Email == "" || userAccount.UserName == "" {
		http.Error(w, "invalid input parameters. please specify a username and email", http.StatusBadRequest)
		return
	}

	// assert to the auth service, the user exists as a user must first signup through
	// the auth service before being created on the user management service
	if s.config.Production {
		if s.UserExistsToAuthService(w, &userAccount) {
			return
		}
	}

	// hash user password
	password, err := helper.ValidateAndHashPassword(userAccount)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	userAccount.Password = password
	userAccount.PasswordConfirmed = password
	userAccount.AuthnAccountId = authnId

	// validate the request witholds the necessary fields of interest
	if err := userAccount.Validate(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	createdUserAccount, err := s.Db.CreateUser(r.Context(), userAccount)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response := CreateUserResponse{Id: createdUserAccount.Id, Error: err}

	// TODO: move exchange to a data structure that can be easily accessible
	msg := contracts.EmailContract{SenderEmailAddress: s.config.ServiceEmail, TargetEmailAddress: userAccount.Email, Subject: "Welcome To The Cube Platform", Message: "We are happy to have you"}
	msgByteStr := []byte(fmt.Sprintf("%v", msg))
	err = s.MessagingClient.PublishOnQueue(nil, msgByteStr, "Email-Service")
	if err != nil {
		// TODO: implement this as a retryable operation
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// store the request in the database
	s.JSONResponse(w, r, response)
}

func (s *Server) UserExistsToAuthService(w http.ResponseWriter, user *model.User) bool {
	// Obtain the account of interest from the authentication service as it
	// is the single source of truth for all authentication records tied to a
	// user
	aggregatedErr, authnAccount := s.Auth.GetAccount(user.AuthnAccountId, user.UserName, user.Password)
	if aggregatedErr != nil {
		if errorutils.ProcessAggregatedErrors(w, aggregatedErr) {
			return true
		}
	}

	// once the account is obtained, we assert that it is not locked or deleted
	// this is important to enforce that consistency at the record level is witheld
	// throughout the entire backend
	if authnAccount == nil {
		http.Error(w, "account authentication records do not exist", http.StatusNotFound)
		return true
	}

	if authnAccount.Result.Locked {
		http.Error(w, "account is locked", http.StatusNotFound)
		return true
	} else if authnAccount.Result.Deleted {
		http.Error(w, "account is deleted", http.StatusNotFound)
		return true
	}
	return false
}
