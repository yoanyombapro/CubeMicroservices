package api

import (
	"net/http"
	"strconv"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/helper/customerror"
	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/requestutils"
)

type UpdateUserSubscriptionRequest struct {
	Subscription model.Subscriptions `json:"result"`
}

type UpdateUserSubscriptionResponse struct {
	Error        error               `json:"error"`
	Subscription model.Subscriptions `json:"result"`
}

// swagger:route PUT /v1/user/subscription?accountId=xxx&&subscriptionId=xxx User Subscription updateSubscription
// Updates a user subscription record
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: updateSubscriptionResponse
// updates a user subscription record and ties it to a user account
func (s *Server) updateUserSubscriptionHandler(w http.ResponseWriter, r *http.Request) {
	var (
		createSubscriptionRequest  UpdateUserSubscriptionRequest
		createSubscriptionResponse UpdateUserSubscriptionResponse
	)
	q := r.URL.Query()

	accId, _ := strconv.Atoi(q.Get("accountId"))
	accountId := uint32(accId)

	subscrpId, _ := strconv.Atoi(q.Get("subscriptionId"))
	subscriptionId := uint32(subscrpId)

	// decode the request body
	err := customerror.DecodeJSONBody(w, r, &createSubscriptionRequest)
	if err != nil {
		requestutils.ProcessMalformedRequest(w, err)
		return
	}

	subscription, err := s.Db.UpdateUserSubscription(r.Context(), accountId, subscriptionId, createSubscriptionRequest.Subscription)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	createSubscriptionResponse.Error = err
	createSubscriptionResponse.Subscription = *subscription
	s.JSONResponse(w, r, createSubscriptionResponse)
}
