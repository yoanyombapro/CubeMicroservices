package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"
)

func TestUpdateUser(t *testing.T) {
	var createUserRequest CreateUserRequest
	var userResponse UserOperationResponse
	numUsersToCreate := 1

	srv, cleaner := InitializeMockServerAndDb()
	defer cleaner()

	_, createdUsers := createUsersThroughApi(srv, numUsersToCreate)
	// iterate over the set of created users and update a field and send the request to
	// the database and assert the response code makes sense

	for _, createdUser := range createdUsers {
		if createdUser != nil {
			// specify url
			url := "/v1/user/update"

			// update fields of interest
			createdUser.User.Email = "updatedEmailAddress@email.com"
			createdUser.User.UserName = "updatedUserName"
			createdUser.User.FirstName = "updatedFirsttName"
			createdUser.User.LastName = "updatedLastName"

			// convert the user struct to json and attempt update operation
			createUserRequest.User = createdUser.User
			jsonStr, err := json.Marshal(&createUserRequest)
			if err != nil {
				fmt.Println(err)
				return
			}

			// attempt to obtain the user again from the database through id
			// set up http request
			req, _ := http.NewRequest("GET", url, bytes.NewBuffer(jsonStr))
			req.Header.Set("Content-Type", "application/json")

			// perform the request
			response := ExecuteRequest(req, srv.updatedUserAccountHandler)

			// check the response code
			CheckResponseCode(t, http.StatusOK, response.Code)

			// unmarshall the response and assert that there are no errors
			_ = json.Unmarshal(response.Body.Bytes(), &userResponse)

			if userResponse.Error != nil {
				t.Errorf("Expected empty error field to be returned'. Got '%v'", userResponse.Error)
			}
		}
	}
}
