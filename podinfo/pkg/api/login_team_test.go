package api

import (
	"bytes"
	"encoding/json"
	"math/rand"
	"net/http"
	"strconv"
	"testing"
	"time"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/errorutils"
)

func TestTeamLogin(t *testing.T) {
	rand.Seed(time.Now().UnixNano())
	randNum := strconv.Itoa(rand.Intn(100000))

	loginTeamReq := LoginTeamRequest{
		TeamName: "Lens Platform" + randNum,
		Password: "ksdnlfsjd" + randNum,
	}

	// marshall the struct into a json string
	jsonStr, err := json.Marshal(&loginTeamReq)
	if err != nil {
		return
	}

	srv := InitializeMockServer()

	// call the authentication service through wrapper
	aggregatedErr, _ := srv.Auth.SignUp(loginTeamReq.TeamName, loginTeamReq.Password)
	if aggregatedErr != nil {
		if errorutils.ProcessAggregatedErrorsInTest(t, aggregatedErr) {
			return
		}
	}

	// set up http request
	req, _ := http.NewRequest("POST", "/v1/login/team", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	// perform request
	response := ExecuteRequest(req, srv.loginTeamHandler)
	CheckResponseCode(t, http.StatusOK, response.Code)

	var loginResponse LoginResponse
	_ = json.Unmarshal(response.Body.Bytes(), &loginResponse)

	if loginResponse.Error != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", loginResponse.Error)
	}
}
