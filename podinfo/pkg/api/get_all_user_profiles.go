package api

import (
	"net/http"
	"strconv"

	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
)

type GetAllUserProfileResponse struct {
	Error       error           `json:"error"`
	UserProfile []model.Profile `json:"result"`
}

// swagger: parameters getAllUserProfilesByProfileType
type GetUserProfilesByProfileTypeRequestSwagger struct {
	// in: query
	// required: true
	ProfileType string
	// required: true
	Limit uint32
}

// swagger:parameters getUserProfile
type GetUserProfileRequestSwagger struct {
	// in: query
	// required: true
	AccountId uint32 `json:"error"`
}

// swagger:response getAllUserProfilesResponse
type getAllUserProfilesResponse struct {
	// in: body
	Body struct {
		// set of profiles of interest
		// required: true
		Profile []model.Profile `json:"profiles"`
	}
}

// swagger:route GET /v1/user/profile?limit=xxx&profileType=xxxx User UserProfile getAllUserProfilesByProfileType
// Gets a set of user profiles by profile type
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: getAllUserProfilesResponse
func (s *Server) getAllUserProfilesByProfileTypeHandler(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()

	limit, _ := strconv.Atoi(q.Get("limit"))
	maxUserProfiles := uint32(limit)

	profileType := q.Get("profileType")

	// query the database for the set of user profiles of interest
	userProfiles, err := s.Db.GetAllUserProfilesByType(r.Context(), profileType, maxUserProfiles)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response := GetAllUserProfileResponse{
		err,
		userProfiles,
	}
	s.JSONResponse(w, r, response)
}

// swagger: parameters getAllUserProfilesByNationality
type GetUserProfilesByNationalityRequestSwagger struct {
	// in: query
	// required: true
	Nationality string
	// required: true
	Limit uint32
}

// swagger:route GET /v1/user/profile?limit=xxx&nationality=xxxx User UserProfile getAllUserProfilesByNationality
// Gets a set of user profiles by nationality
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: getAllUserProfilesResponse
func (s *Server) getAllUserProfilesByNationalityTypeHandler(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()

	limit, _ := strconv.Atoi(q.Get("limit"))
	maxUserProfiles := uint32(limit)

	profileType := q.Get("nationality")

	// query the database for the set of user profiles of interest
	userProfiles, err := s.Db.GetAllUserProfilesByNationality(r.Context(), profileType, maxUserProfiles)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response := GetAllUserProfileResponse{
		err,
		userProfiles,
	}
	s.JSONResponse(w, r, response)
}

// swagger: response getAllUserProfiles
type GetAllUserProfilesSwagger struct {
	// in: query
	// required: true
	Limit uint32
}

// swagger:route GET /v1/user/profile?limit=xxx User UserProfile getAllUserProfiles
// Gets a set of user profiles
//
//     Consumes:
//     - application/json
//     - application/x-protobuf
//
//     Produces:
//     - application/json
//     - application/x-protobuf
//
//     Schemes: http, https, ws, wss
//
//     Security:
//       api_key:
//       oauth: read, write
// responses:
//      200: getAllUserProfilesResponse
func (s *Server) getAllUserProfilesHandler(w http.ResponseWriter, r *http.Request) {
	q := r.URL.Query()

	limit, _ := strconv.Atoi(q.Get("limit"))
	maxUserProfiles := uint32(limit)

	// query the database for the set of user profiles of interest
	userProfiles, err := s.Db.GetAllUserProfiles(r.Context(), maxUserProfiles)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	response := GetAllUserProfileResponse{
		err,
		userProfiles,
	}
	s.JSONResponse(w, r, response)
}
