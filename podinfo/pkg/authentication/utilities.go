package authentication

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"

	"gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"
)

const (
	AccountsUrl = "http://localhost:3000/accounts"
	Origin      = "http://localhost:3000"
)

type Authentication struct {
	Origin                           string
	AccountsBase                     string
	SessionBase                      string
	Availability                     string
	Import                           string
	RefreshSession                   string
	EnablePrivateEndpointInteraction bool
}

// Obtain authentication service
func NewAuthenticationService(origin string, enablePrivateEndpointInteraction bool) *Authentication {
	if origin == "" {
		// TODO: remove hard coded config should be configurable
		origin = Origin
	}
	return &Authentication{
		Origin:                           origin,
		AccountsBase:                     origin + "/accounts",
		Availability:                     origin + "/accounts/available",
		Import:                           origin + "/accounts/import",
		SessionBase:                      origin + "/session",
		RefreshSession:                   origin + "/session/refresh",
		EnablePrivateEndpointInteraction: enablePrivateEndpointInteraction,
	}
}

func (a *Authentication) SetHeadersAndPerformRequest(request *http.Request, username, password string, isPublic bool) (error, *http.Response) {
	if !isPublic {
		request.SetBasicAuth(username, password)
	}

	request.Header.Set("Origin", a.Origin)
	request.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(request)
	return err, resp
}

type ParseJwtResponse struct {
	Auth     []string         `json:"aud"`
	AuthTime *jwt.NumericDate `json:"auth_time"`
	Exp      *jwt.NumericDate `json:"exp"`
	Iat      *jwt.NumericDate `json:"iat"`
	Iss      string           `json:"iss"`
	Sub      string           `json:"sub"`
}

func ExtractClaims(tokenStr string) (uint32, error) {
	parsed, err := jose.ParseSigned(tokenStr)
	if err != nil {
		return 0, err
	}

	if parsed == nil || parsed.Signatures == nil || len(parsed.Signatures) == 0 {
		return 0, errors.New("invalid parsed jwt token")
	}

	// since we do not have the private key impossible to verify hence
	// we use an unsafe approach for now and obtain the payload
	payload := parsed.UnsafePayloadWithoutVerification()

	var response ParseJwtResponse
	_ = json.Unmarshal(payload, &response)

	id, err := strconv.Atoi(response.Sub)
	if err != nil {
		return 0, nil
	}
	return uint32(id), nil
}
