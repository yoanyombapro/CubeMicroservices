package authentication

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

func (a *Authentication) SignUp(username, password string) (*AggregatedError, *TokenId) {
	var (
		authError AuthError
		result    TokenId
	)

	credentials := Credentials{
		Username: username,
		Password: password,
	}

	jsonStr, err := json.Marshal(&credentials)

	request, err := http.NewRequest("POST", a.AccountsBase, bytes.NewBuffer(jsonStr))
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	// publically available endpoint
	err, resp := a.SetHeadersAndPerformRequest(request, "", "", true)
	defer resp.Body.Close()
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	aggregatedError, done := ExtractErrorResponse(resp, err, body, authError)
	if done {
		return aggregatedError, nil
	}

	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	err = json.Unmarshal(body, &result)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	return nil, &result
}

func (a *Authentication) GetAccount(id uint32, username, password string) (*AggregatedError, *AuthAccount) {
	var (
		authError   AuthError
		authAccount AuthAccount
	)

	url := a.AccountsBase + "/" + fmt.Sprint(id)

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	// private api endpoint
	err, resp := a.SetHeadersAndPerformRequest(request, username, password, false)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	aggregatedError, done := ExtractErrorResponse(resp, err, body, authError)
	if done {
		return aggregatedError, nil
	}

	err = json.Unmarshal(body, &authAccount)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	return nil, &authAccount
}

func (a *Authentication) UpdateUsername(username, password string, id uint32) *AggregatedError {
	var authError AuthError
	url := a.AccountsBase + "/" + fmt.Sprint(id)
	jsonStr, err := json.Marshal(&UsernamePayload{Username: username})

	request, err := http.NewRequest("PUT", url, bytes.NewBuffer(jsonStr))
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}
	}

	// private api endpoint
	err, resp := a.SetHeadersAndPerformRequest(request, username, password, false)
	defer resp.Body.Close()

	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}
	}

	body, err := ioutil.ReadAll(resp.Body)

	aggregatedError, done := ExtractErrorResponse(resp, err, body, authError)
	if done {
		return aggregatedError
	}

	return nil
}

func (a *Authentication) IsUsernameAvailable(username string) (*AggregatedError, bool) {
	var authError AuthError

	jsonStr, err := json.Marshal(&UsernamePayload{Username: username})

	request, err := http.NewRequest("GET", a.Availability, bytes.NewBuffer(jsonStr))
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, false
	}

	// public api endpoint
	err, resp := a.SetHeadersAndPerformRequest(request, "", "", true)
	defer resp.Body.Close()
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, false
	}

	body, err := ioutil.ReadAll(resp.Body)

	aggregatedError, done := ExtractErrorResponse(resp, err, body, authError)
	if done {
		return aggregatedError, false
	}

	return nil, true
}

func (a *Authentication) LockOrUnlockAccount(id int, username, password string, lock bool) *AggregatedError {
	var authError AuthError
	var url string

	if lock {
		url = a.AccountsBase + "/" + fmt.Sprint(id) + "/lock"
	} else {
		url = a.AccountsBase + "/" + fmt.Sprint(id) + "/unlock"
	}

	request, err := http.NewRequest("PUT", url, nil)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}
	}

	err, resp := a.SetHeadersAndPerformRequest(request, username, password, false)
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}
	}

	aggregatedError, done := ExtractErrorResponse(resp, err, body, authError)
	if done {
		return aggregatedError
	}

	return nil
}

func (a *Authentication) DeleteAccount(username, password string, id uint32) *AggregatedError {
	var authError AuthError

	url := a.AccountsBase + "/" + fmt.Sprint(id)

	request, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}
	}

	err, resp := a.SetHeadersAndPerformRequest(request, username, password, false)
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}
	}

	aggregatedError, done := ExtractErrorResponse(resp, err, body, authError)
	if done {
		return aggregatedError
	}

	return nil
}

func ExtractErrorResponse(resp *http.Response, err error, body []byte, authError AuthError) (*AggregatedError, bool) {
	if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusCreated {
		err = json.Unmarshal(body, &authError)
		if err != nil {
			return &AggregatedError{Error: err, AuthErrorMsg: nil}, true
		}
		return &AggregatedError{Error: nil, AuthErrorMsg: &authError}, true
	}
	return nil, false
}

func (a *Authentication) UpdateAccount(username, password string, locked bool) (*AggregatedError, int) {
	var (
		authError             AuthError
		updateAccountResponse UpdateAccountResponse
	)

	jsonStr, err := json.Marshal(&UpdateAccount{Username: username, Password: password, Locked: locked})
	request, err := http.NewRequest("POST", a.Import, bytes.NewBuffer(jsonStr))
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, 0
	}

	err, resp := a.SetHeadersAndPerformRequest(request, username, password, false)
	defer resp.Body.Close()

	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, 0
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, 0
	}

	aggregatedError, done := ExtractErrorResponse(resp, err, body, authError)
	if done {
		return aggregatedError, 0
	}

	err = json.Unmarshal(body, &updateAccountResponse)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, 0
	}

	id, err := strconv.Atoi(updateAccountResponse.Result.Id)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, 0
	}

	return nil, id
}

func (a *Authentication) Login(username, password string) (*AggregatedError, *string) {
	var (
		authError       AuthError
		accountResponse UpdateAccountResponse
	)
	jsonStr, err := json.Marshal(&LoginAccount{Username: username, Password: password})
	request, err := http.NewRequest("POST", a.SessionBase, bytes.NewBuffer(jsonStr))
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	err, resp := a.SetHeadersAndPerformRequest(request, username, password, true)
	defer resp.Body.Close()
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	aggregatedError, done := ExtractErrorResponse(resp, err, body, authError)
	if done {
		return aggregatedError, nil
	}

	err = json.Unmarshal(body, &accountResponse)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	return nil, &accountResponse.Result.Id
}

func (a *Authentication) RefreshToken() (*AggregatedError, *TokenId) {
	var (
		result TokenId
	)

	request, err := http.NewRequest("GET", a.RefreshSession, nil)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	err, resp := a.SetHeadersAndPerformRequest(request, "", "", true)
	defer resp.Body.Close()
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	err = json.Unmarshal(body, &result)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	return nil, &result
}

func (a *Authentication) LogOut() *AggregatedError {
	request, err := http.NewRequest("DELETE", a.SessionBase, nil)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}
	}

	err, resp := a.SetHeadersAndPerformRequest(request, "", "", true)
	defer resp.Body.Close()
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}
	}

	if resp.StatusCode != http.StatusOK {
		err := errors.New("Unable to log out account")
		return &AggregatedError{Error: err, AuthErrorMsg: nil}
	}

	return nil
}

func (a *Authentication) GetJwks(uri string) (*AggregatedError, *JsonKeys) {
	var jwtKeys JsonKeys
	var url string

	if uri == "" {
		url = a.Origin + "/jwks"
	} else {
		url = "http://" + uri
	}

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	err, resp := a.SetHeadersAndPerformRequest(request, "", "", true)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	err = json.Unmarshal(body, &jwtKeys)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	return nil, &jwtKeys
}

func (a *Authentication) GetJwtPublicKey() (*AggregatedError, *JwtConfiguration) {
	var jwtConfig JwtConfiguration
	url := a.Origin + "/configuration"
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	err, _ = a.SetHeadersAndPerformRequest(request, "", "", true)
	if err != nil {
		return &AggregatedError{Error: err, AuthErrorMsg: nil}, nil
	}

	return nil, &jwtConfig
}
