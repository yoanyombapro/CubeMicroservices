package version

// VERSION specifies the current version tag of this build or container
var VERSION = "latest"

// REVISION specifies the current revisions to the version tag for this respectvice build or container
var REVISION = "unknown"
