package jwtutils

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

// JwtCustomClaims witholds the claims id and Structured version of Claims Section, as referenced at
// https://tools.ietf.org/html/rfc7519#section-4.1
type JwtCustomClaims struct {
	ID uint32 `json:"id"`
	jwt.StandardClaims
}

// GenerateToken takes as input the id to sign, the issuer and the secret and generates a jwt web token
func GenerateToken(id uint32, issuer, secret string) (string, error) {
	claims := &JwtCustomClaims{
		id,
		jwt.StandardClaims{
			Issuer:    issuer,
			ExpiresAt: time.Now().Add(time.Minute * 60).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	t, err := token.SignedString([]byte(secret))
	if err != nil {
		return "", err
	}

	return t, nil
}
