package testutils

import (
	crypto "crypto/rand"
	"fmt"
	"io"
	"math/rand"
	"time"
)

// RandomId generates a random uint32 id value
func RandomId() uint32 {
	rand.Seed(time.Now().UnixNano())
	var a = rand.Uint32()
	a %= 10000 - 1000
	a += 1000
	return a
}

// NewUUID generates a random UUID according to RFC 4122
func NewUUID() (string, error) {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(crypto.Reader, uuid)
	if n != len(uuid) || err != nil {
		return "", err
	}
	// variant bits
	uuid[8] = uuid[8]&^0xc0 | 0x80
	// version 4 (pseudo-random)
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:]), nil
}
