package database

import (
	"context"
	"testing"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/testutils"
)

// TestCreateUser test a user is properly created in the backend database
func TestCreateUser(t *testing.T) {
	db, cleaner := InitializeTestSetup()
	defer cleaner()

	user, err := GenerateTestUser()
	expectedEmptyError(t, err)

	createdUser, err := db.CreateUser(context.TODO(), *user)
	expectedEmptyError(t, err)

	if createdUser.UserName != user.UserName {
		t.Errorf("user fields do not match")
	}

	// test the error case
	// test case user already exists in the database
	// this should throw an error
	_, err = db.CreateUser(context.TODO(), *createdUser)
	expectedNonEmptyError(t, err)
}

// TestGetUserByID attempts to get a user from the database by user id
func TestGetUserByID(t *testing.T) {
	db, cleaner := InitializeTestSetup()
	defer cleaner()

	// test fail cases when user does not exist
	randomId := testutils.RandomId()
	_, err := db.GetUserByID(context.TODO(), randomId)
	expectedNonEmptyError(t, err)

	// create user
	user, err := GenerateTestUser()
	expectedEmptyError(t, err)

	// err = db.Engine.Save(&user).Error
	createdUser, err := db.CreateUser(context.TODO(), *user)
	expectedEmptyError(t, err)

	// attempt to get user by id
	foundUser, err := db.GetUserByID(context.TODO(), createdUser.Id)
	expectedEmptyError(t, err)

	if user.UserName != foundUser.UserName {
		t.Errorf("user fields don't match")
		t.Errorf("invalid fields- expected %s but got %s", user.UserName, foundUser.UserName)
	}
}

// TestCreateUserProfile attempts to create a user profile if it doesnt exist
func TestCreateUserProfile(t *testing.T) {
	// setup database prior to testing
	db, cleaner := InitializeTestSetup()
	defer cleaner()

	// test the fail cases in which the user account does not exist
	randomId := testutils.RandomId()
	userProfile, err := GenerateTestUserProfile()
	expectedEmptyError(t, err)

	_, err = db.CreateUserProfile(context.TODO(), randomId, *userProfile)
	expectedNonEmptyError(t, err)

	// create an actual test user
	testUser := CreateAndSaveTestUser(t, db)
	// attempt to create a profile and tie it to this given user
	_, err = db.CreateUserProfile(context.TODO(), testUser.Id, *userProfile)
	expectedEmptyError(t, err)
}

// TestCreateUserSubscription attempts to create a subscription if it doesnt exist
func TestCreateUserSubscription(t *testing.T) {
	// setup database prior to testing
	db, cleaner := InitializeTestSetup()
	defer cleaner()

	// test fail case. Case in which no user account exists for the given subscription
	randomId := testutils.RandomId()
	subscription, err := GenerateTestSubscription()
	expectedEmptyError(t, err)

	ctx := context.TODO()

	err = db.CreateUserSubscription(ctx, randomId, *subscription)
	expectedNonEmptyError(t, err)

	// create user account and tie the subscription to such account
	userAccount := CreateAndSaveTestUser(t, db)
	err = db.CreateUserSubscription(ctx, userAccount.Id, *subscription)
	expectedEmptyError(t, err)
}

// TestUpdateUser attempts to create a user account and update it
func TestUpdateUser(t *testing.T) {
	db, cleaner := InitializeTestSetup()
	defer cleaner()

	user, err := GenerateTestUser()
	expectedEmptyError(t, err)

	ctx := context.TODO()

	createdUser, err := db.CreateUser(ctx, *user)
	expectedEmptyError(t, err)

	if createdUser.UserName != user.UserName {
		t.Errorf("user fields do not match")
	}

	userToUpdate := createdUser
	userToUpdate.UserName = "UpdatedUsername"
	userToUpdate.Email = "UpdatedEmail"
	userToUpdate.IsActive = true

	// attempt to update the user
	updatedUser, err := db.UpdateUser(ctx, userToUpdate.Id, *userToUpdate)
	expectedEmptyError(t, err)

	if updatedUser == nil {
		t.Errorf("Updated user nil")
	}
}

func expectedEmptyError(t *testing.T, err error) {
	if err != nil {
		t.Errorf("Expected empty error'. Got '%v'", err)
	}
}

func expectedNonEmptyError(t *testing.T, err error) {
	if err == nil {
		t.Errorf("Expected non empty error'. Got '%v'", err)
	}
}
