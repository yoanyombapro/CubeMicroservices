package database

import (
	"context"
	"os"

	"github.com/jinzhu/gorm"
	"go.uber.org/zap"
	"gopkg.in/gormigrate.v1"

	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
)

// IDatabase defines the interface definition for this service's database
type IDatabase interface {
	CreateUser(ctx context.Context, user model.User) error
	CreateUserProfile(userID int32, profile model.Profile) error
	CreateUserSubscription(userID int32, subscription model.Subscriptions) error

	GetUserByID(userID int32) (error, *model.User)
	DoesUserExists(userID int32, username, email string) (error, bool)
	DoesUserProfileExists(userID, profileID int32) (error, bool)
	DoesSubscriptionExist(subscriptionName string, userID int32) (error, bool)
	DoesSubscriptionExistById(userID, subscriptionID int32) (error, bool)

	UpdateUser(userID int32, user model.User) (error, *model.User)
	UpdateUserProfile(userID, profileID int32, profile model.Profile) (error, *model.Profile)
	UpdateUserSubscription(userID int32, subscriptionID int32, subscription model.Subscriptions) (error, *model.Subscriptions)

	DeleteUser(userID int32) error
	DeleteUserProfile(userID, profileID int32) error
	DeleteUserSubscription(userID, subscriptionID int32) error

	GetAllUsers(limit int32) (error, []model.User)
	GetAllUsersByAccountType(accountType string, limit int32) (error, []model.User)
	GetAllUsersByIntent(intent string, limit int32) (error, []model.User)
	GetUserProfile(userID int32) (error, model.Profile)
	GetUserProfiles(limit int32) (error, []model.Profile)
	GetAllUserProfilesByType(profileType string, limit int32) (error, []model.Profile)
	GetAllUserProfilesByNationality(nationality string, limit int32) (error, []model.Profile)
	GetAllUserSubscriptions(userID int32) (error, []model.Subscriptions)

	CreateTeam(team model.Team) error
	CreateTeamProfile(teamID int32, profile model.TeamProfile) error
	CreateOrUpdateTeamSubscription(teamID int32, subscription model.Subscriptions) (e0 error, m1 *model.Team)

	AddMemberToTeamOrUpdate(teamID int32, member model.User, isAddOperation bool) (e0 error, m1 model.Team)
	AddAdvisorToTeamOrUpdate(teamID int32, advisor model.User, isAddOperation bool) (e0 error, m1 model.Team)
	UpdateTeam(teamID int32, team model.Team) (e0 error, m1 *model.Team)
	UpdateTeamProfile(teamID, teamProfileID int32, profile model.TeamProfile) (error, *model.Team)
	UpdateTeamAdmin(teamID, adminID int32, admin model.User) (error, *model.Team)

	/*
		UpdateTeam(team_id int32, team model.Team)
		UpdateTeamProfile(team_id, team_profile_id int32, profile model.TeamProfile) error
		UpdateTeamAdmin(team_id, admin_id int32, admin model.User) error
		UpdateTeamMember(team_id, member_id int32, member model.User) error
		UpdateTeamAdvisor(team_id, advisor_id int32, advisor model.User) error
		UpdateTeamSubscription(team_id, subscription_id int32, subscription model.Subscriptions) error

		DeleteTeam(team_id int32) error
		DeleteTeamProfile(team_id, team_profile_id int32) error
		DeleteTeamMember(team_id, member_id int32) error
		DeleteTeamAdvisor(team_id, advisor_id int32) error
		DeleteTeamAdmin(team_id, admin_id int32) error

		GetTeam(team_id int32) (error, *model.Team)
		GetAllTeams(limit int32) (error, []*model.Team)
		GetAllTeamsByType(team_type string, limit int32) (error, []*model.Team)
		GetAllTeamsByIndustry(industry string, limit int32) (error, []*model.Team)
		GetAllTeamsByNumberOfEmployees(numEmployees, limit int32) (error, []*model.Team)

		GetTeamProfile(team_id, profile_id int32) (error, *model.TeamProfile)
		GetAllTeamProfiles(profiles int32) (error, []*model.TeamProfile)
		GetAllProfilesByTag(tag string, limit int32) (error, []*model.TeamProfile)

		GetTeamSubscription(team_id, subscription_id int32) (error, *model.Subscriptions)
		GetTeamSubscriptions(team_id int32) (error, []*model.Subscriptions)
	*/
}

// Database witholds the a reference to the underlying database connection handle
// and a logging entity useful for interacting with the current database connection
type Database struct {
	Engine *gorm.DB
	Logger *zap.Logger
}

// Transaction is a type serving as a function decorator for common database transactions
type Transaction func(tx *gorm.DB) error

// ComplexTransaction is a type serving as a function decorator for complex database transactions
type ComplexTransaction func(tx *gorm.DB) (interface{}, error)

var (
	postgres = "postgres"
)

// New creates a database connection and returns the connection object
func New(conn string, logger *zap.Logger) (*Database, error) {
	db, err := gorm.Open(postgres, conn)

	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}

	logger.Info("Successfully connected to the database")

	db.SingularTable(true)
	db.LogMode(false)
	db = db.Set("gorm:auto_preload", true)

	logger.Info("Migrating database schema")

	err = CreateTablesOrMigrateSchemas(db, logger)
	if err != nil {
		logger.Error(err.Error())
		os.Exit(1)
	}

	logger.Info("Successfully migrated database")

	return &Database{
		Engine: db,
		Logger: logger,
	}, nil
}

// CreateTablesOrMigrateSchemas creates a given set of models based on a schema
// if it does not exist or migrates the model schemas to the latest version
func CreateTablesOrMigrateSchemas(db *gorm.DB, logger *zap.Logger) error {
	migration := gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{
		{
			ID: "20200416",
			Migrate: func(tx *gorm.DB) error {
				return tx.AutoMigrate(
					model.AddressORM{},
					model.EducationORM{},
					model.MediaORM{},
					model.SubscriptionsORM{},
					model.SocialMediaORM{},
					model.DetailsORM{},
					model.ExperienceORM{},
					model.InvestmentORM{},
					model.UserORM{},
					model.ProfileORM{},
					model.GroupORM{},
					model.TeamORM{},
					model.TeamProfileORM{},
					model.InvestorDetailORM{},
					model.StartupDetailORM{},
					model.SettingsORM{},
					model.LoginActivityORM{},
					model.PaymentsORM{},
					model.CardORM{},
					model.PinORM{},
					model.Privacy{},
					model.NotificationORM{},
					model.PostAndCommentsPushNotificationORM{},
					model.FollowingAndFollowersPushNotificationORM{},
					model.DirectMessagesPushNotificationORM{},
					model.EmailAndSmsPushNotificationORM{}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable(
					model.AddressORM{},
					model.EducationORM{},
					model.MediaORM{},
					model.SubscriptionsORM{},
					model.SocialMediaORM{},
					model.DetailsORM{},
					model.ExperienceORM{},
					model.InvestmentORM{},
					model.UserORM{},
					model.ProfileORM{},
					model.GroupORM{},
					model.TeamORM{},
					model.TeamProfileORM{},
					model.InvestorDetailORM{},
					model.StartupDetailORM{},
					model.SettingsORM{},
					model.LoginActivityORM{},
					model.PaymentsORM{},
					model.CardORM{},
					model.PinORM{},
					model.Privacy{},
					model.NotificationORM{},
					model.PostAndCommentsPushNotificationORM{},
					model.FollowingAndFollowersPushNotificationORM{},
					model.DirectMessagesPushNotificationORM{},
					model.EmailAndSmsPushNotificationORM{}).Error
			},
		},
	})

	err := migration.Migrate()
	if err != nil {
		return err
	}

	return nil
}

// CreateTableForTests creates a given set of models based on a schema
// model for tests purposes
func CreateTableForTests(db *gorm.DB) error {
	migration := gormigrate.New(db, gormigrate.DefaultOptions, []*gormigrate.Migration{
		{
			ID: "20200523",
			Migrate: func(tx *gorm.DB) error {
				return tx.CreateTable(
					model.AddressORM{},
					model.EducationORM{},
					model.MediaORM{},
					model.SubscriptionsORM{},
					model.SocialMediaORM{},
					model.DetailsORM{},
					model.ExperienceORM{},
					model.InvestmentORM{},
					model.UserORM{},
					model.ProfileORM{},
					model.GroupORM{},
					model.TeamORM{},
					model.TeamProfileORM{},
					model.InvestorDetailORM{},
					model.StartupDetailORM{},
					model.SettingsORM{},
					model.LoginActivityORM{},
					model.PaymentsORM{},
					model.CardORM{},
					model.PinORM{},
					model.Privacy{},
					model.NotificationORM{},
					model.PostAndCommentsPushNotificationORM{},
					model.FollowingAndFollowersPushNotificationORM{},
					model.DirectMessagesPushNotificationORM{},
					model.EmailAndSmsPushNotificationORM{}).Error
			},
			Rollback: func(tx *gorm.DB) error {
				return tx.DropTable(
					model.AddressORM{},
					model.EducationORM{},
					model.MediaORM{},
					model.SubscriptionsORM{},
					model.SocialMediaORM{},
					model.DetailsORM{},
					model.ExperienceORM{},
					model.InvestmentORM{},
					model.UserORM{},
					model.ProfileORM{},
					model.GroupORM{},
					model.TeamORM{},
					model.TeamProfileORM{},
					model.InvestorDetailORM{},
					model.StartupDetailORM{},
					model.SettingsORM{},
					model.LoginActivityORM{},
					model.PaymentsORM{},
					model.CardORM{},
					model.PinORM{},
					model.Privacy{},
					model.NotificationORM{},
					model.PostAndCommentsPushNotificationORM{},
					model.FollowingAndFollowersPushNotificationORM{},
					model.DirectMessagesPushNotificationORM{},
					model.EmailAndSmsPushNotificationORM{}).Error
			},
		},
	})

	err := migration.Migrate()
	if err != nil {
		return err
	}

	return nil
}
