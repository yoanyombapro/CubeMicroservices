package database

import (
	"context"
	"errors"

	"github.com/jinzhu/gorm"
	"go.uber.org/zap"

	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
)

// GetUserIfExists checks that a given user exists based on user id
func (db *Database) GetUserIfExists(userID uint32, username, email string) (bool, *model.UserORM, error) {
	transaction := func(tx *gorm.DB) (interface{}, error) {
		// check if the user exists based on username, email and id
		var user model.UserORM

		if userID != 0 && username != "" && email != "" {
			if recordNotFound := tx.Where(&model.UserORM{Id: userID, Email: email, UserName: username}).Find(&user).RecordNotFound(); recordNotFound {
				return nil, errors.New("user does not exits")
			}
		} else if userID != 0 && username == "" && email == "" {
			// user name and email is empty so obtain user by querying for user id
			if recordNotFound := tx.Where(&model.UserORM{Id: userID}).Find(&user).RecordNotFound(); recordNotFound {
				return nil, errors.New("user does not exits")
			}
		} else if userID == 0 && username != "" && email != "" {
			// user name and email is empty so obtain user by querying for user id
			if recordNotFound := tx.Where(&model.UserORM{Email: email, UserName: username}).Find(&user).RecordNotFound(); recordNotFound {
				return nil, errors.New("user does not exits")
			}
		} else if userID == 0 && username != "" && email == "" {
			// user name and email is empty so obtain user by querying for user id
			if recordNotFound := tx.Where(&model.UserORM{UserName: username}).Find(&user).RecordNotFound(); recordNotFound {
				return nil, errors.New("user does not exits")
			}
		} else if userID == 0 && username == "" && email != "" {
			// user name and email is empty so obtain user by querying for user id
			if recordNotFound := tx.Where(&model.UserORM{Email: email}).Find(&user).RecordNotFound(); recordNotFound {
				return nil, errors.New("user does not exits")
			}
		} else {
			return nil, errors.New("Invalid input parameters")
		}

		// Convert the userORM to user object and perform validation checks
		ctx := context.TODO()
		userObj, err := user.ToPB(ctx)
		if err != nil {
			return nil, err
		}

		// Actually perform user field validation
		if err := userObj.Validate(); err != nil {
			return nil, err
		}

		return &user, nil
	}

	output, err := db.PerformComplexTransaction(transaction)
	if err != nil {
		return false, nil, err
	}

	userAccount := output.(*model.UserORM)
	return true, userAccount, nil
}

// CheckIfUserExistsByUsername checks that a given user exists based on a user account username
func (db *Database) CheckIfUserExistsByUsername(username string) (bool, *model.UserORM, error) {
	transaction := func(tx *gorm.DB) (interface{}, error) {
		// check if the user exists based on username, email and id
		var user model.UserORM

		// assert the username passed in is not empty and attempts to obtain user with the specified username
		if username != "" {
			if recordNotFound := tx.Where(&model.UserORM{UserName: username}).Find(&user).RecordNotFound(); recordNotFound {
				return nil, errors.New("user does not exits")
			}
		} else {
			return nil, errors.New("Invalid input parameters")
		}

		// Convert the userORM to user object and perform validation checks
		ctx := context.TODO()
		userObj, err := user.ToPB(ctx)
		if err != nil {
			return nil, err
		}

		// Actually perform field validation
		if err := userObj.Validate(); err != nil {
			return nil, err
		}

		return &user, nil
	}

	output, err := db.PerformComplexTransaction(transaction)
	if err != nil {
		return false, nil, err
	}

	userAccount := output.(*model.UserORM)
	return true, userAccount, nil
}

// GetSubscriptionIfExistsByName checks that a given subscription exists by name and user id to which it is tied to
func (db *Database) GetSubscriptionIfExistsByName(subscriptionName string, userID uint32) (bool, *model.SubscriptionsORM, error) {
	transaction := func(tx *gorm.DB) (interface{}, error) {
		var userOrm *model.UserORM
		var subscriptions []*model.SubscriptionsORM

		// obtains the user by id if it exists
		exists, userOrm, err := db.GetUserIfExists(userID, "", "")
		if !exists {
			return nil, err
		}

		// obtain all the subscriptions from the user
		subscriptions = userOrm.Subscriptions

		// iterate over all such subscriptions searching for the subscription name of interest
		for _, subscription := range subscriptions {
			if subscription.SubscriptionName == subscriptionName {
				return subscription, nil
			}
		}

		return nil, errors.New("subscription does not exist")
	}

	output, err := db.PerformComplexTransaction(transaction)
	if err != nil {
		return false, nil, err
	}

	subscription := output.(*model.SubscriptionsORM)
	return true, subscription, nil
}

// GetSubscriptionExistById checks that a given subscription exists by subscription id and the account id it is tied to
func (db *Database) GetSubscriptionExistById(userID, subscriptionID uint32) (bool, *model.SubscriptionsORM, error) {
	transaction := func(tx *gorm.DB) (interface{}, error) {
		var userOrm *model.UserORM
		var subscriptions []*model.SubscriptionsORM

		// obtain user by id
		exists, userOrm, err := db.GetUserIfExists(userID, "", "")
		if !exists {
			return nil, err
		}

		// obtain all the subscriptions from the user
		subscriptions = userOrm.Subscriptions

		// iterate over all such subscriptions searching for the subscription id of interest
		for _, subscription := range subscriptions {
			if subscription.Id == subscriptionID {
				return subscription, nil
			}
		}

		return nil, errors.New("subscription does not exist")
	}

	output, err := db.PerformComplexTransaction(transaction)
	if err != nil {
		return false, nil, err
	}

	subscription := output.(*model.SubscriptionsORM)
	return true, subscription, nil
}

// GetUserProfileIfExists checks that a given profile exists for a given user based on user id
func (db *Database) GetUserProfileIfExists(userId uint32) (bool, *model.ProfileORM, error) {
	transaction := func(tx *gorm.DB) (interface{}, error) {
		var profile *model.ProfileORM
		var userOrm *model.UserORM

		// preliminarily, check if the user account exists
		exists, userOrm, err := db.GetUserIfExists(userId, "", "")
		if !exists {
			db.Logger.Error("error", zap.Error(err))
			return nil, errors.New("user account does not exist. Create an account before a profile")
		}

		// after validating the user object's fields, obtain the profile of interest from the user table
		profile = userOrm.Profile

		ctx := context.TODO()

		// convert the returned userORM to a user object and validate that all
		// fields of interest are present
		userProfile, err := profile.ToPB(ctx)
		if err != nil {
			return nil, err
		}

		// if the user object witholds invalid fields, return an error
		if err = userProfile.Validate(); err != nil {
			return nil, err
		}

		// Return an error if the obtained profile object witholds a nil id
		if profile.Id == 0 {
			return nil, errors.New("profile of interest does not exist")
		}

		return profile, nil
	}

	output, err := db.PerformComplexTransaction(transaction)
	if err != nil {
		return false, nil, err
	}

	profile := output.(*model.ProfileORM)
	return true, profile, nil
}
