package database

import (
	"testing"

	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/testutils"
)

// TestGetUserIfExists tests a user can be obtained from the database if it exists
func TestGetUserIfExists(t *testing.T) {
	db, cleaner := InitializeTestSetup()
	defer cleaner()

	// preliminary test deals with lack of existence. Hence we test for the case in which
	// the user does not exist
	// generate a random number
	id := testutils.RandomId()

	// attempt to get user if it exist. Note this call should fail
	_, _, err := db.GetUserIfExists(id, "", "")
	if err == nil {
		t.Errorf("Expected non empty error field to be returned'. Got '%v'", err)
	}

	user := CreateAndSaveTestUser(t, db)

	// attempt to obtain the user again by both id, username, and email
	exist, savedUser, err := db.GetUserIfExists(user.Id, user.UserName, user.Email)
	if !exist {
		t.Errorf("Expected user to exist. Got a non empty error field'. Got '%v'", err)
	}

	// attempt to get the user from just the id
	exist, savedUser, err = db.GetUserIfExists(user.Id, "", "")
	if !exist {
		t.Errorf("Expected user to exist. Got a non empty error field'. Got '%v'", err)
	}

	// attempt to get the user from just by the id
	exist, savedUser, err = db.GetUserIfExists(0, user.UserName, user.Email)
	if !exist {
		t.Errorf("Expected user to exist. Got a non empty error field'. Got '%v'", err)
	}

	// attempt to get the user just by email
	exist, savedUser, err = db.GetUserIfExists(0, "", user.Email)
	if !exist {
		t.Errorf("Expected user to exist. Got a non empty error field'. Got '%v'", err)
	}

	// attempt to get the user just by username
	exist, savedUser, err = db.GetUserIfExists(0, user.UserName, "")
	if !exist {
		t.Errorf("Expected user to exist. Got a non empty error field'. Got '%v'", err)
	}

	if savedUser != nil && user != nil {
		if savedUser.UserName != user.UserName ||
			savedUser.Email != user.Email ||
			savedUser.FirstName != user.FirstName ||
			savedUser.LastName != user.LastName ||
			savedUser.Intent != user.Intent ||
			savedUser.UserAccountType != user.UserAccountType {
			t.Errorf("Created user and saved user do not have matching fields")
		}
	}
}

// TestCheckIfUserExistsByUsername tests that a user can be obtained by username if it exists
func TestCheckIfUserExistsByUsername(t *testing.T) {
	db, cleaner := InitializeTestSetup()
	defer cleaner()

	// initially, we test lack of existence. We generate a random string for user
	// name and attempt to check if it exists in the database
	randomUUID, err := testutils.NewUUID()
	if err != nil {
		t.Errorf("Expected non empty error'. Got '%v'", err)
	}
	userName := "testUserName" + randomUUID
	exists, _, err := db.CheckIfUserExistsByUsername(userName)
	if exists {
		t.Errorf("User should not exist")
	}

	// create a test user in the database and test it exists
	user := CreateAndSaveTestUser(t, db)
	exists, savedUser, err := db.CheckIfUserExistsByUsername(user.UserName)
	if !exists {
		t.Errorf("User should exist. Expected empty error'. Got '%v'", err)
	}

	if savedUser != nil && savedUser.UserName != user.UserName {
		t.Errorf("Username fields do not match")
	}
}

// TestGetSubscriptionIfExistsByName attempts to get a subscription by name from the databases
func TestGetSubscriptionIfExistsByName(t *testing.T) {
	db, cleaner := InitializeTestSetup()
	defer cleaner()

	// initiially we test fail cases. so the case a subscription does not exist
	randomUUID, err := testutils.NewUUID()
	if err != nil {
		t.Errorf("Expected non empty error'. Got '%v'", err)
	}

	subscriptionName := "testSubscription" + randomUUID
	accountId := testutils.RandomId()

	exists, _, err := db.GetSubscriptionIfExistsByName(subscriptionName, accountId)
	if exists {
		t.Errorf("Subscription should not exist. Expected a non empty error'. Got '%v'", err)
	}

	// create and save a subscription to a database
	accountId, subscriptionName, _ = CreateAndSaveTestSubscription(t, db)
	exists, _, err = db.GetSubscriptionIfExistsByName(subscriptionName, accountId)
	if !exists {
		t.Errorf("Subscription should  exist. Expected an empty error'. Got '%v'", err)
	}
}

// TestGetSubscriptionIfExistsByID attempts to get a subscription by id from the database
func TestGetSubscriptionIfExistsByID(t *testing.T) {
	db, cleaner := InitializeTestSetup()
	defer cleaner()

	subscriptionId := testutils.RandomId()
	accountId := testutils.RandomId()

	// initially test the fail case. user subscription does not exist
	exists, _, err := db.GetSubscriptionExistById(accountId, subscriptionId)
	if exists {
		t.Errorf("Subscription should not exist. Expected a non empty error'. Got '%v'", err)
	}

	// create and save a subscription to a database
	accountId, _, subscriptionId = CreateAndSaveTestSubscription(t, db)
	exists, _, err = db.GetSubscriptionExistById(accountId, subscriptionId)
	if !exists {
		t.Errorf("Subscription should  exist. Expected an empty error'. Got '%v'", err)
	}
}

// TestGetUserProfileIfExists attempts to obtain a user profile from the database
func TestGetUserProfileIfExists(t *testing.T) {
	db, cleaner := InitializeTestSetup()
	defer cleaner()

	// test the fail case
	accountId := testutils.RandomId()
	exists, _, err := db.GetUserProfileIfExists(accountId)
	if exists {
		t.Errorf("Expected non-empty error'. Got '%v'", err)
	}

	accountId = CreateAndSaveTestUserProfile(t, db)
	exists, profile, err := db.GetUserProfileIfExists(accountId)
	if !exists {
		t.Errorf("Expected empty error'. Got '%v'", err)
	}

	// assert the profile is empty
	if *profile.UserId != accountId {
		t.Errorf("profile id fields don't match")
	}
}
