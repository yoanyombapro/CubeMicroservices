package database

import (
	"context"
	"errors"

	"github.com/jinzhu/copier"
	"github.com/jinzhu/gorm"
	"go.uber.org/zap"

	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
)

// CreateTeam creates a team record in the backend database if it doesn't already
// exists through a database transaction.
func (db *Database) CreateTeam(ctx context.Context, team model.Team, adminID uint32) (uint32, error) {
	transaction := func(tx *gorm.DB) (interface{}, error) {
		var teamAccount model.TeamORM
		// test if the admin even exist
		exists, userAdmin, err := db.GetUserIfExists(adminID, "", "")
		if !exists {
			db.Logger.Error(err.Error())
			return nil, err
		}

		// sanity check that the team of interest does indeed have an email and name
		if team.Name == "" || team.Email == "" {
			db.Logger.Error("team must have an email and name")
			return nil, errors.New("team account must have an email and name")
		}

		// validate team fields and convert to an ORM time
		if err := team.Validate(); err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		newTeamAccount, err := team.ToORM(ctx)
		if err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		recordNotFound := tx.Where(&model.TeamORM{Email: team.Email}).First(&teamAccount).RecordNotFound()

		// reactivate the team account if it exists already in
		// the backend data store and it is not active
		if !recordNotFound && !teamAccount.IsActive {
			// team account exists. check if the
			// team is active if not reactivate the team
			// by first performing a deep copy and saving the updated team
			if err := copier.Copy(&teamAccount, newTeamAccount); err != nil {
				db.Logger.Error(err.Error())
				return nil, err
			}

			if err := tx.Save(&teamAccount).Error; err != nil {
				db.Logger.Error(err.Error())
				return nil, err
			}
			db.Logger.Info("team account successfully reactivated")
			return teamAccount.Id, nil
		} else if !recordNotFound {
			db.Logger.Error("team account already exists")
			return nil, errors.New("team account already exists")
		}

		// generate a random reset token for the team account
		// and set it
		GenerateTokenAndUpdate(20, teamAccount.ResetTokenExpiration)

		// add admin reference to the team
		newTeamAccount.Admin = userAdmin

		// save the record in the database
		if err = tx.Create(&newTeamAccount).Error; err != nil {
			db.Logger.Error(err.Error())
			return err, nil
		}

		db.Logger.Info("team account successfully created")

		return newTeamAccount.Id, nil
	}

	output, err := db.PerformComplexTransaction(transaction)
	if err != nil {
		return 0, err
	}

	teamAccountID := output.(uint32)
	return teamAccountID, nil
}

// GetTeamByID gets a team based on a passed in id. It performs the operation in
// a transaction.
func (db *Database) GetTeamByID(ctx context.Context, teamID uint32) (*model.Team, error) {
	transaction := func(tx *gorm.DB) (interface{}, error) {
		// assert team id is not malformed
		if teamID == 0 {
			db.Logger.Error("invalid team id")
			return nil, errors.New("invalid team id")
		}

		// attempt to obtain the team from the database
		exists, teamAccount := db.getTeamIfExist(teamID, tx)
		if !exists {
			db.Logger.Error("team does not exist")
			return nil, errors.New("team does not exist")
		}

		team, err := teamAccount.ToPB(ctx)
		if err != nil {
			db.Logger.Error(err.Error())
			return err, nil
		}

		db.Logger.Info("successfully obtained team by id")
		return &team, nil
	}

	output, err := db.PerformComplexTransaction(transaction)

	if err != nil {
		return nil, err
	}

	teamAccount := output.(*model.Team)
	return teamAccount, nil
}

// DeleteTeamByID deletes a team based on a passed in id. It performs the operation in
// a transaction.
func (db *Database) DeleteTeamByID(ctx context.Context, teamID uint32) error {
	transaction := func(tx *gorm.DB) error {
		// assert team id is not malformed
		if teamID == 0 {
			db.Logger.Error("invalid team id")
			return errors.New("invalid team id")
		}

		// attempt to obtain the team from the database
		exists, team := db.getTeamIfExist(teamID, tx)
		if !exists {
			db.Logger.Error("team does not exist")
			return errors.New("team does not exist")
		}

		if err := tx.Where(model.TeamORM{Id: teamID}).Delete(&team).Error; err != nil {
			db.Logger.Error(err.Error())
			return err
		}

		db.Logger.Info("successfully deleted team by ID", zap.String("teamId", string(teamID)))
		return nil
	}

	return db.PerformTransaction(transaction)
}

// CreateTeamProfile creates a team profile in the backend database assuming
// that a team account already exists.
func (db *Database) CreateTeamProfile(ctx context.Context, teamID uint32, profile model.TeamProfile) (uint32, error) {
	transaction := func(tx *gorm.DB) (interface{}, error) {
		var teamAccount model.TeamORM
		if recordNotFound := tx.Where(model.TeamORM{Id: teamID}).First(&teamAccount).RecordNotFound(); recordNotFound {
			db.Logger.Error("team account does not exist")
			return nil, errors.New("team account does not exist")
		}

		// error out if the team profile already exists for a given account
		if teamAccount.TeamProfile != nil {
			db.Logger.Error("team profile already exists")
			return nil, errors.New("team profile already exists")
		}

		// convert the profile object passed as input parameter into an ORM type
		teamProfile, err := profile.ToORM(ctx)
		if err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		// add an autogenerated team profile id
		teamAccount.TeamProfile = &teamProfile

		// save the team  type
		if err := tx.Save(&teamAccount).Error; err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		db.Logger.Info("successfully created team profile",
			zap.String("teamId", string(teamID)), zap.String("teamProfileId", string(teamAccount.TeamProfile.Id)))

		return teamProfile.Id, nil
	}

	output, err := db.PerformComplexTransaction(transaction)

	if err != nil {
		return 0, err
	}

	teamProfileID := output.(uint32)
	return teamProfileID, nil
}

// GetTeamProfileByID gets a team profile based on a passed in team account id.
func (db *Database) GetTeamProfileByID(ctx context.Context, teamID uint32) (*model.TeamProfile, error) {
	transaction := func(tx *gorm.DB) (interface{}, error) {
		// assert team id is not malformed
		if teamID == 0 {
			db.Logger.Error("invalid id")
			return nil, errors.New("invalid team id")
		}

		// attempt to obtain the team from the database
		exists, teamAccount := db.getTeamIfExist(teamID, tx)
		if !exists {
			db.Logger.Error("team does not exist")
			return nil, errors.New("team does not exist")
		}

		if teamAccount.TeamProfile == nil {
			db.Logger.Error("team profile does not exist")
			return nil, errors.New("team profile does not exist")
		}

		// obtain the team profile from the team account
		teamProfile := teamAccount.TeamProfile

		profile, err := teamProfile.ToPB(ctx)
		if err != nil {
			db.Logger.Error(err.Error())
			return err, nil
		}

		db.Logger.Info("successfully obtained team by profile id", zap.String("id", string(teamID)))

		return &profile, nil
	}

	output, err := db.PerformComplexTransaction(transaction)

	if err != nil {
		return nil, err
	}

	teamProfile := output.(*model.TeamProfile)
	return teamProfile, nil
}

// DeleteTeamProfileByID deletes a team profile based on a passed in account id. It performs the operation in
// a transaction. Returned are any errors occuring during the transaction
func (db *Database) DeleteTeamProfileByID(_ context.Context, teamID uint32) error {
	transaction := func(tx *gorm.DB) error {
		var (
			teamProfileID uint32
			teamProfile   model.TeamProfileORM
		)

		// assert team id is not malformed
		if teamID == 0 {
			db.Logger.Error("invalid team id")
			return errors.New("invalid team id")
		}

		// attempt to obtain the team from the database
		exists, team := db.getTeamIfExist(teamID, tx)
		if !exists {
			db.Logger.Error("team does not exist")
			return errors.New("team does not exist")
		}

		// obtain the profile id from the team account if the profile indeed
		// exists in the data store
		if team.TeamProfile == nil {
			db.Logger.Error("profile does not exist")
			return errors.New("profile does not exist")
		}

		teamProfileID = team.TeamProfile.Id
		if err := tx.Where(model.TeamProfileORM{Id: teamProfileID}).Delete(&teamProfile).Error; err != nil {
			db.Logger.Error(err.Error())
			return err
		}

		db.Logger.Info("successfully deleted team profile", zap.String("teamId", string(teamID)))

		return nil
	}

	return db.PerformTransaction(transaction)
}

// CreateOrUpdateTeamSubscription creates or updates a team subscription and ties it to a team account. A subscription
// is only successfully created if the team account of interest already exists. Additionally if
// a subscription of the same name exists already and is tied to the user id of interest, the subscription
// data is only updated if the subscription record stored in the database is 'inactive'.
func (db *Database) CreateOrUpdateTeamSubscription(ctx context.Context, teamID uint32, subscription model.Subscriptions) (*model.Team, error) {
	transaction := func(tx *gorm.DB) (interface{}, error) {
		// validate the new subscription
		if err := subscription.Validate(); err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		newSubscriptionOrm, err := subscription.ToORM(ctx)
		if err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		// create a new subscription if it doesn't already exist
		exists, teamOrm := db.getTeamIfExist(teamID, tx)
		if !exists {
			db.Logger.Error("team account does not exist")
			return nil, errors.New("team account does not exist. please create one and try again")
		}

		team, err := teamOrm.ToPB(ctx)
		if err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		// we first check if the subscription presently exists. it is important to consider that an
		// invocation to doesSubscriptionExist could state that the subscription does not exist for a multitude of reasons
		// including : the team account does not exist, or the subscriptions list returned from the existing team account does
		// not contain the current subscription being searched. Hence below we add further checks to account for this
		subscriptionExists, oldSubsriptionOrm := db.getSubscriptionIfExist(teamID, tx, &subscription)
		if subscriptionExists && !oldSubsriptionOrm.IsActive {
			// perform deep copy into old subscription and save
			if err := copier.Copy(oldSubsriptionOrm, newSubscriptionOrm); err != nil {
				db.Logger.Error(err.Error())
				return nil, err
			}

			// save the subscription into the subscription table. by default
			// all associations of the subscription wil be updated
			if err := tx.Save(&oldSubsriptionOrm).Error; err != nil {
				db.Logger.Error(err.Error())
				return nil, err
			}

			db.Logger.Info("successfully create/updated team subscription")
			return team, nil
		} else if subscriptionExists && oldSubsriptionOrm.IsActive {
			db.Logger.Error("subscription is already active")
			return nil, errors.New("subscription is already active")
		}

		teamOrm.Subscriptions = append(teamOrm.Subscriptions, &newSubscriptionOrm)

		if err := tx.Save(&teamOrm).Error; err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		team, err = teamOrm.ToPB(ctx)
		if err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		return &team, nil
	}

	output, err := db.PerformComplexTransaction(transaction)

	if err != nil {
		return &model.Team{}, err
	}

	updatedTeam := output.(*model.Team)
	return updatedTeam, nil
}

// AddMemberToTeamOrUpdate adds or updates a new member to a team if a team account exists.
// This operation is performed within a transaction hence any errors occurring
// throughout the transaction lifecycle are returned as well as the updated team account record
func (db *Database) AddMemberToTeamOrUpdate(ctx context.Context, teamID uint32, member model.User, isAddOperation bool) (model.Team, error) {
	// TODO: Consider adding another step. Making the preliminary step a pending op until the admin accepts
	transaction := func(tx *gorm.DB) (interface{}, error) {
		newMemberAdded := false
		exists, teamOrm := db.getTeamIfExist(teamID, tx)
		if !exists {
			db.Logger.Error("team account does not exist")
			return nil, errors.New("team account does not exist. please create one and try again")
		}

		// validate user and then convert the user to a user Orm type
		if err := member.Validate(); err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		memberToAdd, err := member.ToORM(ctx)
		if err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		if teamOrm.TeamProfile == nil {
			db.Logger.Error("team profile does not exist")
			return nil, errors.New("team profile does not exist. please create one and try again")
		}

		numTeamMembers := len(teamOrm.Members)
		teamMembers := make([]*model.UserORM, numTeamMembers, numTeamMembers)

		for idx, member := range teamOrm.Members {
			// update a member if it already exists irrespective of the operation type
			if member.Email == memberToAdd.Email &&
				member.UserName == memberToAdd.UserName &&
				member.Id == memberToAdd.Id &&
				newMemberAdded == false {
				teamMembers = append(teamMembers, &memberToAdd)
				newMemberAdded = true
			} else {
				teamMembers = append(teamMembers, member)
			}

			// only add if we haven't already added the member, we are at the last iteration,
			// and this is an add operation
			if idx == numTeamMembers-1 && newMemberAdded == false && isAddOperation {
				newMemberAdded = true
				teamMembers = append(teamMembers, &memberToAdd)
			}
		}

		// if the new member id not added and the operation is an update operation
		// return an error since there doesn't exist a user to update
		if !newMemberAdded && !isAddOperation {
			db.Logger.Error("member does not exist")
			return nil, errors.New("member does not exist")
		} else if !newMemberAdded && isAddOperation {
			db.Logger.Error("unable to add member")
			return nil, errors.New("unable to add new member")
		}

		teamOrm.Members = teamMembers
		if err := tx.Save(&teamOrm).Error; err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		updatedTeam, err := teamOrm.ToPB(ctx)
		if err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		db.Logger.Info("successfully added member to team",
			zap.String("id", string(teamID)), zap.String("memberUsername", memberToAdd.UserName))
		return &updatedTeam, nil
	}

	output, err := db.PerformComplexTransaction(transaction)

	if err != nil {
		return model.Team{}, err
	}

	updatedTeam := output.(*model.Team)
	return *updatedTeam, nil
}

// AddAdvisorToTeamOrUpdate adds or updates an advisor to the team if the advisor doesnt currently exist or updates
// an existing one. This operation is performed within a transaction hence, any error occurring throughout
// the transaction lifecycle is returned, as well as the updated team account record
func (db *Database) AddAdvisorToTeamOrUpdate(ctx context.Context, teamID uint32, advisor model.User, isAddOperation bool) (model.Team, error) {
	// TODO: Consider adding another step. Making the preliminary step a pending op until the admin accepts
	transaction := func(tx *gorm.DB) (interface{}, error) {
		newAdvisorAdded := false
		exists, teamOrm := db.getTeamIfExist(teamID, tx)
		if !exists {
			db.Logger.Error("team account does not exist")
			return nil, errors.New("team account does not exist. please create one and try again")
		}

		// validate user and then convert the user to a user Orm type
		if err := advisor.Validate(); err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		advisorToAdd, err := advisor.ToORM(ctx)
		if err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		if teamOrm.TeamProfile == nil {
			db.Logger.Error("team profile does not exist")
			return nil, errors.New("team profile does not exist. please create one and try again")
		}

		numTeamMembers := len(teamOrm.Members)
		teamMembers := make([]*model.UserORM, numTeamMembers, numTeamMembers)

		for idx, advisor := range teamOrm.Advisors {
			if advisor.Email == advisorToAdd.Email &&
				advisor.UserName == advisorToAdd.UserName &&
				advisor.Id == advisorToAdd.Id &&
				newAdvisorAdded == false {
				teamMembers = append(teamMembers, &advisorToAdd)
				newAdvisorAdded = true
			} else {
				teamMembers = append(teamMembers, advisor)
			}

			// only add if we havent already added the member, we are at the last iteration,
			// and this is an add operation
			if idx == numTeamMembers-1 && newAdvisorAdded == false && isAddOperation {
				newAdvisorAdded = true
				teamMembers = append(teamMembers, &advisorToAdd)
			}
		}

		// if the new member id not added and the operation is an update operation
		// return an error since there doesn't exist a user to update
		if !newAdvisorAdded && !isAddOperation {
			db.Logger.Error("advisor doest not exist")
			return nil, errors.New("advisor does not exist")
		} else if newAdvisorAdded && !isAddOperation {
			db.Logger.Error("unable to add advisor")
			return nil, errors.New("error occurred. unable to add new advisor")
		}

		teamOrm.Members = teamMembers
		if err := tx.Save(&teamOrm).Error; err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		updatedTeam, err := teamOrm.ToPB(ctx)
		if err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		db.Logger.Info("successfully added/updated team advisor")
		return &updatedTeam, nil
	}

	output, err := db.PerformComplexTransaction(transaction)

	if err != nil {
		return model.Team{}, err
	}

	updatedTeam := output.(*model.Team)
	return *updatedTeam, nil
}

// UpdateTeam updates the team account if it already exists. This operation is performed
// within a transaction hence any errors occuring throughout the transaction lifecycle are
// returned as well as the updated team account record
func (db *Database) UpdateTeam(ctx context.Context, teamID uint32, team model.Team) (*model.Team, error) {
	transaction := func(tx *gorm.DB) (interface{}, error) {
		exists, teamOrm := db.getTeamIfExist(teamID, tx)
		if !exists {
			db.Logger.Error("team account does not exist")
			return nil, errors.New("team account does not exist. please create one and try again")
		}

		// validate the new team object passed as an input parameter and convert to orm type
		// then update in the backend database
		if err := team.Validate(); err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		newTeamOrm, err := team.ToORM(ctx)
		if err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		// assert that both team ids match prior to updating
		if newTeamOrm.Id == teamOrm.Id {
			if err := tx.Save(&newTeamOrm).Error; err != nil {
				db.Logger.Error(err.Error())
				return nil, err
			}

			return &team, nil
		}

		db.Logger.Error("requested team to update has different id")
		return nil, errors.New("requested team to update has different id")
	}

	output, err := db.PerformComplexTransaction(transaction)

	if err != nil {
		return nil, err
	}

	updatedTeam := output.(*model.Team)
	return updatedTeam, nil
}

// UpdateTeamProfile updates the team accounts profile if the team indeed exists
func (db *Database) UpdateTeamProfile(ctx context.Context, teamID uint32, profile model.TeamProfile) (*model.Team, error) {
	transaction := func(tx *gorm.DB) (interface{}, error) {
		// check that the team of interest exists
		ok, teamAccount := db.getTeamIfExist(teamID, tx)
		if !ok {
			db.Logger.Error("team account does not exist")
			return nil, errors.New("team account does not exist. please create one and try again")
		}

		// from the team model extract the profile and validate it exists
		oldProfile := teamAccount.TeamProfile
		if oldProfile == nil {
			db.Logger.Error("team profile account cannot be updated as it does not exist")
			return nil, errors.New("team profile account cannot be updated as it does not exist")
		}

		// convert the team profile object to an ORM type after validating
		if err := profile.Validate(); err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		// check that the profile being updated witholds the same id
		newProfile, err := profile.ToORM(ctx)
		if err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		// if profile Ids match then update else error out
		if newProfile.Id == oldProfile.Id {
			teamAccount.TeamProfile = &newProfile
			if err := tx.Save(&teamAccount).Error; err != nil {
				db.Logger.Error(err.Error())
				return nil, err
			}

			team, err := teamAccount.ToPB(ctx)
			if err != nil {
				db.Logger.Error(err.Error())
				return nil, err
			}

			db.Logger.Info("successfully update team profile")
			return &team, nil
		}

		db.Logger.Error("target team profile id and profile id of team to be updated dont match.")
		return nil, errors.New("target team profile id and profile id of team to be updated dont match.")
	}

	output, err := db.PerformComplexTransaction(transaction)

	if err != nil {
		return nil, err
	}

	updatedTeam := output.(*model.Team)
	return updatedTeam, nil

}

// UpdateTeamAdmin updates a team admin if both the admin and team exist
func (db *Database) UpdateTeamAdmin(ctx context.Context, teamID uint32, adminID uint32, admin model.User) (*model.Team, error) {
	transaction := func(tx *gorm.DB) (interface{}, error) {
		exists, teamOrm := db.getTeamIfExist(teamID, tx)
		if !exists {
			db.Logger.Error("team account does not exist")
			return nil, errors.New("team account does not exist. please create one")
		}

		// validate the admin input param
		if err := admin.Validate(); err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		adminModel, err := admin.ToORM(ctx)
		if err != nil {
			db.Logger.Error(err.Error())
			return nil, err
		}

		if teamOrm.Admin != nil && teamOrm.Admin.Id == adminID {
			// perform a deep copy
			if err := copier.Copy(teamOrm.Admin, adminModel); err != nil {
				db.Logger.Error(err.Error())
				return nil, err
			}

			if err = tx.Save(&teamOrm).Error; err != nil {
				db.Logger.Error(err.Error())
				return nil, err
			}

			// convert teamOrm to team object and return
			team, err := teamOrm.ToPB(ctx)
			if err != nil {
				db.Logger.Error(err.Error())
				return nil, err
			}

			db.Logger.Info("successfully updated team admin")
			return team, nil

		}

		db.Logger.Error("admin ids do not match. cannot perform update")
		return nil, errors.New("admin ids do not match. cannot perform update")

	}

	output, err := db.PerformComplexTransaction(transaction)

	if err != nil {
		return nil, err
	}

	updatedTeam := output.(*model.Team)
	return updatedTeam, nil
}

// getTeamIfExist checks wether a team exists in the database. It returns a boolen denoting
// record existence as well as the actual record of interest
func (db *Database) getTeamIfExist(teamID uint32, tx *gorm.DB) (bool, *model.TeamORM) {
	var teamOrm model.TeamORM
	if recordNotFound := tx.First(&teamOrm, teamID).RecordNotFound(); recordNotFound {
		return false, nil
	}

	return true, &teamOrm
}

// getSubscriptionIfExist checks wether a subscription exists. It returns a boolean denoting
// existence as well as the obtained subscription
func (db *Database) getSubscriptionIfExist(teamID uint32, tx *gorm.DB, subscription *model.Subscriptions) (bool, *model.SubscriptionsORM) {

	exists, teamOrm := db.getTeamIfExist(teamID, tx)

	if !exists {
		return false, nil
	}

	if teamOrm.Subscriptions == nil || len(teamOrm.Subscriptions) == 0 {
		return false, nil
	}

	for _, teamSubscription := range teamOrm.Subscriptions {
		if teamSubscription.SubscriptionName == subscription.SubscriptionName {
			return true, teamSubscription
		}
	}

	return false, nil
}
