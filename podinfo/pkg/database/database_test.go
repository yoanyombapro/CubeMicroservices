package database

import (
	"os"
)

// InitializeTestSetup initializes a connection to the database initially
func InitializeTestSetup() (*Database, func()) {
	testDbInstance, cleanerHandler := Setup()
	if cleanerHandler == nil || testDbInstance == nil {
		os.Exit(1)
	}
	return testDbInstance, cleanerHandler
}
