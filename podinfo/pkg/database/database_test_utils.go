package database

import (
	"database/sql"
	"testing"

	"os"

	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/jinzhu/gorm"
	"go.uber.org/zap"

	model "gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/models/proto"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/utils/testutils"
)

var connSettings = "postgresql://doadmin:oqshd3sto72yyhgq@test-do-user-6612421-0.a.db.ondigitalocean.com:25060/defaultdb?sslmode=require"

// Setup sets up database connection prior to testing
func Setup() (*Database, func()) {
	// database connection string
	// initialize connection to the database
	db := Initialize(connSettings)
	// spin up tables for testing
	_ = CreateTableForTests(db.Engine)
	// defer deleting all created entries
	cleaner := DeleteCreatedEntities(db.Engine)
	return db, cleaner
}

// Initialize creates a singular connection to the backend database instance
func Initialize(connSettings string) *Database {
	var err error
	// configure logging
	logger := zap.L()
	defer logger.Sync()
	stdLog := zap.RedirectStdLog(logger)
	defer stdLog()

	connectionString := connSettings

	// connect to database
	dbInstance, err := New(connectionString, logger)
	if err != nil {
		logger.Info("Error connecting to database", zap.Error(err))
		os.Exit(1)
	}

	return dbInstance
}

// DeleteCreatedEntities sets up GORM `onCreate` hook and return a function that can be deferred to
// remove all the entities created after the hook was set up
// You can use it as
//
// func TestSomething(t *testing.T){
//     db, _ := gorm.Open(...)
//
//     cleaner := DeleteCreatedEntities(db)
//     defer cleaner()
//
// }
func DeleteCreatedEntities(db *gorm.DB) func() {
	type entity struct {
		table   string
		keyname string
		key     interface{}
	}
	var entries []entity
	hookName := "cleanupHook"

	db.Callback().Create().After("gorm:create").Register(hookName, func(scope *gorm.Scope) {
		// fmt.Printf("Inserted entities of %s with %s=%v\n", scope.TableName(), scope.PrimaryKey(), scope.PrimaryKeyValue())
		entries = append(entries, entity{table: scope.TableName(), keyname: scope.PrimaryKey(), key: scope.PrimaryKeyValue()})
	})
	return func() {
		// Remove the hook once we're done
		defer db.Callback().Create().Remove(hookName)
		// Find out if the current db object is already a transaction
		_, inTransaction := db.CommonDB().(*sql.Tx)
		tx := db
		if !inTransaction {
			tx = db.Begin()
		}
		// Loop from the end. It is important that we delete the entries in the
		// reverse order of their insertion
		for i := len(entries) - 1; i >= 0; i-- {
			entry := entries[i]
			// fmt.Printf("Deleting entities from '%s' table with key %v\n", entry.table, entry.key)
			tx.Table(entry.table).Where(entry.keyname+" = ?", entry.key).Delete("")
		}

		if !inTransaction {
			tx.Commit()
		}
	}
}

// CreateAndSaveTestUser creates a test user and saves it to the database
func CreateAndSaveTestUser(t *testing.T, db *Database) *model.UserORM {
	// create a test user in the database and attempt to obtain the user of interest if it exists
	user, err := GenerateTestUserOrm()
	if err != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", err)
	}

	// save the temporary user to the database
	if err := db.Engine.Save(&user).Error; err != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", err)
	}
	return user
}

// CreateAndSaveTestSubscription generate a test subscription and saves it to a database
func CreateAndSaveTestSubscription(t *testing.T, db *Database) (uint32, string, uint32) {
	// create a test subscription in the database and attempt to obtain the subscription of interest if it exists
	// it is imperative that a user is created and the subscription is tied to this user
	user, err := GenerateTestUserOrm()
	if err != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", err)
	}

	subscription, err := GenerateTestSubscriptionOrm()
	if err != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", err)
	}

	user.Subscriptions = append(user.Subscriptions, subscription)

	// save the temporary user to the database
	if err := db.Engine.Save(&user).Error; err != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", err)
	}
	return user.Id, subscription.SubscriptionName, user.Subscriptions[0].Id
}

// CreateAndSaveTestUserProfile generate a test user profile and saves it to a database
func CreateAndSaveTestUserProfile(t *testing.T, db *Database) uint32 {
	// create a test user profile in the database and attempt to obtain the profile of interest if it exists
	// it is imperative that a user is created and the profile is tied to this user
	user, err := GenerateTestUserOrm()
	if err != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", err)
	}

	profile, err := GenerateTestUserProfileOrm()
	if err != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", err)
	}

	user.Profile = profile

	// save the temporary user to the database
	if err := db.Engine.Save(&user).Error; err != nil {
		t.Errorf("Expected empty error field to be returned'. Got '%v'", err)
	}
	return user.Id
}

// GenerateTestUserProfileOrm generates a user profile of orm type
func GenerateTestUserProfileOrm() (*model.ProfileORM, error) {
	id, err := testutils.NewUUID()
	if err != nil {
		return nil, err
	}

	profile := model.ProfileORM{
		Address:                  nil,
		AvatarUrl:                "http://random.url.avatar.com" + id,
		Bio:                      "new profile for testing",
		BlockedAccountsPrivacyId: nil,
		CreatedAt:                nil,
		DeletedAt:                nil,
		Education:                nil,
		Experience:               nil,
		Group:                    nil,
		Id:                       0,
		MutedAccountsPrivacyId:   nil,
		Nationality:              "cameroon",
		ProfileType:              "investor",
		Settings:                 nil,
		Skills:                   nil,
		SocialMedia:              nil,
		Team:                     nil,
		UpdatedAt:                nil,
		UserId:                   nil,
	}

	return &profile, nil
}

// CreateAndSaveSubscriptionOrm create a random subscription and saves it to a database
func GenerateTestSubscriptionOrm() (*model.SubscriptionsORM, error) {
	id, err := testutils.NewUUID()
	if err != nil {
		return nil, err
	}

	subscription := model.SubscriptionsORM{
		SubscriptionName:   "Smart News Subscriptions" + id,
		SubscriptionStatus: "Active",
		StartDate:          nil,
		EndDate:            nil,
		AccessType:         "Public",
		IsActive:           false,
	}

	return &subscription, nil
}

// GenerateTestUser returns a new random user orm object
func GenerateTestUserOrm() (*model.UserORM, error) {
	id, err := testutils.NewUUID()
	if err != nil {
		return nil, err
	}

	user := model.UserORM{
		UserName:          "yoanyomba" + id,
		Email:             "yoanyombapro@gmail.com" + id,
		PasswordConfirmed: "crunchesForYou" + id,
		Password:          "crunchesForYou" + id,
		FirstName:         "yoan" + id,
		LastName:          "yomba" + id,
		Intent:            "Investment" + id,
	}
	return &user, nil
}

// GenerateTestUser creates a test user
func GenerateTestUser() (*model.User, error) {
	str, err := testutils.NewUUID()
	if err != nil {
		return nil, err
	}

	user := model.User{
		Id:                   0,
		CreatedAt:            nil,
		DeletedAt:            nil,
		UpdatedAt:            nil,
		UserAccountType:      "investor",
		FirstName:            "yoan" + str,
		LastName:             "yomba" + str,
		UserName:             "yoanyomba" + str,
		Gender:               "male",
		Languages:            "french, english",
		Password:             "testuser-password" + str,
		PasswordConfirmed:    "testuser-password" + str,
		Age:                  0,
		BirthDate:            "",
		PhoneNumber:          "",
		Email:                "yoanyomba@test.com" + str,
		Intent:               "make a lot of friends",
		Profile:              nil,
		ResetToken:           str,
		ResetTokenExpiration: nil,
		Subscriptions:        nil,
		IsActive:             false,
		AuthnAccountId:       0,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	return &user, nil
}

// GenerateTestUserProfile creates a test user profile
func GenerateTestUserProfile() (*model.Profile, error) {
	userProfile := model.Profile{
		Id:                   0,
		CreatedAt:            nil,
		DeletedAt:            nil,
		UpdatedAt:            nil,
		Bio:                  "here to make a lot of new friends and grow my company",
		Experience:           nil,
		Address:              nil,
		Education:            nil,
		Skills:               nil,
		Team:                 nil,
		Group:                nil,
		SocialMedia:          nil,
		Settings:             nil,
		ProfileType:          "investor",
		Nationality:          "",
		AvatarUrl:            "",
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}

	return &userProfile, nil
}

// GeneratetestSubscription creates a random user subscription for testing
func GenerateTestSubscription() (*model.Subscriptions, error) {
	str, err := testutils.NewUUID()
	if err != nil {
		return nil, err
	}

	return &model.Subscriptions{
		Id:                   0,
		SubscriptionName:     "testSubscription" + str,
		SubscriptionStatus:   "testSubscriptionStatus" + str,
		StartDate:            &timestamp.Timestamp{},
		EndDate:              &timestamp.Timestamp{},
		AccessType:           "Full",
		IsActive:             false,
		CreatedAt:            nil,
		DeletedAt:            nil,
		UpdatedAt:            nil,
		XXX_NoUnkeyedLiteral: struct{}{},
		XXX_unrecognized:     nil,
		XXX_sizecache:        0,
	}, nil
}
