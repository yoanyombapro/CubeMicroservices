package customerror

import (
	"errors"
)

var (
	// ErrUserprofileAlreadyExists user already exists
	ErrUserprofileAlreadyExists = errors.New("User profile already exists")
	// ErrUserAccountDoesNotExist user account does not exist
	ErrUserAccountDoesNotExist = errors.New("User account does not exist. Please create a user account first")
	// ErrSubscriptionAlreadyExists subscription already exists
	ErrSubscriptionAlreadyExists = errors.New("Subscription already exists")
)
