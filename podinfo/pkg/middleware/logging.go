package middleware

import (
	"net/http"

	"go.uber.org/zap"
)

// LoggingMiddleware witholds logging functionality useful for
// logging request metadata
type LoggingMiddleware struct {
	logger *zap.Logger
}

// NewLoggingMiddleware returns a news pointer reference to LoggingMiddleware
func NewLoggingMiddleware(logger *zap.Logger) *LoggingMiddleware {
	return &LoggingMiddleware{
		logger: logger,
	}
}

// Handler is the login middleware's handler function. It logs request metadata
// in a middleware chain
func (m *LoggingMiddleware) Handler(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		m.logger.Debug(
			"request started",
			zap.String("proto", r.Proto),
			zap.String("uri", r.RequestURI),
			zap.String("method", r.Method),
			zap.String("remote", r.RemoteAddr),
			zap.String("user-agent", r.UserAgent()),
		)
		next.ServeHTTP(w, r)
	})
}
