# Cube Platform Backend Microservices 

[![Codacy Badge](https://api.codacy.com/project/badge/Grade/75d55f786c504e1cb5209dd8dfa93cb7)](https://www.codacy.com/manual/yoanyombapro/CubeMicroservices?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=yoanyombapro/CubeMicroservices&amp;utm_campaign=Badge_Grade)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/yoanyombapro/CubeMicroservices)](https://goreportcard.com/report/gitlab.com/yoanyombapro/CubeMicroservices)
[![GoDoc](https://godoc.org/gitlab.com/yoanyombapro/CubeMicroservices?status.svg)](https://godoc.org/gitlab.com/yoanyombapro/CubeMicroservices)

This project is implements numerous practical microservices for the CUBE platform

## Table of Contents

-   [Architecture Overview](#architecture-overview)
    -   [System Diagram](#system-diagram)
    -   [Container Diagram](#container-diagram)
    -   [Container Specifications](#container-specifications)
-   [*Evaluation Scenarios*](#evaluation-scenarios)
    -   [**Desktop Evaluation**](#desktop-evaluation)
        -   [Docker Desktop Installation](#docker-desktop-installation)
            -   [Getting Started](#getting-started)
            -   [Docker Compose Classic (Local)](#docker-compose-classic-local)
            -   [Docker Stacks (Kubernetes or Swarm)](#docker-stacks-kubernetes-or-swarm)
            -   [Deploying to Kubernetes](#deploying-to-kubernetes)
                -   [Docker Build](#build-and-deploy)
                -   [Docker Deploy](#deploy-with-docker)
    -   [**Hyperscale Evaluation**](#hyperscale-evaluation)
        -   [Helm Installation](#helm-installation)
        -   [Update Dependencies](#update-dependencies)
        -   [Helm Deploy](#helm-deploy)
        -   [Metrics and Monitoring](#metrics-and-monitoring)
-   [Running the Social Network](#running-a-social-network)
    -   [API Gateway](#api-gateway)
    -   [Generating Data](#generating-data)
    -   [Event Sourcing and CQRS](#event-sourcing-and-cqrs)
        -   [Finding Mutual Friends](#finding-mutual-friends)
        -   [Recommending New Friends](#recommending-new-friends)
-   [Conventional Best Practices](#conventional-best-practices)
    -   [Domain Services](#domain-services)
    -   [Aggregate Services](#aggregate-services)
-   [License](#license)
