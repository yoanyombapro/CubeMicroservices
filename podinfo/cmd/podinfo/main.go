package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"

	"gitlab.com/yoanyombapro/CubeMicroservices/common/circuitbreaker"
	"gitlab.com/yoanyombapro/CubeMicroservices/common/counters/infrastructure"
	"gitlab.com/yoanyombapro/CubeMicroservices/common/messaging/rabbitmq"
	"gitlab.com/yoanyombapro/CubeMicroservices/common/tracing"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/api"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/authentication"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/database"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/grpc"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/signals"
	"gitlab.com/yoanyombapro/CubeMicroservices/podinfo/pkg/version"
)

func main() {
	// flags definition
	fs := pflag.NewFlagSet("default", pflag.ContinueOnError)
	fs.Int("port", 9898, "HTTP port")
	fs.Int("port-metrics", 9897, "metrics port")
	fs.Int("grpc-port", 9886, "gRPC port")
	fs.String("grpc-service-name", "podinfo", "gPRC service name")
	fs.String("level", "info", "log level debug, info, warn, error, flat or panic")
	fs.StringSlice("backend-url", []string{}, "backend service URL")
	fs.Duration("http-client-timeout", 2*time.Minute, "client timeout duration")
	fs.Duration("http-server-timeout", 30*time.Second, "server read and write timeout duration")
	fs.Duration("http-server-shutdown-timeout", 5*time.Second, "server graceful shutdown timeout duration")
	fs.String("data-path", "/data", "data local path")
	fs.String("config-path", "", "config dir path")
	fs.String("config", "config.yaml", "config file name")
	fs.String("ui-path", "./ui", "UI local path")
	fs.String("ui-logo", "", "UI logo")
	fs.String("ui-color", "#34577c", "UI color")
	fs.String("ui-message", fmt.Sprintf("greetings from podinfo v%v", version.VERSION), "UI message")
	fs.Bool("h2c", false, "allow upgrading to H2C")
	fs.Bool("random-delay", false, "between 0 and 5 seconds random delay")
	fs.Bool("random-error", false, "1/3 chances of a random response error")
	fs.Bool("unhealthy", false, "when set, healthy state is never reached")
	fs.Bool("unready", false, "when set, ready state is never reached")
	fs.Int("stress-cpu", 0, "number of CPU cores with 100 load")
	fs.Int("stress-memory", 0, "MB of data to load into memory")
	fs.String("conn-type", "postgresql://", "database connection type")
	fs.String("conn-address", "doadmin:oqshd3sto72yyhgq@test-do-user-6612421-0.a.db.ondigitalocean.com:25060/", "database connection address")
	fs.String("conn-name", "defaultdb", "Database connection name")
	fs.String("conn-settings", "?sslmode=require", "database connection settings")
	fs.String("service-name", "user_management_test", "the service name")
	fs.Bool("production", false, "defines wether service running in production")
	fs.Bool("redirectohttps", false, "redirect all http traffic to https")
	fs.String("zipkin-server-url", "http://zipkin:9411", "url of the zipkin server")
	fs.String("amqp-server-url", "amqp://guest:guest@localhost:5672", "url of the rabbitmq server")
	fs.Bool("enable-tracing", true, "enable tracing of distributed transactions")
	fs.Bool("enable-messaging", true, "enable communication with msg queues")
	fs.Bool("enable-monitoring", true, "enable monitoring via counters")
	fs.Int("AUTHN_PORT", 3000, "authentication service port")
	fs.String("AUTH_SERVICE", "authentication-service", "authentication service name")
	fs.Bool("ENABLE_AUTH_SERVICE_PRIVATE_INTEGRATION", false, "enables communication with authentication service")
	fs.String("AMQP_CONSUMER_QUEUES", "", "set of queue names that this service consumes messages from")
	fs.Int("NUM_CONSUMING_QUEUES", 0, "number of consuming queues")
	fs.String("AMQP_PRODUCER_QUEUES", "Email-Service:direct,Notification-Service:direct,discovery:direct", "set of queue names that this service pushes messages to")
	fs.Int("NUM_PRODUCING_QUEUES", 2, "number of producing queues")
	fs.String("SERVICE_EMAIL", "yoanyombapro@gmail.com", "service email used to send emails to users")

	versionFlag := fs.BoolP("version", "v", false, "get version number")

	var (
		mc            *rabbitmq.RabbitMQClient               = nil
		infraCounters *infrastructure.InfrastructureCounters = nil
	)

	// parse flags
	err := fs.Parse(os.Args[1:])
	switch {
	case err == pflag.ErrHelp:
		os.Exit(0)
	case err != nil:
		fmt.Fprintf(os.Stderr, "Error: %s\n\n", err.Error())
		fs.PrintDefaults()
		os.Exit(2)
	case *versionFlag:
		fmt.Println(version.VERSION)
		os.Exit(0)
	}

	// bind flags and environment variables
	viper.BindPFlags(fs)
	viper.RegisterAlias("backendUrl", "backend-url")
	hostname, _ := os.Hostname()
	viper.SetDefault("jwt-secret", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9")
	viper.SetDefault("ui-logo", "https://raw.githubusercontent.com/stefanprodan/podinfo/gh-pages/cuddle_clap.gif")
	viper.Set("hostname", hostname)
	viper.Set("version", version.VERSION)
	viper.Set("revision", version.REVISION)
	viper.SetEnvPrefix("PODINFO")
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
	viper.AutomaticEnv()

	// load config from file
	if _, err := os.Stat(filepath.Join(viper.GetString("config-path"), viper.GetString("config"))); err == nil {
		viper.SetConfigName(strings.Split(viper.GetString("config"), ".")[0])
		viper.AddConfigPath(viper.GetString("config-path"))
		if err := viper.ReadInConfig(); err != nil {
			fmt.Printf("Error reading config file, %v\n", err)
		}
	}

	tracingEnabled := viper.GetBool("enable-tracing")
	messagingEnabled := viper.GetBool("enable-messaging")
	monitoringEnabled := viper.GetBool("enable-monitoring")
	serviceName := viper.GetString("service-name")

	consumingQueues := viper.GetString("AMQP_CONSUMER_QUEUES")
	numConsumingQueues := viper.GetInt("NUM_CONSUMING_QUEUES")
	producingQueues := viper.GetString("AMQP_PRODUCER_QUEUES")
	numProducingQueues := viper.GetInt("NUM_PRODUCING_QUEUES")

	// configure logging
	logger, _ := initZap(viper.GetString("level"))
	defer logger.Sync()
	stdLog := zap.RedirectStdLog(logger)
	defer stdLog()

	connectionString := viper.GetString("conn-type") + viper.GetString("conn-address") + viper.GetString("conn-name") + viper.GetString("conn-settings")

	// connect to database
	db, err := database.New(connectionString, logger)
	defer db.Engine.Close()

	if err != nil {
		logger.Info("Error connecting to database", zap.Error(err))
		os.Exit(1)
	}

	// start stress tests if any
	beginStressTest(viper.GetInt("stress-cpu"), viper.GetInt("stress-memory"), logger)

	// validate port
	if _, err := strconv.Atoi(viper.GetString("port")); err != nil {
		port, _ := fs.GetInt("port")
		viper.Set("port", strconv.Itoa(port))
	}

	if _, err := strconv.Atoi(viper.GetString("AUTHN_PORT")); err != nil {
		authnPort, _ := fs.GetInt("AUTHN_PORT")
		viper.Set("AUTHN_PORT", strconv.Itoa(authnPort))
	}

	// load gRPC server config
	var grpcCfg grpc.Config
	if err := viper.Unmarshal(&grpcCfg); err != nil {
		logger.Panic("config unmarshal failed", zap.Error(err))
	}

	// start gRPC server
	if grpcCfg.Port > 0 {
		grpcSrv, _ := grpc.NewServer(&grpcCfg, logger)
		go grpcSrv.ListenAndServe()
	}

	// load HTTP server config
	var srvCfg api.Config
	if err := viper.Unmarshal(&srvCfg); err != nil {
		logger.Panic("config unmarshal failed", zap.Error(err))
	}

	if messagingEnabled {
		// process consuming and producing queues
		queues := ParseAndCreateQueueReference(consumingQueues, producingQueues, numProducingQueues, numConsumingQueues, logger)
		// initiate broker connection
		mc = initializeMessaging(&srvCfg, queues, logger)
	}

	if tracingEnabled {
		initializeTracing(&srvCfg)
		circuitbreaker.ConfigureHystrix([]string{"EmailService", "SocialService"}, mc)
		client := &http.Client{}
		var transport http.RoundTripper = &http.Transport{
			DisableKeepAlives: true,
		}
		client.Transport = transport
		circuitbreaker.Client = client
	}

	// log version and port
	logger.Info("Starting "+serviceName,
		zap.String("version", viper.GetString("version")),
		zap.String("revision", viper.GetString("revision")),
		zap.String("port", srvCfg.Port),
	)

	// obtain authentication port and service name
	authSrvName := viper.GetString("auth-service-name")
	authSvcConnStr := "http://" + authSrvName + ":" + viper.GetString("AUTHN_PORT")
	authSvcEnablePrivateInteraction := viper.GetBool("ENABLE_AUTH_SERVICE_PRIVATE_INTEGRATION")

	// create a connection wrapper to the authentication service
	auth := authentication.NewAuthenticationService(authSvcConnStr, authSvcEnablePrivateInteraction)

	if monitoringEnabled {
		// initialize infrastructure counters
		infraCounters = infrastructure.New(srvCfg.ServiceName, db.Engine)
	}

	// start HTTP server
	srv, _ := api.NewServer(&srvCfg, logger, db, mc, infraCounters, auth)
	stopCh := signals.SetupSignalHandler()

	// call the authentication service and obtain configuration parameters such as jwt signing key
	aggErr, jwtConfig := srv.Auth.GetJwtPublicKey()
	if aggErr != nil {
		logger.Panic("failed to obtain jwt public key uri", zap.Error(aggErr.Error))
	}

	// from the config object should call a get request to the jwks_uri
	// which is the url at which the public key resides
	aggErr, jwtKeys := srv.Auth.GetJwks(jwtConfig.JwtPublicKeyURI)
	if aggErr != nil {
		logger.Panic("failed to obtain jwt public key", zap.Error(aggErr.Error))
	}

	logger.Info("Successfully obtained jwt config parameters from authentication service")

	// extract the public key and store as within the server
	srv.JwtConfig = jwtConfig
	srv.Keys = jwtKeys
	srv.ListenAndServe(stopCh)
}

func initZap(logLevel string) (*zap.Logger, error) {
	level := zap.NewAtomicLevelAt(zapcore.InfoLevel)
	switch logLevel {
	case "debug":
		level = zap.NewAtomicLevelAt(zapcore.DebugLevel)
	case "info":
		level = zap.NewAtomicLevelAt(zapcore.InfoLevel)
	case "warn":
		level = zap.NewAtomicLevelAt(zapcore.WarnLevel)
	case "error":
		level = zap.NewAtomicLevelAt(zapcore.ErrorLevel)
	case "fatal":
		level = zap.NewAtomicLevelAt(zapcore.FatalLevel)
	case "panic":
		level = zap.NewAtomicLevelAt(zapcore.PanicLevel)
	}

	zapEncoderConfig := zapcore.EncoderConfig{
		TimeKey:        "ts",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		MessageKey:     "msg",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.ISO8601TimeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}

	zapConfig := zap.Config{
		Level:       level,
		Development: false,
		Sampling: &zap.SamplingConfig{
			Initial:    100,
			Thereafter: 100,
		},
		Encoding:         "json",
		EncoderConfig:    zapEncoderConfig,
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
	}

	return zapConfig.Build()
}

// initialize zipkin connection
func initializeTracing(cfg *api.Config) {
	tracing.InitTracing(cfg.ZipkinServerUrl, cfg.ServiceName)
}

var stressMemoryPayload []byte

func beginStressTest(cpus int, mem int, logger *zap.Logger) {
	done := make(chan int)
	if cpus > 0 {
		logger.Info("starting CPU stress", zap.Int("cores", cpus))
		for i := 0; i < cpus; i++ {
			go func() {
				for {
					select {
					case <-done:
						return
					default:

					}
				}
			}()
		}
	}

	if mem > 0 {
		path := "/tmp/podinfo.data"
		f, err := os.Create(path)

		if err != nil {
			logger.Error("memory stress failed", zap.Error(err))
		}

		if err := f.Truncate(1000000 * int64(mem)); err != nil {
			logger.Error("memory stress failed", zap.Error(err))
		}

		stressMemoryPayload, err = ioutil.ReadFile(path)
		f.Close()
		os.Remove(path)
		if err != nil {
			logger.Error("memory stress failed", zap.Error(err))
		}
		logger.Info("starting CPU stress", zap.Int("memory", len(stressMemoryPayload)))
	}
}

func initializeMessaging(cfg *api.Config, queues rabbitmq.Queues, logger *zap.Logger) *rabbitmq.RabbitMQClient {
	if cfg.AmqpServerUrl == "" {
		panic("No 'amqp_server_url' set in configuration, cannot start")
	}

	amqpClient, err := rabbitmq.New(cfg.AmqpServerUrl, queues, logger)
	if err != nil {
		logger.Error("error occurred while initiating connection to rabbitmq broker.", zap.Error(err))
		panic(err.Error())
	}

	return amqpClient
}

func ParseAndCreateQueueReference(consumingQueues, producingQueues string, numProducerQueues, numConsumerQueues int, logger *zap.Logger) rabbitmq.Queues {
	// parse both string based in comma seperator
	consumerQueueSet := strings.SplitN(consumingQueues, ",", numProducerQueues)
	producerQueueSet := strings.SplitN(producingQueues, ",", numConsumerQueues)

	fmt.Println(consumerQueueSet)
	fmt.Println(producerQueueSet)

	queueToExchangeMapping := make(map[string]rabbitmq.Exchange)

	// populate the queue to exchange mapping
	populateQueueToExchangeMapping(consumerQueueSet, queueToExchangeMapping, logger)
	populateQueueToExchangeMapping(producerQueueSet, queueToExchangeMapping, logger)

	// initialize a queue from the queue to exchange mapping
	return rabbitmq.InitiateQueues(queueToExchangeMapping)
}

func populateQueueToExchangeMapping(queueSet []string, queueToExchangeMapping map[string]rabbitmq.Exchange, logger *zap.Logger) {
	// from parsed set of queues extract the queue name and exchange type and create a queue instance
	for _, queueName := range queueSet {
		if queueName != "" {
			queueDetails := strings.SplitN(queueName, ":", 2)
			logger.Info("details", zap.Any("queue details", queueDetails))

			name := queueDetails[0]
			exchangeType := queueDetails[1]

			// TODO: Look into the impact of each respective params and enable where fitting
			exchange := rabbitmq.Exchange{
				ExchangeName: name,
				ExchangeType: exchangeType,
				Durable:      true,
				AutoDelete:   false,
				Internal:     false,
				NoWait:       false,
				Args:         nil,
			}

			if _, ok := queueToExchangeMapping[queueName]; !ok {
				queueToExchangeMapping[name] = exchange
			}
		}
	}
}
