version: "3.7"

services:
  redis:
    image: redis
    ports:
      - "8701:6379"
    networks:
      - app-tier

  postgres:
    image: postgres:latest
    restart: unless-stopped
    environment:
      - POSTGRES_DB=dev
      - POSTGRES_USER=postgres-dev
      - POSTGRES_PASSWORD=dev
    ports:
      - 5432:5432

  mysql:
    image: mysql:5.7
    ports:
      - "8702:3306"
    environment:
      - MYSQL_ALLOW_EMPTY_PASSWORD=yes
      - MYSQL_ROOT_PASSWORD

  authentication-service:
    image: keratin/authn-server:latest
    container_name: $AUTH_SERVICE
    command: sh -c "./authn migrate && ./authn server"
    ports:
      - "7002:3000"
    depends_on:
      - redis
      - mysql
    networks:
      - app-tier
    env_file:
      - .env

  storage:
    image: openzipkin/zipkin-mysql
    container_name: mysql
    # Uncomment to expose the storage port for testing
    ports:
      - 3306:3306
    # The zipkin process services the UI, and also exposes a POST endpoint that
    # instrumentation can send trace data to. Scribe is disabled by default.
    networks:
      - app-tier

  # The zipkin process services the UI, and also exposes a POST endpoint that
  # instrumentation can send trace data to. Scribe is disabled by default.
  zipkin:
    image: openzipkin/zipkin
    container_name: zipkin
    # Environment settings are defined here https://github.com/openzipkin/zipkin/blob/master/zipkin-server/README.md#environment-variables
    # zipkin configuration https://github.com/openzipkin-attic/docker-zipkin
    environment:
      - STORAGE_TYPE=mysql
      # Point the zipkin at the storage backend
      - MYSQL_HOST=mysql
      # Uncomment to enable scribe
      # - SCRIBE_ENABLED=true
      # Uncomment to enable self-tracing
      # - SELF_TRACING_ENABLED=true
      # Uncomment to enable debug logging
      # - JAVA_OPTS=-Dlogging.level.zipkin2=DEBUG
    ports:
      # Port used for the Zipkin UI and HTTP Api
      - 9411:9411
      # Uncomment if you set SCRIBE_ENABLED=true
      # - 9410:9410
    depends_on:
      - "storage"
    networks:
      - app-tier

  # Adds a cron to process spans since midnight every hour, and all spans each day
  # This data is served by http://192.168.99.100:8080/dependency
  #
  # For more details, see https://github.com/openzipkin/docker-zipkin-dependencies
  dependencies:
    image: openzipkin/zipkin-dependencies
    container_name: dependencies
    entrypoint: crond -f
    environment:
      - STORAGE_TYPE=mysql
      - MYSQL_HOST=mysql
      # Add the baked-in username and password for the zipkin-mysql image
      - MYSQL_USER=zipkin
      - MYSQL_PASS=zipkin
      # Uncomment to see dependency processing logs
      # - ZIPKIN_LOG_LEVEL=DEBUG
      # Uncomment to adjust memory used by the dependencies job
      # - JAVA_OPTS=-verbose:gc -Xms1G -Xmx1G
    depends_on:
      - "storage"
    networks:
      - app-tier

  user-service:
    build:
      context: .
      dockerfile: Dockerfile
    volumes:
      - '.:/go/src/gitlab.com/yoanyombapro/CubeMicroservices/podinfo'
    container_name: service
    env_file:
      - docker/.env
    ports:
      - 9898:9898
      - 9896:9896
    restart: on-failure
    networks:
      - app-tier
      - web
    depends_on:
      - stats
      - queue-ram1
      - queue-disc1
      - zipkin
      - authentication-service
      - postgres
    links:
      - stats

  stats:
    image: bitnami/rabbitmq:3.7
    environment:
      - RABBITMQ_NODE_TYPE=stats
      - RABBITMQ_NODE_NAME=rabbit@stats
      - RABBITMQ_ERL_COOKIE=s3cr3tc00ki3
      - RABBITMQ_DEFAULT_USER=user
      - RABBITMQ_DEFAULT_PASS=bitnami
    networks:
      - app-tier
    ports:
      - 15672:15672
      - 5672:5672
    volumes:
      - 'rabbitmqstats_data:/bitnami'
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:15672"]
      interval: 30s
      timeout: 10s
      retries: 5

  queue-disc1:
    image: bitnami/rabbitmq:3.7
    environment:
      - RABBITMQ_NODE_TYPE=queue-disc
      - RABBITMQ_NODE_NAME=rabbit@queue-disc1
      - RABBITMQ_CLUSTER_NODE_NAME=rabbit@stats
      - RABBITMQ_ERL_COOKIE=s3cr3tc00ki3
      - RABBITMQ_DEFAULT_USER=user
      - RABBITMQ_DEFAULT_PASS=bitnami
    networks:
      - app-tier
    volumes:
      - 'rabbitmqdisc1_data:/bitnami'

  queue-ram1:
    image: bitnami/rabbitmq:3.7
    environment:
      - RABBITMQ_NODE_TYPE=queue-ram
      - RABBITMQ_NODE_NAME=rabbit@queue-ram1
      - RABBITMQ_CLUSTER_NODE_NAME=rabbit@stats
      - RABBITMQ_ERL_COOKIE=s3cr3tc00ki3
      - RABBITMQ_DEFAULT_USER=user
      - RABBITMQ_DEFAULT_PASS=bitnami
    networks:
      - app-tier
    volumes:
      - 'rabbitmqram1_data:/bitnami'

volumes:
  rabbitmqstats_data:
    driver: local
  rabbitmqdisc1_data:
    driver: local
  rabbitmqram1_data:
    driver: local

networks:
  app-tier:
    driver: bridge
  web:
    external: true

