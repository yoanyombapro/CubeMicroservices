#!/usr/bin/env bash
echo "preparing to run unit tests"
echo "stopping and removing all old docker images"
make clean
echo "starting authentication service"
make run-authentication-service
echo "commencing unit test run"
go test -v -race ./...
echo "completed unit test run"