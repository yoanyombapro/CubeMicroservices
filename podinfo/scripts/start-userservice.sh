#!/usr/bin/env bash
echo "preparing to start user microservice"
echo "stopping and removing all old docker images"
make clean
echo "starting authentication service"
make run-authentication-service
echo "remove any process running with port 9898 and 9896 open"
kill -9 $(lsof -t -i:9898) && kill -9 $(lsof -t -i:9896)
echo "starting user's microservice"
make modd