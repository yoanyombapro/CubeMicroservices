module gitlab.com/yoanyombapro/CubeMicroservices/podinfo

go 1.13

require (
	github.com/DATA-DOG/go-sqlmock v1.4.1 // indirect
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/chzyer/logex v1.1.10 // indirect
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e
	github.com/chzyer/test v0.0.0-20180213035817-a1ea475d72b1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fatih/color v1.9.0
	github.com/fsnotify/fsnotify v1.4.9
	github.com/go-openapi/runtime v0.19.15
	github.com/golang/protobuf v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/gorilla/websocket v1.4.2
	github.com/hashicorp/go-getter v1.4.1
	github.com/infobloxopen/atlas-app-toolkit v0.21.1
	github.com/infobloxopen/protoc-gen-gorm v0.20.0
	github.com/jinzhu/copier v0.0.0-20190924061706-b57f9002281a
	github.com/jinzhu/gorm v1.9.12
	github.com/lib/pq v1.6.0
	github.com/mwitkow/go-proto-validators v0.3.0
	github.com/prometheus/client_golang v1.6.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.7.0
	github.com/stefanprodan/podinfo v1.8.0
	github.com/swaggo/swag v1.6.6
	gitlab.com/yoanyombapro/CubeMicroservices/common v0.0.0-20200603080905-0b3786261850
	go.uber.org/zap v1.15.0
	golang.org/x/crypto v0.0.0-20200602180216-279210d13fed
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9
	google.golang.org/genproto v0.0.0-20200603110839-e855014d5736
	google.golang.org/grpc v1.29.1
	gopkg.in/gormigrate.v1 v1.6.0
	gopkg.in/square/go-jose.v2 v2.5.1
)

replace gitlab.com/yoanyombapro/CubeMicroservices/common => ../common
