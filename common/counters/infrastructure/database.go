package infrastructure

import (
	"github.com/jinzhu/gorm"
	"github.com/prometheus/client_golang/prometheus"
)

func InitDatabaseCounters(servicename string, db *gorm.DB) *DatabaseCounters {
	// https://www.alexedwards.net/blog/configuring-sqldb
	numOpenDatabaseConnections := prometheus.NewGaugeFunc(
		prometheus.GaugeOpts{
			Namespace:   servicename,
			Subsystem:   "database",
			Name:        "open_connections",
			Help:        "Open database connections",
		},
		func() float64 { return float64(db.DB().Stats().MaxOpenConnections) },
	)

	numOpenIdleDatabaseConnections := prometheus.NewGaugeFunc(
		prometheus.GaugeOpts{
			Namespace:   servicename,
			Subsystem:   "database",
			Name:        "idle_connections_open",
			Help:        "Number of idle mysql connections open.",
		},
		func() float64 { return float64(db.DB().Stats().Idle) },
	)

	numDatabaseConnectionsInUse := prometheus.NewGaugeFunc(
		prometheus.GaugeOpts{
			Namespace:   servicename,
			Subsystem:   "database",
			Name:        "connections_in_use",
			Help:        "Number of mysql connections in use.",
		},
		func() float64 { return float64(db.DB().Stats().InUse) },
	)

	timeBlockedWaitingForDatabaseConnection := prometheus.NewGaugeFunc(
		prometheus.GaugeOpts{
			Namespace:   servicename,
			Subsystem:   "database",
			Name:        "database_connection_latency",
			Help:        "Time blocked waiting for a new connection to the database",
		},
		func() float64 { return float64(db.DB().Stats().WaitDuration) },
	)

	databaseOperationLatency := prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace: servicename,
			Subsystem: "database",
			Name:      "database_operation_latency",
			Help:      "Latency per database operation",
		},
		[]string{"operation"},
	)

	return &DatabaseCounters{
		NumDatabaseConnections:                   &numOpenDatabaseConnections,
		BlockedWaitingTimeForDatabaseConnections: &timeBlockedWaitingForDatabaseConnection,
		DatabaseConnectionsInUse:                 &numDatabaseConnectionsInUse,
		IdleDatabaseConnections:                  &numOpenIdleDatabaseConnections,
		LatencyByOperation:                       databaseOperationLatency,
	}
}

func (rc *DatabaseCounters) RegisterMetrics(){
	_ =   RegisterGaugeMetric(*rc.NumDatabaseConnections)
	_ =   RegisterGaugeMetric(*rc.BlockedWaitingTimeForDatabaseConnections)
	_ =   RegisterGaugeMetric(*rc.DatabaseConnectionsInUse)
	_ =   RegisterGaugeMetric(*rc.IdleDatabaseConnections)
	_ =   RegisterSummaryVecMetric(*rc.LatencyByOperation)
}
