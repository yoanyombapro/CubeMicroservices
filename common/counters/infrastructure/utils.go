package infrastructure

import (
	"github.com/prometheus/client_golang/prometheus"
)

// RegisterGaugeMetric registers a gauge metrics counter
func RegisterGaugeMetric(gaugeMetric prometheus.GaugeFunc) *prometheus.GaugeFunc {
	err := prometheus.Register(gaugeMetric)
	if err != nil {
		if prometheus.Unregister(gaugeMetric) {
			prometheus.MustRegister(gaugeMetric)
		} else {
			panic(err)
		}
	}
	return &gaugeMetric
}

// RegisterCounterVecMetric registers a countervec metrics counter
func RegisterCounterVecMetric(counterMetric prometheus.CounterVec) *prometheus.CounterVec {
	err := prometheus.Register(counterMetric)
	if err != nil {
		if prometheus.Unregister(counterMetric) {
			prometheus.MustRegister(counterMetric)
		} else {
			panic(err)
		}
	}
	return &counterMetric
}

// RegisterCounterMetric registers a counter
func RegisterCounterMetric(counterMetric prometheus.Counter) *prometheus.Counter {
	err := prometheus.Register(counterMetric)
	if err != nil {
		if prometheus.Unregister(counterMetric) {
			prometheus.MustRegister(counterMetric)
		} else {
			panic(err)
		}
	}
	return &counterMetric
}

// RegisterSummaryVecMetric registers a summary vec counter
func RegisterSummaryVecMetric(summaryMetric prometheus.SummaryVec) *prometheus.SummaryVec {
	err := prometheus.Register(summaryMetric)
	if err != nil {
		if prometheus.Unregister(summaryMetric) {
			prometheus.MustRegister(summaryMetric)
		} else {
			panic(err)
		}
	}
	return &summaryMetric
}
