package infrastructure

import (
	"github.com/prometheus/client_golang/prometheus"
)

// initialize run time counters
func InitRequestCounters(servicename string) *RequestCounters {
	totalHttpRequest := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: servicename,
			Subsystem: "requests",
			Name:      "http_requests_total",
			Help:      "How many HTTP requests processed, partitioned by status code and HTTP method.",
		},
		[]string{"code", "method", "path"},
	)

	httpRequestLatencySummaryCounter := prometheus.NewSummaryVec(
		prometheus.SummaryOpts{
			Namespace:  servicename,
			Subsystem:  "requests",
			Name:       "http_request_latency_summary",
			Help:       "quantile summary of http request latency",
			Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
		},
		[]string{"method", "path"},
	)

	// httpRequestLatencyCounter.WithLabelValues("POST","/user").Observe(30 + math.Floor(120*math.Sin(float64(i)*0.1))/10)

	return &RequestCounters{
		RequestLatencySummaryCounters: RegisterSummaryVecMetric(*httpRequestLatencySummaryCounter),
		TotalRequestsCounters:         RegisterCounterVecMetric(*totalHttpRequest),
	}
}

func (rc *RequestCounters) RegisterMetrics(){
	_ =  RegisterSummaryVecMetric(*rc.RequestLatencySummaryCounters)
	_ = RegisterCounterVecMetric(*rc.TotalRequestsCounters)
}
